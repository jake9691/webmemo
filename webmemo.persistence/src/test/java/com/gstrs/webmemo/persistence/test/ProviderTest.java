package com.gstrs.webmemo.persistence.test;

import com.gstrs.webmemo.model.entities.Address;
import com.gstrs.webmemo.model.entities.Employee;
import com.gstrs.webmemo.model.entities.Memo;
import com.gstrs.webmemo.model.entities.Organization;
import com.gstrs.webmemo.model.entities.Provider;
import com.gstrs.webmemo.model.entities.Salesman;
import com.gstrs.webmemo.model.entities.Store;
import com.gstrs.webmemo.persistence.IOrganizationRegistry;
import com.gstrs.webmemo.persistence.IProviderRegistry;
import com.gstrs.webmemo.persistence.IWebMemo;
import com.gstrs.webmemo.persistence.validation.ViolationWrapper;
import java.util.Set;
import javax.ejb.EJB;
import junit.framework.Assert;
import static junit.framework.Assert.*;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 *
 * @author Jacob
 */
@RunWith(Arquillian.class)
public class ProviderTest {

    @Deployment
    public static Archive<?> createDeployment() {
        return ShrinkWrap.create(WebArchive.class, "webmemo.war")
                .addPackage("com.gstrs.webmemo.persistence")
                .addPackage("com.gstrs.webmemo.persistence.impl")
                .addAsResource("test-persistence.xml", "META-INF/persistence.xml")
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");

    }

    @Before
    public void before() throws Exception {
    }

    final static Logger logger = LoggerFactory.getLogger(ProviderTest.class);

    @EJB
    private IProviderRegistry providerRegistry;
    @EJB
    private IOrganizationRegistry organizationRegistry;
    @EJB
    private IWebMemo webmemo;

    @Test
    public void testCreateProvider() {
        Organization org = new Organization("Blast");
        Set<ViolationWrapper> violationsOrg = organizationRegistry.create(org);
        assertTrue(violationsOrg.isEmpty());
        
        Provider provider = new Provider("Puma", "PUMA", org);
        Set<ViolationWrapper> violationsProv = providerRegistry.create(provider);
        assertTrue(violationsProv.isEmpty());
        
        Provider prov = providerRegistry.find(provider.getId());
        Assert.assertTrue(prov.equals(provider));
    }
    
    @Test
    public void testValidation(){
        Organization org = new Organization("orgIs");
        Set<ViolationWrapper> violationsOrg = organizationRegistry.create(org);
        assertTrue(violationsOrg.isEmpty());
        
        Provider adidas = new Provider("Adidas", "ADIDA", org);
        Set<ViolationWrapper> violationsProv = providerRegistry.create(adidas);
        assertTrue(violationsProv.isEmpty());
        
        Provider nikeError = new Provider("Nike", "ADIDA", org);
        violationsProv = providerRegistry.create(nikeError);
        assertTrue(violationsProv.size() > 0);
        
        Provider adidasError = new Provider("Adidas", "NIKE", org);
        violationsProv = providerRegistry.create(adidasError);
        assertTrue(violationsProv.size() > 0);
        
        Provider adidasError2 = new Provider("adidas", "NIKE", org);
        violationsProv = providerRegistry.create(adidasError2);
        assertTrue(violationsProv.size() > 0);
        
        Provider addressTest =  new Provider("address", "ADDRE", org);
        addressTest.setAddress(new Address());
        violationsProv = webmemo.getProviderRegistry().create(addressTest);
        assertTrue(violationsProv.size() > 0);
    }
    
    @Test
    public void testProviderDelete(){
        Organization org = new Organization("deleteproviderOrg");
        Set<ViolationWrapper> viol = organizationRegistry.create(org);
        assertTrue(viol.isEmpty());
        
        Provider provider = new Provider("Björn Borg", "BBORG", org);
        Set<ViolationWrapper> violationsProvider = webmemo.getProviderRegistry().create(provider);
        assertTrue(violationsProvider.isEmpty());
        
        Store store = new Store("deleteprivderstore",org);
        Set<ViolationWrapper> violationsStore = webmemo.getStoreRegistry().create(store);
        assertTrue(violationsStore.isEmpty());

        Employee creator = new Employee("Jacob", "DeleteProvider", "JL", "1122", store);
        Employee responsible = new Employee("Thor", "DeleteProvider", "TG", "2233", store);
        Set<ViolationWrapper> violationsEmpl = webmemo.getEmployeeRegistry().create(creator);
        assertTrue(violationsEmpl.isEmpty());
        violationsEmpl = webmemo.getEmployeeRegistry().create(responsible);
        assertTrue(violationsEmpl.isEmpty());

        Memo memo = new Memo(creator);
        memo.setResponsible(responsible);
        memo.getItem().setProvider(provider);
        Set<ViolationWrapper> violationsMemo = webmemo.getMemoRegistry().create(memo);
        assertTrue(violationsMemo.isEmpty());

        Memo memoResult = webmemo.getMemoRegistry().find(memo.getId());
        assertTrue(memo.equals(memoResult));
        
        violationsProvider = webmemo.getProviderRegistry().delete(provider.getId());
        assertTrue(violationsProvider.size() > 0);
        
        violationsMemo = webmemo.getMemoRegistry().delete(memo.getId());
        assertTrue(violationsMemo.isEmpty());
        
        violationsProvider = webmemo.getProviderRegistry().delete(provider.getId());
        assertTrue(violationsProvider.isEmpty());
    }
    
    @Test
    public void testCreateSalesman(){
        Salesman salesman = new Salesman("Björn", "Gårde", "0703477630");
        Set<ViolationWrapper> violations = webmemo.getSalesmanRegistry().create(salesman);
        assertTrue(violations.isEmpty());
        
        Organization org = new Organization("createSalesmanOrg");
        violations = webmemo.getOrganizationRegistry().create(org);
        assertTrue(violations.isEmpty());
        
        Provider provider = new Provider("Björn Borg2", "BBORG2", org);
        provider.addSalesman(salesman);
        Set<ViolationWrapper> violationsProvider = webmemo.getProviderRegistry().create(provider);
        assertTrue(violationsProvider.isEmpty());
        
    }
}
