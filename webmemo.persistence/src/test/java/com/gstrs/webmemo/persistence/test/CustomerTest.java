package com.gstrs.webmemo.persistence.test;

import com.gstrs.webmemo.model.entities.Customer;
import com.gstrs.webmemo.model.entities.Employee;
import com.gstrs.webmemo.model.entities.Memo;
import com.gstrs.webmemo.model.entities.Organization;
import com.gstrs.webmemo.model.entities.Store;
import com.gstrs.webmemo.persistence.ICustomerRegistry;
import com.gstrs.webmemo.persistence.IEmployeeRegistry;
import com.gstrs.webmemo.persistence.IMemoRegistry;
import com.gstrs.webmemo.persistence.IOrganizationRegistry;
import com.gstrs.webmemo.persistence.IStoreRegistry;
import com.gstrs.webmemo.persistence.validation.ViolationWrapper;
import java.util.List;
import java.util.Set;
import javax.ejb.EJB;
import junit.framework.Assert;
import static junit.framework.Assert.*;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 *
 * @author Jacob
 */
@RunWith(Arquillian.class)
public class CustomerTest {

    @Deployment
    public static Archive<?> createDeployment() {
        return ShrinkWrap.create(WebArchive.class, "webmemo.war")
                .addPackage("com.gstrs.webmemo.persistence")
                .addPackage("com.gstrs.webmemo.persistence.impl")
                .addAsResource("test-persistence.xml", "META-INF/persistence.xml")
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");

    }

    @Before
    public void before() throws Exception {
    }

    final static Logger logger = LoggerFactory.getLogger(CustomerTest.class);

    @EJB
    private ICustomerRegistry customerRegistry;
    @EJB
    private IStoreRegistry storeRegistry;
    @EJB
    private IOrganizationRegistry organizationRegistry;
    @EJB
    private IEmployeeRegistry employeeRegistry;
    @EJB
    private IMemoRegistry memoRegistry;

    @Test
    public void testCreateCustomer() {
        Organization org = new Organization("Testlkfsld");
        Set<ViolationWrapper> violationsOrg = organizationRegistry.create(org);
        assertTrue(violationsOrg.isEmpty());

        Store store = new Store("GBGTESTs", org);
        Set<ViolationWrapper> violationsStore = storeRegistry.create(store);
        Assert.assertTrue(violationsStore.isEmpty());

        Customer customer = new Customer("Jacob", "greger", "0703477630", store);
        Set<ViolationWrapper> violationsCustomer = customerRegistry.create(customer);
        Assert.assertTrue(violationsCustomer.isEmpty());

        Customer customerResult = customerRegistry.find(customer.getId());

        Assert.assertTrue(customerResult.equals(customer));
    }

    @Test
    public void testMoveAllCustomerFromStore1ToStore2() {
        Organization org = new Organization("moveCustomerStore");
        Set<ViolationWrapper> violationsOrg = organizationRegistry.create(org);
        assertTrue(violationsOrg.isEmpty());

        Store store1 = new Store("store1", org);
        Set<ViolationWrapper> violationsStore = storeRegistry.create(store1);
        Assert.assertTrue(violationsStore.isEmpty());

        Store store2 = new Store("store2", org);
        violationsStore = storeRegistry.create(store2);
        Assert.assertTrue(violationsStore.isEmpty());

        Customer customer = new Customer("Jacob", "Larsson2", "0703477630", store1);
        Set<ViolationWrapper> violationsCustomer = customerRegistry.create(customer);
        Assert.assertTrue(violationsCustomer.isEmpty());

        Customer customer2 = new Customer("Gustav", "Larsson3", "0703477630", store1);
        violationsCustomer = customerRegistry.create(customer2);
        Assert.assertTrue(violationsCustomer.isEmpty());

        customerRegistry.moveAllCustomerFromStore1ToStore2(store1, store2);

        Customer customerResult = customerRegistry.find(customer.getId());
        assertTrue(customerResult.getCustomerCreatedInStore().equals(store2));

        customerResult = customerRegistry.find(customer2.getId());
        assertTrue(customerResult.getCustomerCreatedInStore().equals(store2));
    }

    @Test
    public void testValidation() {
        Organization org = new Organization("custValidOrg");
        Set<ViolationWrapper> violationsOrg = organizationRegistry.create(org);
        assertTrue(violationsOrg.isEmpty());

        Store store1 = new Store("custvalidStore", org);
        Set<ViolationWrapper> violationsStore = storeRegistry.create(store1);
        Assert.assertTrue(violationsStore.isEmpty());

        Customer customer1 = new Customer("Jacob", "Bertilsson", "0703477630", store1);
        Set<ViolationWrapper> violationsCustomer = customerRegistry.create(customer1);
        Assert.assertTrue(violationsCustomer.isEmpty());

        Customer customer2 = new Customer("Jacob", "Bertilsson", "0703477630", store1);
        violationsCustomer = customerRegistry.create(customer2);
        Assert.assertTrue(violationsCustomer.size() > 0);

        Customer customer3 = new Customer("Jacob", "bertilsson", "0703477630", store1);
        violationsCustomer = customerRegistry.create(customer3);
        Assert.assertTrue(violationsCustomer.size() > 0);
    }

    @Test
    public void testCustomerValidationDelete() {
        Organization org = new Organization("orgDeleteCUstomer");
        Set<ViolationWrapper> violationsOrg = organizationRegistry.create(org);
        assertTrue(violationsOrg.isEmpty());

        Store store1 = new Store("StoreDeletecustomer", org);
        Set<ViolationWrapper> violationsStore = storeRegistry.create(store1);
        Assert.assertTrue(violationsStore.isEmpty());

        

        Employee employee = new Employee("Viktor", "DELETECUSTOMER", "VD", "93476", store1);
        Set<ViolationWrapper> violationsEmpl = employeeRegistry.create(employee);
        assertTrue(violationsEmpl.isEmpty());
        
        
        Customer customer = new Customer("Jacob", "DeleteCustomer", "0703477630", store1);
        Set<ViolationWrapper> violationsCustomer = customerRegistry.create(customer);
        assertTrue(violationsCustomer.isEmpty());
        
        Memo memo = new Memo(employee);
        memo.setResponsible(employee);
        memo.setCustomer(customer);
        Set<ViolationWrapper> violationsMemo = memoRegistry.create(memo);
        assertTrue(violationsMemo.isEmpty());
        
        violationsCustomer = customerRegistry.delete(customer.getId());
        assertTrue(violationsCustomer.size() > 0);
        
        violationsMemo = memoRegistry.delete(memo.getId());
        assertTrue(violationsMemo.isEmpty());
        
        violationsCustomer = customerRegistry.delete(customer.getId());
        assertTrue(violationsCustomer.isEmpty());
        
        Customer customerRest = customerRegistry.find(customer.getId());
        assertTrue(customerRest == null);
    }
    
    @Test
    public void testSearchForCustomer() {
        Organization org = new Organization("testSearchOrg");
        Set<ViolationWrapper> violationsOrg = organizationRegistry.create(org);
        assertTrue(violationsOrg.isEmpty());

        Store store = new Store("testSearchStore", org);
        Set<ViolationWrapper> violationsStore = storeRegistry.create(store);
        Assert.assertTrue(violationsStore.isEmpty());

        Customer customer = new Customer("Sune", "lennartsson", "0705477430", store);
        Set<ViolationWrapper> violationsCustomer = customerRegistry.create(customer);
        Assert.assertTrue(violationsCustomer.isEmpty());
        
        Customer customer2 = new Customer("David", "Karlsson", "0705477530", store);
        violationsCustomer = customerRegistry.create(customer2);
        Assert.assertTrue(violationsCustomer.isEmpty());
        
        Customer customer3 = new Customer("Johan", "Larsson", "0758935434", store);
        violationsCustomer = customerRegistry.create(customer3);
        Assert.assertTrue(violationsCustomer.isEmpty());
        
        Customer customer4 = new Customer("Niklas", "Johansson", "0708935435", store);
        violationsCustomer = customerRegistry.create(customer4);
        Assert.assertTrue(violationsCustomer.isEmpty());

        List<Customer> customerResult = customerRegistry.searchForCustomer("johan", null, 0, 10);
        assertTrue(customerResult.size() == 2);
        assertTrue(customerResult.contains(customer3));
        assertTrue(customerResult.contains(customer4));
        
        customerResult = customerRegistry.searchForCustomer("893", null, 0, 10);
        assertTrue(customerResult.size() == 2);
        assertTrue(customerResult.contains(customer3));
        assertTrue(customerResult.contains(customer4));
        
        customerResult = customerRegistry.searchForCustomer("07054 dav", null, 0, 10);
        assertTrue(customerResult.size() == 1);
        assertTrue(customerResult.contains(customer2));
        
    }
}
