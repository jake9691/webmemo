package com.gstrs.webmemo.persistence.test;

import com.gstrs.webmemo.model.entities.Customer;
import com.gstrs.webmemo.model.entities.Employee;
import com.gstrs.webmemo.model.entities.Memo;
import com.gstrs.webmemo.model.entities.Order;
import com.gstrs.webmemo.model.entities.Organization;
import com.gstrs.webmemo.model.entities.Reception;
import com.gstrs.webmemo.model.entities.Reminder;
import com.gstrs.webmemo.model.entities.Store;
import com.gstrs.webmemo.persistence.ICustomerRegistry;
import com.gstrs.webmemo.persistence.IEmployeeRegistry;
import com.gstrs.webmemo.persistence.IMemoRegistry;
import com.gstrs.webmemo.persistence.IOrderRegistry;
import com.gstrs.webmemo.persistence.IOrganizationRegistry;
import com.gstrs.webmemo.persistence.IReceptionRegistry;
import com.gstrs.webmemo.persistence.IReminderRegistry;
import com.gstrs.webmemo.persistence.IStoreRegistry;
import com.gstrs.webmemo.persistence.validation.ViolationWrapper;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import javax.ejb.EJB;
import static junit.framework.Assert.*;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 *
 * @author Jacob
 */
@RunWith(Arquillian.class)
public class MemoTest {

    @Deployment
    public static Archive<?> createDeployment() {
        return ShrinkWrap.create(WebArchive.class, "webmemo.war")
                .addPackage("com.gstrs.webmemo.persistence")
                .addPackage("com.gstrs.webmemo.persistence.impl")
                .addAsResource("test-persistence.xml", "META-INF/persistence.xml")
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");

    }

    /**
     * add the permission for a user to the database if they're not already
     * there.1
     *
     * @throws Exception
     */
    @Before
    public void before() throws Exception {
    }

    @EJB
    private IStoreRegistry storeRegistry;
    @EJB
    private IEmployeeRegistry employeeRegistry;
    @EJB
    private IMemoRegistry memoRegistry;
    @EJB
    private IOrderRegistry orderRegistry;
    @EJB
    private IReceptionRegistry receptionRegistry;
    @EJB
    private ICustomerRegistry customerRegistry;
    @EJB
    private IOrganizationRegistry organizationRegistry;
    @EJB
    private IReminderRegistry reminderRegistry;

    final static Logger logger = LoggerFactory.getLogger(MemoTest.class);

    @Test
    public void testCreateMemo() {
        Organization org = new Organization("IKEAsas");
        organizationRegistry.create(org);
        Store mar = new Store("Mariestad",org);
        Set<ViolationWrapper> violationsStore = storeRegistry.create(mar);
        assertTrue(violationsStore.isEmpty());

        Employee creator = new Employee("Jacob", "Larsson", "JL", "1122", mar);
        Employee responsible = new Employee("Thor", "Gautsson", "TG", "2233", mar);
        Set<ViolationWrapper> violationsEmpl = employeeRegistry.create(creator);
        assertTrue(violationsEmpl.isEmpty());
        violationsEmpl = employeeRegistry.create(responsible);
        assertTrue(violationsEmpl.isEmpty());

        Memo memo = new Memo(creator);
        memo.setResponsible(responsible);
        Set<ViolationWrapper> violationsMemo = memoRegistry.create(memo);
        assertTrue(violationsMemo.isEmpty());

        Memo memoResult = memoRegistry.find(memo.getId());
        assertTrue(memo.equals(memoResult));
    }

    @Test
    public void testCreateCompleteMemo() {
        Organization org = new Organization("ELGIG");
        organizationRegistry.create(org);
        
        Store krs = new Store("Kristianstad", org);
        Set<ViolationWrapper> violationsStore = storeRegistry.create(krs);
        assertTrue(violationsStore.isEmpty());

        Employee fredrik = new Employee("Fredrik", "Lundin", "FL", "6632", krs);
        Employee viktor = new Employee("Viktor", "Johansson", "VJ", "1119", krs);
        Set<ViolationWrapper> violationsEmpl = employeeRegistry.create(fredrik);
        assertTrue(violationsEmpl.isEmpty());
        violationsEmpl = employeeRegistry.create(viktor);
        assertTrue(violationsEmpl.isEmpty());

        Memo memo = new Memo(fredrik);
        memo.setResponsible(viktor);
        Set<ViolationWrapper> violationsMemo = memoRegistry.create(memo);
        assertTrue(violationsMemo.isEmpty());

        Memo memoResult = memoRegistry.find(memo.getId());
        Assert.assertTrue(memo.equals(memoResult));

        memoResult.getItem().setName("Skridskor");
        violationsMemo = memoRegistry.update(memoResult);
        assertTrue(violationsMemo.isEmpty());

        memoResult = memoRegistry.find(memo.getId());
        Assert.assertTrue(memoResult.getItem().getName().equals("Skridskor"));

        Order order = new Order(viktor);
        order.setOrderId("IKP982332");
        orderRegistry.create(order);

        memo.setOrder(order);
        memoRegistry.update(memo);//Should cascade order

        Reception reception = new Reception(fredrik);
        reception.setIsCostumerNotified(true);
        reception.setRemarks("The item is recieved. I called the customer and notified him");
        receptionRegistry.create(reception);

        memo.setReception(reception);
        memoRegistry.update(memo);//Should cascade order

        memoResult = memoRegistry.find(memo.getId());

        Assert.assertTrue(memoResult.getOrder().equals(order));
        Assert.assertTrue(memoResult.getReception().equals(reception));

    }

    @Test
    public void testFindRangeOfMemosForStore() {
        //UMEÃ…
        Organization org = new Organization("ONOFF");
        Set<ViolationWrapper> violationsOrg = organizationRegistry.create(org);
        assertTrue(violationsOrg.isEmpty());
        
        Store ume = new Store("UmeÃ¥", org);
        Set<ViolationWrapper> violationsStore = storeRegistry.create(ume);
        assertTrue(violationsStore.isEmpty());
        

        Employee freddyEMPUme = new Employee("Freddy", "Andersson", "FA", "6632", ume);

        Customer philipCustUme = new Customer("Philip", "UMEÃ…", "0703477630", ume);

        Set<ViolationWrapper> violationsEmpl = employeeRegistry.create(freddyEMPUme);
        assertTrue(violationsEmpl.isEmpty());

        Set<ViolationWrapper> violationsCust = customerRegistry.create(philipCustUme);
        assertTrue(violationsCust.isEmpty());
        

        Memo memoUme1 = new Memo(freddyEMPUme);
        memoUme1.setResponsible(freddyEMPUme);
        memoUme1.setCustomer(philipCustUme);
        Set<ViolationWrapper> violationsMemo = memoRegistry.create(memoUme1);
        ume.addMemo(memoUme1);
        assertTrue(violationsMemo.isEmpty());
        
        try{
           TimeUnit.MILLISECONDS.sleep(10);
        }catch(InterruptedException e){
            e.printStackTrace();
        }
        
        Memo memoUme2 = new Memo(freddyEMPUme);
        memoUme2.setResponsible(freddyEMPUme);
        memoUme2.setCustomer(philipCustUme);
        violationsMemo = memoRegistry.create(memoUme2);
        ume.addMemo(memoUme2);
        assertTrue(violationsMemo.isEmpty());
        
        try{
           TimeUnit.MILLISECONDS.sleep(10);
        }catch(InterruptedException e){
            e.printStackTrace();
        }
        
        Memo memoUme3 = new Memo(freddyEMPUme);
        memoUme3.setResponsible(freddyEMPUme);
        memoUme3.setCustomer(philipCustUme);
        violationsMemo = memoRegistry.create(memoUme3);
        ume.addMemo(memoUme3);
        assertTrue(violationsMemo.isEmpty());

        violationsStore = storeRegistry.update(ume);
        assertTrue(violationsStore.isEmpty());

        //LULEÃ…
        Store lule = new Store("LuleÃ¥", org);
        violationsStore = storeRegistry.create(lule);
        assertTrue(violationsStore.isEmpty());

        Employee lolloEMPLule = new Employee("Lollo", "Andersson", "LA", "1337", lule);

        Customer danielCustLule = new Customer("Daniel", "LULEÃ…", "0703477630", lule);

        violationsEmpl = employeeRegistry.create(lolloEMPLule);
        assertTrue(violationsEmpl.isEmpty());

        violationsCust = customerRegistry.create(danielCustLule);
        assertTrue(violationsCust.isEmpty());
        
        
        Memo memoLule1 = new Memo(lolloEMPLule);
        memoLule1.setResponsible(lolloEMPLule);
        memoLule1.setCustomer(danielCustLule);
        violationsMemo = memoRegistry.create(memoLule1);
        lule.addMemo(memoLule1);
        assertTrue(violationsMemo.isEmpty());

        try{
           TimeUnit.MILLISECONDS.sleep(10);
        }catch(InterruptedException e){
            e.printStackTrace();
        }
        
        Memo memoLule2 = new Memo(freddyEMPUme);
        memoLule2.setResponsible(freddyEMPUme);
        memoLule2.setCustomer(danielCustLule);
        violationsMemo = memoRegistry.create(memoLule2);
        lule.addMemo(memoLule2);
        
        assertTrue(violationsMemo.isEmpty());
        
        try{
           TimeUnit.MILLISECONDS.sleep(10);
        }catch(InterruptedException e){
            e.printStackTrace();
        }
        
        Memo memoLule3 = new Memo(freddyEMPUme);
        memoLule3.setResponsible(freddyEMPUme);
        memoLule3.setCustomer(danielCustLule);
        violationsMemo = memoRegistry.create(memoLule3);
        lule.addMemo(memoLule3);
        
        assertTrue(violationsMemo.isEmpty());

        storeRegistry.update(lule);

        List<Memo> luleMemoList = memoRegistry.findRangeOfMemoForStore(lule.getId(), 0, 5);

        Assert.assertTrue(luleMemoList.size() == 3);
        Assert.assertTrue(luleMemoList.contains(memoLule1));
        Assert.assertTrue(luleMemoList.contains(memoLule2));
        Assert.assertTrue(luleMemoList.contains(memoLule3));

        List<Memo> umeMemoList = memoRegistry.findRangeOfMemoForStore(ume.getId(), 0, 3);

        Assert.assertTrue(umeMemoList.size() == 3);
        Assert.assertTrue(umeMemoList.contains(memoUme1));
        Assert.assertTrue(umeMemoList.contains(memoUme2));
        Assert.assertTrue(umeMemoList.contains(memoUme3));

        List<Memo> umeMemoListOrder = memoRegistry.findRangeOfMemoForStore(ume.getId(), 0, 2);

        Assert.assertTrue(umeMemoListOrder.size() == 2);
        Assert.assertTrue(umeMemoListOrder.get(0).equals(memoUme2));
        Assert.assertTrue(umeMemoListOrder.get(1).equals(memoUme3));

    }

    @Test
    public void testFindRangeOfMemosWithResponsibleEmployee() {
        Organization org = new Organization("NextG");
        organizationRegistry.create(org);
        
        Store pite = new Store("PiteÃ¥", org);
        storeRegistry.create(pite);
        Employee jennyEMPPite = new Employee("Jenny", "Andersson", "JA", "1119", pite);
        employeeRegistry.create(jennyEMPPite);
        Employee gregerEMPPite = new Employee("Greger", "Andersson", "GA", "4568", pite);
        employeeRegistry.create(gregerEMPPite);
        Customer davidCustPite = new Customer("David", "PITEÃ…", "0703477630", pite);
        customerRegistry.create(davidCustPite);

        Memo memoUme1 = new Memo(jennyEMPPite);
        memoUme1.setResponsible(gregerEMPPite);
        memoUme1.setCustomer(davidCustPite);
        memoRegistry.create(memoUme1);
        pite.addMemo(memoUme1);

        try{
           TimeUnit.MILLISECONDS.sleep(10);
        }catch(InterruptedException e){
            e.printStackTrace();
        }
        
        Memo memoUme2 = new Memo(jennyEMPPite);
        memoUme2.setResponsible(gregerEMPPite);
        memoUme2.setCustomer(davidCustPite);
        memoRegistry.create(memoUme2);
        pite.addMemo(memoUme2);
        
        try{
           TimeUnit.MILLISECONDS.sleep(10);
        }catch(InterruptedException e){
            e.printStackTrace();
        }

        Memo memoUme3 = new Memo(gregerEMPPite);
        memoUme3.setResponsible(jennyEMPPite);
        memoUme3.setCustomer(davidCustPite);
        memoRegistry.create(memoUme3);
        pite.addMemo(memoUme3);
        
        try{
           TimeUnit.MILLISECONDS.sleep(10);
        }catch(InterruptedException e){
            e.printStackTrace();
        }

        Memo memoUme4 = new Memo(gregerEMPPite);
        memoUme4.setResponsible(jennyEMPPite);
        memoUme4.setCustomer(davidCustPite);
        memoRegistry.create(memoUme4);
        pite.addMemo(memoUme4);
        
        try{
           TimeUnit.MILLISECONDS.sleep(10);
        }catch(InterruptedException e){
            e.printStackTrace();
        }

        Memo memoUme5 = new Memo(gregerEMPPite);
        memoUme5.setResponsible(jennyEMPPite);
        memoUme5.setCustomer(davidCustPite);
        memoRegistry.create(memoUme5);
        pite.addMemo(memoUme5);

        storeRegistry.update(pite);

        List<Memo> piteMemoListGreger = memoRegistry.findRangeOfMemoWithResponsible(gregerEMPPite.getId(), 0, 5);

        Assert.assertTrue(piteMemoListGreger.size() == 2);
        Assert.assertTrue(piteMemoListGreger.contains(memoUme1));
        Assert.assertTrue(piteMemoListGreger.contains(memoUme2));

        List<Memo> piteMemoListJenny = memoRegistry.findRangeOfMemoWithResponsible(jennyEMPPite.getId(), 0, 5);

        Assert.assertTrue(piteMemoListJenny.size() == 3);
        Assert.assertTrue(piteMemoListJenny.get(0).equals(memoUme3));
        Assert.assertTrue(piteMemoListJenny.get(1).equals(memoUme4));
        Assert.assertTrue(piteMemoListJenny.get(2).equals(memoUme5));

        List<Memo> piteMemoListOrder = memoRegistry.findRangeOfMemoWithResponsible(jennyEMPPite.getId(), 0, 2);

        Assert.assertTrue(piteMemoListOrder.size() == 2);
        Assert.assertTrue(piteMemoListOrder.get(0).equals(memoUme4));
        Assert.assertTrue(piteMemoListOrder.get(1).equals(memoUme5));
    }

    @Test
    public void testFindRangeOfMemosWithCustomer() {
        Organization org = new Organization("LastG");
        organizationRegistry.create(org);
        
        Store kir = new Store("Kiruna", org);
        storeRegistry.create(kir);

        Employee joelEMP = new Employee("Joel", "Larsson", "JL", "1348", kir);
        Employee fiddyEMP = new Employee("Fiddy", "Andersson", "VA", "1359", kir);

        Customer gustavCustKir = new Customer("Gustav", "LULEÃ…", "0703477630", kir);
        Customer ragnarCustKir = new Customer("Ragnar", "LULEÃ…", "0703477630", kir);

        employeeRegistry.create(joelEMP);
        employeeRegistry.create(fiddyEMP);

        customerRegistry.create(gustavCustKir);
        customerRegistry.create(ragnarCustKir);

        Memo memoKir1 = new Memo(joelEMP);
        memoKir1.setResponsible(fiddyEMP);
        memoKir1.setCustomer(gustavCustKir);
        memoRegistry.create(memoKir1);
        kir.addMemo(memoKir1);

        try{
           TimeUnit.MILLISECONDS.sleep(10);
        }catch(InterruptedException e){
            e.printStackTrace();
        }
        
        Memo memoKir2 = new Memo(joelEMP);
        memoKir2.setResponsible(fiddyEMP);
        memoKir2.setCustomer(gustavCustKir);
        memoRegistry.create(memoKir2);
        kir.addMemo(memoKir2);
        
        try{
           TimeUnit.MILLISECONDS.sleep(10);
        }catch(InterruptedException e){
            e.printStackTrace();
        }
        
        Memo memoKir3 = new Memo(joelEMP);
        memoKir3.setResponsible(fiddyEMP);
        memoKir3.setCustomer(gustavCustKir);
        memoRegistry.create(memoKir3);
        kir.addMemo(memoKir3);

        try{
           TimeUnit.MILLISECONDS.sleep(10);
        }catch(InterruptedException e){
            e.printStackTrace();
        }
        
        Memo memoKir4 = new Memo(joelEMP);
        memoKir4.setResponsible(fiddyEMP);
        memoKir4.setCustomer(ragnarCustKir);
        memoRegistry.create(memoKir4);
        kir.addMemo(memoKir4);

        try{
           TimeUnit.MILLISECONDS.sleep(10);
        }catch(InterruptedException e){
            e.printStackTrace();
        }
        
        Memo memoKir5 = new Memo(joelEMP);
        memoKir5.setResponsible(fiddyEMP);
        memoKir5.setCustomer(ragnarCustKir);
        memoRegistry.create(memoKir5);
        kir.addMemo(memoKir5);

        storeRegistry.update(kir);

        List<Memo> kirMemoListGustav = memoRegistry.findRangeOfMemoWithCustomer(gustavCustKir.getId(), 0, 5);

        Assert.assertTrue(kirMemoListGustav.size() == 3);
        Assert.assertTrue(kirMemoListGustav.get(0).equals(memoKir1));
        Assert.assertTrue(kirMemoListGustav.get(1).equals(memoKir2));
        Assert.assertTrue(kirMemoListGustav.get(2).equals(memoKir3));

        List<Memo> kirMemoListRagnar = memoRegistry.findRangeOfMemoWithCustomer(ragnarCustKir.getId(), 0, 5);

        Assert.assertTrue(kirMemoListRagnar.size() == 2);
        Assert.assertTrue(kirMemoListRagnar.get(0).equals(memoKir4));
        Assert.assertTrue(kirMemoListRagnar.get(1).equals(memoKir5));

        List<Memo> kirMemoListOrderGustav = memoRegistry.findRangeOfMemoWithCustomer(gustavCustKir.getId(), 0, 2);

        Assert.assertTrue(kirMemoListOrderGustav.size() == 2);
        Assert.assertTrue(kirMemoListOrderGustav.get(0).equals(memoKir2));
        Assert.assertTrue(kirMemoListOrderGustav.get(1).equals(memoKir3));
    }
    
    @Test
    public void testDeleteMemo(){
        Organization org = new Organization("DeleteOrg");
        organizationRegistry.create(org);
        
        Store store = new Store("DeleteStore", org);
        Set<ViolationWrapper> violationsStore = storeRegistry.create(store);
        assertTrue(violationsStore.isEmpty());

        Employee creator = new Employee("Creator", "DELETE", "CD", "6632", store);
        Employee responsible = new Employee("Responsible", "DELETE", "RD", "1119", store);
        Set<ViolationWrapper> violationsEmpl = employeeRegistry.create(creator);
        assertTrue(violationsEmpl.isEmpty());
        violationsEmpl = employeeRegistry.create(responsible);
        assertTrue(violationsEmpl.isEmpty());

        Memo memo = new Memo(creator);
        memo.setResponsible(responsible);
        Set<ViolationWrapper> violationsMemo = memoRegistry.create(memo);
        assertTrue(violationsMemo.isEmpty());
        
        store.addMemo(memo);
        
        violationsStore = storeRegistry.update(store);
        assertTrue(violationsStore.isEmpty());

        Memo memoResult = memoRegistry.find(memo.getId());
        Assert.assertTrue(memo.equals(memoResult));

        memoResult.getItem().setName("Borste");
        violationsMemo = memoRegistry.update(memoResult);
        assertTrue(violationsMemo.isEmpty());

        memoResult = memoRegistry.find(memo.getId());
        Assert.assertTrue(memoResult.getItem().getName().equals("Borste"));

        Order order = new Order(responsible);
        order.setOrderId("IKP982332");
        orderRegistry.create(order);

        memo.setOrder(order);
        memoRegistry.update(memo);//Should cascade order

        Reception reception = new Reception(creator);
        reception.setIsCostumerNotified(true);
        reception.setRemarks("The item is recieved. I called the customer and notified him");
        receptionRegistry.create(reception);

        memo.setReception(reception);
        memoRegistry.update(memo);//Should cascade order

        memoResult = memoRegistry.find(memo.getId());

        Assert.assertTrue(memoResult.getOrder().equals(order));
        Assert.assertTrue(memoResult.getReception().equals(reception));
        
        Reminder reminder = new Reminder(responsible, "Remind me that I'm aswesome", memo, new Date(), Reminder.Type.EMPLOYEE_CONFIRM);
        reminderRegistry.create(reminder);
        
        Reminder rem = reminderRegistry.find(reminder.getId());
        assertTrue(rem.equals(reminder));
        
        memoRegistry.delete(memo.getId());
        
        List<Memo> memos = memoRegistry.findRangeOfMemoForStore(store.getId(), 0, 10);
        assertTrue(memos.isEmpty());
        
        Order resOrder = orderRegistry.find(order.getId());
        assertTrue(resOrder == null);
        
        Reception resReception = receptionRegistry.find(reception.getId());
        assertTrue(resReception == null);
        
    }
    
    @Test
    public void testBehaviorJPACascade(){
        Organization org = new Organization("test2");
        Set<ViolationWrapper> violations = organizationRegistry.create(org);
        assertTrue(violations.isEmpty());
        
        Store store = new Store("test2", org);
        violations = storeRegistry.create(store);
        assertTrue(violations.isEmpty());

        Employee creator = new Employee("test2", "test2", "TT", "65456", store);
        violations = employeeRegistry.create(creator);
        assertTrue(violations.isEmpty());
        
        Memo memo = new Memo(creator);
        memo.setResponsible(creator);
        violations = memoRegistry.create(memo);
        assertTrue(violations.isEmpty());
        
        Order order = new Order(creator);
        orderRegistry.create(order); // REsult: It doesnt cascade a persist on an update so we have to use create
        memo.setOrder(order);
        violations = memoRegistry.update(memo);
        assertTrue(violations.isEmpty());
        
        
        Memo memo2 = memoRegistry.find(memo.getId());
        assertTrue(memo2.equals(memo));
        assertTrue(memo2.getOrder().equals(order));
    }
    
    @Test
    public void testSearch(){
         Organization org = new Organization("Search Org");
        Set<ViolationWrapper> violations = organizationRegistry.create(org);
        assertTrue(violations.isEmpty());
        
        Store mar = new Store("MariestadSearch",org);
        violations = storeRegistry.create(mar);
        assertTrue(violations.isEmpty());

        Employee creator = new Employee("Jacob", "Larsson", "JL", "1122", mar);
        Employee responsible = new Employee("Thor", "Gautsson", "TG", "2233", mar);
        violations = employeeRegistry.create(creator);
        assertTrue(violations.isEmpty());
        violations = employeeRegistry.create(responsible);
        assertTrue(violations.isEmpty());
        
        Customer customer = new Customer("Jake", "Lake", "0703477330", mar);
        customer.setAssociation("skhgfakl kjsdhfl k sss skfjalkdhf");
        violations = customerRegistry.create(customer);
        assertTrue(violations.isEmpty());

        Memo memo = new Memo(creator);
        memo.setCustomer(customer);
        memo.setIsReclaim(true);
        memo.setResponsible(responsible);
        memo.setFreeText("jag sKa testa dETTa");
        violations = memoRegistry.create(memo);
        assertTrue(violations.isEmpty());
        
        Memo memo1 = new Memo(responsible);
        memo1.setIsReclaim(true);
        memo1.setResponsible(creator);
        memo1.setFreeText("Du suger detta");
        violations = memoRegistry.create(memo1);
        assertTrue(violations.isEmpty());
        
//        try{
//           TimeUnit.MILLISECONDS.sleep(500);
//        }catch(InterruptedException e){
//            e.printStackTrace();
//        }
        
//        Date.from(Instant.now().minus(3, ChronoUnit.MINUTES))
        List<Memo> memos = memoRegistry.searchRangeOfMemos("sss", null, memo.getCreator().getId(), memo.getResponsible().getId(), 0, Memo.Status.CREATED, true, 0, 10);
        logger.debug("size: " + memos.size());
        assertTrue(memos.size() == 1 && memos.get(0).equals(memo));
        
        memos = memoRegistry.searchRangeOfMemos("Detta", null, 0, 0, 0, Memo.Status.CREATED, memo.isIsReclaim(), 0, 10);
        assertTrue(memos.size() == 2 && memos.contains(memo) && memos.contains(memo1));
        
        
        memos = memoRegistry.searchRangeOfMemos("Detta", null, memo.getCreator().getId(), 0, 0, Memo.Status.CREATED, memo.isIsReclaim(), 0, 10);
        assertTrue(memos.size() == 1 && memos.contains(memo));
        
        
        memos = memoRegistry.searchRangeOfMemos(null, null, 0, 0, 0, null, null, 0, 10);
        assertTrue(memos.size() > 0);
        
        memos = memoRegistry.searchRangeOfMemos("uGer", null, 0, 0, 0, null, null, 0, 10);
        assertTrue(memos.size() == 1 && memos.contains(memo1));
    }
}
