package com.gstrs.webmemo.persistence.test;

import com.gstrs.webmemo.model.entities.Conversation;
import com.gstrs.webmemo.model.entities.Employee;
import com.gstrs.webmemo.model.entities.Message;
import com.gstrs.webmemo.model.entities.Organization;
import com.gstrs.webmemo.model.entities.Permission;
import com.gstrs.webmemo.model.entities.Person;
import com.gstrs.webmemo.model.entities.Provider;
import com.gstrs.webmemo.model.entities.Salesman;
import com.gstrs.webmemo.model.entities.Store;
import com.gstrs.webmemo.model.entities.User;
import com.gstrs.webmemo.persistence.IOrganizationRegistry;
import com.gstrs.webmemo.persistence.IWebMemo;
import com.gstrs.webmemo.persistence.validation.ViolationWrapper;
import java.util.List;
import java.util.Set;
import javax.ejb.EJB;
import junit.framework.Assert;
import static junit.framework.Assert.*;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 *
 * @author Jacob
 */
@RunWith(Arquillian.class)
public class OrganizationTest {

    @Deployment
    public static Archive<?> createDeployment() {
        return ShrinkWrap.create(WebArchive.class, "webmemo.war")
                .addPackage("com.gstrs.webmemo.persistence")
                .addPackage("com.gstrs.webmemo.persistence.impl")
                .addAsResource("test-persistence.xml", "META-INF/persistence.xml")
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");

    }

    @Before
    public void before() throws Exception {
    }
    
    final static Logger logger = LoggerFactory.getLogger(OrganizationTest.class);
    
    @EJB
    private IOrganizationRegistry organizationRegistry;
    @EJB
    private IWebMemo webMemo;
    @Test
    public void testCreateOrganization(){
        Organization org = new Organization("Wiktorsson");
        organizationRegistry.create(org);
        
        Organization orgRes = organizationRegistry.find(org.getId());
        Assert.assertTrue(org.equals(orgRes));
    }
    
    @Test
    public void testValidation(){
        Organization org = new Organization("bertil & CO");
        Set<ViolationWrapper> violations = organizationRegistry.create(org);
        assertTrue(violations.isEmpty());
        
        Organization org2 = new Organization("bertil & CO");
        violations = organizationRegistry.create(org2);
        assertTrue(violations.size() > 0);
        
        Organization org3 = new Organization("bertil & co");
        violations = organizationRegistry.create(org3);
        assertTrue(violations.size() > 0);
    }
    
    @Test
    public void testFindRangeOfStoreForOrganization(){
        Organization org1 = new Organization("org1");
        Set<ViolationWrapper> violations = organizationRegistry.create(org1);
        assertTrue(violations.isEmpty());
        
        Organization org2 = new Organization("org2");
        violations = organizationRegistry.create(org2);
        assertTrue(violations.isEmpty());
        
        Store store1 = new Store("findrange store1", org1);
        Set<ViolationWrapper> violationsStore = webMemo.getStoreRegistry().create(store1);
        assertTrue(violationsStore.isEmpty());
        org1.addStore(store1);
        
        Store store2 = new Store("findrange store2", org1);
        violationsStore = webMemo.getStoreRegistry().create(store2);
        assertTrue(violationsStore.isEmpty());
        org1.addStore(store2);
        
        Store store3 = new Store("findrange store3", org2);
        violationsStore = webMemo.getStoreRegistry().create(store3);
        assertTrue(violationsStore.isEmpty());
        org1.addStore(store3);
        
        Store store4 = new Store("findrange store4", org1);
        violationsStore = webMemo.getStoreRegistry().create(store4);
        assertTrue(violationsStore.isEmpty());
        org1.addStore(store4);
        
        violations = webMemo.getOrganizationRegistry().update(org1);
        assertTrue(violations.isEmpty());
        
        violations = webMemo.getOrganizationRegistry().update(org2);
        assertTrue(violations.isEmpty());
        
        List<Store> storeListRes = webMemo.getStoreRegistry().findRangeOfStoreForOrganization(org1.getId(), 0, 5);
        assertTrue(storeListRes.size() == 3);
        assertTrue(storeListRes.get(0).equals(store1));
        assertTrue(storeListRes.get(1).equals(store2));
        assertTrue(storeListRes.get(2).equals(store4));
        
        storeListRes = webMemo.getStoreRegistry().findRangeOfStoreForOrganization(org2.getId(), 0, 5);
        assertTrue(storeListRes.size() == 1);
        assertTrue(storeListRes.get(0).equals(store3));
    }
    
    @Test
    public void testDeleteStore(){
        Organization org = new Organization("deleteStoreOrg");
        Set<ViolationWrapper> violations = organizationRegistry.create(org);
        assertTrue(violations.isEmpty());
        
        Store store = new Store("deleteStoreStore", org);
        Set<ViolationWrapper> violationsStore = webMemo.getStoreRegistry().create(store);
        assertTrue(violationsStore.isEmpty());
        org.addStore(store);
        
        
        Store store2 = new Store("deleteStoreStore2", org);
        violationsStore = webMemo.getStoreRegistry().create(store2);
        assertTrue(violationsStore.isEmpty());
        org.addStore(store2);
        
        violations = webMemo.getOrganizationRegistry().update(org);
        assertTrue(violations.isEmpty());
        
        
        violationsStore = webMemo.getStoreRegistry().delete(store.getId());
        assertTrue(violationsStore.isEmpty());
        
        List<Store> storeRes = webMemo.getStoreRegistry().findRangeOfStoreForOrganization(org.getId(), 0, 5);
        assertTrue(storeRes.size() == 1);
        assertTrue(storeRes.get(0).equals(store2));
        
        Employee employee = new Employee("Jeanette", "Karlsson", "JK1", "6666", store2);
        Set<ViolationWrapper> violationsEmpl = webMemo.getEmployeeRegistry().create(employee);
        assertTrue(violationsEmpl.isEmpty());
        
        violationsStore = webMemo.getStoreRegistry().delete(store2.getId());
        assertTrue(violationsStore.size() == 1);
        assertFalse(violationsStore.iterator().next().isField());
        
        
    }
    
    @Test
    public void testFindRangeOfOrganizationForUser(){
        User user1 = new User("testfindrangeorguser", "password123", Permission.ADMINISTRATOR);
        Set<ViolationWrapper> violationsUser = webMemo.getUserRegistry().create(user1);
        assertTrue(violationsUser.isEmpty());
        
        Organization org = new Organization("borga1");
        Set<ViolationWrapper> violations = organizationRegistry.create(org);
        assertTrue(violations.isEmpty());
        user1.addOrganization(org);
        
        Organization org2 = new Organization("aorga2");
        violations = organizationRegistry.create(org2);
        assertTrue(violations.isEmpty());
        user1.addOrganization(org2);
        
        Organization org3 = new Organization("corga3");
        violations = organizationRegistry.create(org3);
        assertTrue(violations.isEmpty());
        user1.addOrganization(org3);
        
        violationsUser = webMemo.getUserRegistry().update(user1);
        assertTrue(violationsUser.isEmpty());
        
        List<Organization> list = organizationRegistry.findRangeOfOrganizationForUser(user1, 0, 10);
        
        assertTrue(list.size() == 3);
        assertTrue(list.get(0).equals(org2));
        assertTrue(list.get(1).equals(org));
        assertTrue(list.get(2).equals(org3));
        
        list = organizationRegistry.findRangeOfOrganizationForUser(user1, 0, 2);
        
        assertTrue(list.size() == 2);
        assertTrue(list.get(0).equals(org2));
        assertTrue(list.get(1).equals(org));
        
        
    }
    
    @Test
    public void testDeleteOrganization(){
        User user1 = new User("testdeleteorg", "password123", Permission.ADMINISTRATOR);
        Set<ViolationWrapper> violationsUser = webMemo.getUserRegistry().create(user1);
        assertTrue(violationsUser.isEmpty());
        
        Organization org = new Organization("organization1");
        Set<ViolationWrapper> violations = organizationRegistry.create(org);
        assertTrue(violations.isEmpty());
        user1.addOrganization(org);
        
        Organization org2 = new Organization("organization2");
        violations = organizationRegistry.create(org2);
        assertTrue(violations.isEmpty());
        user1.addOrganization(org2);
        
        violationsUser = webMemo.getUserRegistry().update(user1);
        assertTrue(violationsUser.isEmpty());
        
        Store store = new Store("testdeleteorg", org);
        Set<ViolationWrapper> violationsStore = webMemo.getStoreRegistry().create(store);
        assertTrue(violationsStore.isEmpty());
        
        List<Organization> orgRes2 = webMemo.getOrganizationRegistry().findRangeOfOrganizationForUser(user1, 0, 10);
        assertTrue(orgRes2.contains(org));
        assertTrue(orgRes2.contains(org2));
        
        violations = webMemo.getOrganizationRegistry().delete(org.getId());
        assertTrue(violations.size() > 0);
        
        violations = webMemo.getStoreRegistry().delete(store.getId());
        assertTrue(violations.isEmpty());
        
        Provider provider = new Provider("Cross", "CROSS", org);
        violations = webMemo.getProviderRegistry().create(provider);
        assertTrue(violations.isEmpty());
        
        violations = webMemo.getOrganizationRegistry().delete(org.getId());
        assertTrue(violations.size() > 0);
        
        violations = webMemo.getProviderRegistry().delete(provider.getId());
        assertTrue(violations.isEmpty());
        
        violations = webMemo.getOrganizationRegistry().delete(org.getId());
        assertTrue(violations.isEmpty());
        
        orgRes2 = webMemo.getOrganizationRegistry().findRangeOfOrganizationForUser(user1, 0, 10);
        assertTrue(orgRes2.contains(org2));
        assertTrue(orgRes2.size() == 1);
        
        webMemo.getOrganizationRegistry().delete(org2.getId());
        
        orgRes2 = webMemo.getOrganizationRegistry().findRangeOfOrganizationForUser(user1, 0, 10);
        assertTrue(orgRes2.isEmpty());
        
    }
    
    @Test
    public void testFindRangeOfParticipantsForConversation(){
        
        Salesman salesman1 = new Salesman("sale1", "my", "0703477893");
        Set<ViolationWrapper> violations = webMemo.getSalesmanRegistry().create(salesman1);
        assertTrue(violations.isEmpty());
        
        Salesman salesman2 = new Salesman("sale2", "my", "0703477893");
        violations = webMemo.getSalesmanRegistry().create(salesman2);
        assertTrue(violations.isEmpty());
        
        Salesman salesman3 = new Salesman("sale3", "my", "0703477893");
        violations = webMemo.getSalesmanRegistry().create(salesman3);
        assertTrue(violations.isEmpty());
        
        Message message1 = new Message(salesman1, "Hello everyone");
        violations = webMemo.getMessageRegistry().create(message1);
        assertTrue(violations.isEmpty());
        
        Conversation conv = new Conversation(salesman1, salesman2, message1);
        conv.addParticipant(salesman3);
        violations = webMemo.getConversationRegistry().create(conv);
        assertTrue(violations.isEmpty());
        
        Conversation conv2 = new Conversation(salesman1, salesman2, message1);
        violations = webMemo.getConversationRegistry().create(conv2);
        assertTrue(violations.isEmpty());
        
        List<Person> participants = webMemo.getConversationRegistry().findRangeOfParticipantsForConversation(conv.getId(), 0, 10);
        assertTrue(participants.size() == 3);
        assertTrue(participants.contains(salesman1));
        assertTrue(participants.contains(salesman2));
        assertTrue(participants.contains(salesman3));
        
        participants = webMemo.getConversationRegistry().findRangeOfParticipantsForConversation(conv2.getId(), 0, 10);
        assertTrue(participants.size() == 2);
        assertTrue(participants.contains(salesman1));
        assertTrue(participants.contains(salesman2));
    }
}
