package com.gstrs.webmemo.persistence.test;

import com.gstrs.webmemo.model.entities.Permission;
import static com.gstrs.webmemo.model.entities.Permission.ADMINISTRATOR;
import com.gstrs.webmemo.model.entities.User;
import com.gstrs.webmemo.persistence.IOrganizationRegistry;
import com.gstrs.webmemo.persistence.IUserRegistry;
import com.gstrs.webmemo.persistence.validation.ViolationWrapper;
import java.util.Set;
import javax.ejb.EJB;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 *
 * @author Jacob
 */
@RunWith(Arquillian.class)
public class UserAndPermissionTest {

    @Deployment
    public static Archive<?> createDeployment() {
        return ShrinkWrap.create(WebArchive.class, "webmemo.war")
                .addPackage("com.gstrs.webmemo.persistence")
                .addPackage("com.gstrs.webmemo.persistence.impl")
                .addAsResource("test-persistence.xml", "META-INF/persistence.xml")
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsResource("ValidationMessages.properties");

    }
    
    final static Logger logger = LoggerFactory.getLogger(UserAndPermissionTest.class);
    
    @Before
    public void before() throws Exception {
        
    }

    @EJB
    private IUserRegistry userRegistry;

    @EJB
    private IOrganizationRegistry organizationRegistry;

    @Test
    public void testUserGetByUsername() throws Exception {

        User user = new User("jake123", "password1234", Permission.ORGANIZATIONOWNER);

        userRegistry.create(user);

        User userResult1 = userRegistry.find(user.getId());
        assertTrue(userResult1.getUsername().equals("jake123"));

        User userResult2 = userRegistry.getByUsername("jake123");
        assertTrue(userResult2.getUsername().equals("jake123"));
    }

    @Test
    public void testCreatingSeveralUsers() throws Exception {
        
        String[] array = {"jacob", "david", "ivar_huni", "thorvaldur"};
        for (String s : array) {
            userRegistry.create(new User(s, "password1234", ADMINISTRATOR));
        }
    }

    @Test
    public void testUpdateAUser() throws Exception {
        User user = new User("bertil23", "password1234", Permission.ORGANIZATIONOWNER);

        userRegistry.create(user);

        user.setUsername("123bertil");
        userRegistry.update(user);

        User userResult = userRegistry.find(user.getId());

        assertTrue(userResult.getUsername().equals("123bertil"));
    }

    @Test
    public void testProductDelete() throws Exception {
        User user = new User("bosse", "password1234", Permission.ORGANIZATIONOWNER);
        userRegistry.create(user);
        userRegistry.delete(user.getId());
        assertTrue(null == userRegistry.find(user.getId()));
    }

    @Test
    public void testFindAll() throws Exception {
        int count = userRegistry.count();
        int actual = userRegistry.findAll().size();
        assertTrue(count == actual);
    }
    
    @Test
    public void testValidation() throws Exception {
        User user1 = new User();
        user1.setUsername("jake9191");
        user1.setPassword("kajfhlkjshad");
        user1.setPermission(ADMINISTRATOR);
        Set<ViolationWrapper> violations = userRegistry.create(user1);
        
        assertTrue(violations.isEmpty());
        User user2 = new User();
        user2.setUsername("jake9191");
        user2.setPassword("kajfhlkjshad");
        user2.setPermission(ADMINISTRATOR);
        violations = userRegistry.create(user2);
        assertTrue(violations.size() > 0);
        assertTrue(violations.size() == 1);
        assertTrue(violations.iterator().next().isField());
    }
}