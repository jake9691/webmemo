package com.gstrs.webmemo.persistence.test;

import com.gstrs.webmemo.model.entities.Employee;
import com.gstrs.webmemo.model.entities.Organization;
import com.gstrs.webmemo.model.entities.Reminder;
import com.gstrs.webmemo.model.entities.Store;
import com.gstrs.webmemo.persistence.IEmployeeRegistry;
import com.gstrs.webmemo.persistence.IOrganizationRegistry;
import com.gstrs.webmemo.persistence.IReminderRegistry;
import com.gstrs.webmemo.persistence.IStoreRegistry;
import java.util.Date;
import javax.ejb.EJB;
import junit.framework.Assert;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 *
 * @author Jacob
 */
@RunWith(Arquillian.class)
public class ReminderTest {

    @Deployment
    public static Archive<?> createDeployment() {
        return ShrinkWrap.create(WebArchive.class, "webmemo.war")
                .addPackage("com.gstrs.webmemo.persistence")
                .addPackage("com.gstrs.webmemo.persistence.impl")
                .addAsResource("test-persistence.xml", "META-INF/persistence.xml")
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");

    }

    @Before
    public void before() throws Exception {
    }

    final static Logger logger = LoggerFactory.getLogger(ReminderTest.class);

    @EJB
    private IReminderRegistry reminderRegistry;
    @EJB
    private IStoreRegistry storeRegistry;
    @EJB
    private IEmployeeRegistry employeeRegistry;
    @EJB
    private IOrganizationRegistry organizationRegistry;
    
    
    @Test
    public void testCreateReminder() {
        Organization org = new Organization("Kanelbulle");
        organizationRegistry.create(org);
        Store karl = new Store("Karlskoga2", org);
        storeRegistry.create(karl);
        Employee sven = new Employee("Sven-Olaf", "Larsson", "SL", "1622", karl);
        employeeRegistry.create(sven);
        Reminder reminder = new Reminder(sven, "remember bla", new Date());
        reminderRegistry.create(reminder);

        Reminder rem = reminderRegistry.find(reminder.getId());
        Assert.assertTrue(rem.equals(reminder));
    }
}
