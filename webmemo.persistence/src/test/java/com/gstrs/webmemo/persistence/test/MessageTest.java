package com.gstrs.webmemo.persistence.test;

import com.gstrs.webmemo.model.entities.Conversation;
import com.gstrs.webmemo.model.entities.Customer;
import com.gstrs.webmemo.model.entities.Employee;
import com.gstrs.webmemo.model.entities.Message;
import com.gstrs.webmemo.model.entities.Organization;
import com.gstrs.webmemo.model.entities.Store;
import com.gstrs.webmemo.persistence.IConversationRegistry;
import com.gstrs.webmemo.persistence.ICustomerRegistry;
import com.gstrs.webmemo.persistence.IEmployeeRegistry;
import com.gstrs.webmemo.persistence.IMessageRegistry;
import com.gstrs.webmemo.persistence.IOrganizationRegistry;
import com.gstrs.webmemo.persistence.IStoreRegistry;
import com.gstrs.webmemo.persistence.validation.ViolationWrapper;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import javax.ejb.EJB;
import static junit.framework.Assert.*;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 *
 * @author Jacob
 */
@RunWith(Arquillian.class)
public class MessageTest {

    @Deployment
    public static Archive<?> createDeployment() {
        return ShrinkWrap.create(WebArchive.class, "webmemo.war")
                .addPackage("com.gstrs.webmemo.persistence")
                .addPackage("com.gstrs.webmemo.persistence.impl")
                .addAsResource("test-persistence.xml", "META-INF/persistence.xml")
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");

    }

    @EJB
    private IStoreRegistry storeRegistry;
    @EJB
    private IEmployeeRegistry employeeRegistry;
    @EJB
    private ICustomerRegistry customerRegistry;
    @EJB
    private IMessageRegistry messageRegistry;
    @EJB
    private IConversationRegistry conversationRegistry;
    @EJB
    private IOrganizationRegistry organizationRegistry;
    
    final static Logger logger = LoggerFactory.getLogger(MessageTest.class);

    @Test
    public void testCreateMessageConversation() {
        Organization org = new Organization("Gforce");
        Set<ViolationWrapper> violationsOrg = organizationRegistry.create(org);
        assertTrue(violationsOrg.isEmpty());
        
        Store kls = new Store("Karlstad", org);
        Set<ViolationWrapper> violationsStore = storeRegistry.create(kls);
        assertTrue(violationsStore.isEmpty());
        
        Employee employee = new Employee("Bertil", "Larsson", "BL", "9122", kls);
        Set<ViolationWrapper> violationsEmpl = employeeRegistry.create(employee);
        assertTrue(violationsEmpl.isEmpty());
        Customer customer = new Customer("Håkan", "Bråkan", "0703477630", kls);
        Set<ViolationWrapper> violationsCust = customerRegistry.create(customer);
        assertTrue(violationsCust.isEmpty());
        Message message = new Message(employee, "Hej allihopa. Vad är det för mat på L's kitchen idag?");
        Set<ViolationWrapper> violationsMessage = messageRegistry.create(message);
        Conversation conversation = new Conversation(employee, customer, message);
        customer.addConversation(conversation);
        employee.addConversation(conversation);
        Set<ViolationWrapper> violationsConv = conversationRegistry.create(conversation);
        assertTrue(violationsConv.isEmpty());

        Conversation convResult = conversationRegistry.find(conversation.getId());
        Assert.assertTrue(convResult.equals(conversation));
        Assert.assertTrue(messageRegistry.findRangeWithMessagesForConversation(convResult.getId(), 0, 1).get(0).equals(message));
    }

    @Test
    public void testOrderOfMessages() {
        Organization org = new Organization("Hat");
        organizationRegistry.create(org);
        
        Store gbg = new Store("Göteborg", org);
        storeRegistry.create(gbg);
        Employee employee = new Employee("Sune", "Karlsson", "SK", "9172", gbg);
        employeeRegistry.create(employee);
        Customer customer = new Customer("Robert", "Gustavsson", "0703477630", gbg);
        customerRegistry.create(customer);

        Message message1 = new Message(employee, "Tjenare robert!!");
        messageRegistry.create(message1);
        
        try{
           TimeUnit.MILLISECONDS.sleep(10);
        }catch(InterruptedException e){
            e.printStackTrace();
        }
        

        Message message2 = new Message(customer, "Hej Sune. Vad vill du då?");
        messageRegistry.create(message2);
        
        try{
           TimeUnit.MILLISECONDS.sleep(10);
        }catch(InterruptedException e){
            e.printStackTrace();
        }

        Message message3 = new Message(employee, "Tänkte meddela att dina skridskor har kommit.");
        messageRegistry.create(message3);

        Conversation conversation = new Conversation(employee, customer, message1);
        conversation.addMessage(message3);
        conversation.addMessage(message2);
        conversationRegistry.create(conversation);

        customer.addConversation(conversation);
        employee.addConversation(conversation);

        customerRegistry.update(customer);
        employeeRegistry.update(employee);

        Conversation convResult = conversationRegistry.find(conversation.getId());

        List<Message> messageList = messageRegistry.findRangeWithMessagesForConversation(convResult.getId(), 0, 3);
        boolean isFirst = true;
        Date date1 = null;
        for (Message m : messageList) {
            if (isFirst) {
                date1 = m.getDateCreated();
                isFirst = false;
            } else {
                Assert.assertTrue(date1.compareTo(m.getDateCreated()) < 0);
            }
        }
        messageList = messageRegistry.findRangeWithMessagesForConversation(convResult.getId(), 0, 2);
        Assert.assertTrue(messageList.get(0).equals(message2));
        Assert.assertTrue(messageList.get(1).equals(message3));
    }
    
    @Test
    public void testDeleteMessageAndConversation(){
        Organization org = new Organization("deleteMessageOrg");
        Set<ViolationWrapper> violationsOrg = organizationRegistry.create(org);
        assertTrue(violationsOrg.isEmpty());
        
        Store store = new Store("deleteMessageStore", org);
        Set<ViolationWrapper> violationsStore = storeRegistry.create(store);
        assertTrue(violationsStore.isEmpty());
        
        Employee employee = new Employee("Bertil", "DeleteEmployeeMess", "BD", "9122", store);
        Set<ViolationWrapper> violationsEmpl = employeeRegistry.create(employee);
        assertTrue(violationsEmpl.isEmpty());
        Customer customer = new Customer("Håkan", "deleteCustomerMess", "0703477630", store);
        Set<ViolationWrapper> violationsCust = customerRegistry.create(customer);
        assertTrue(violationsCust.isEmpty());
        
        Message message = new Message(employee, "är det någon rolig på gång?");
        Set<ViolationWrapper> violationsMessage = messageRegistry.create(message);
        assertTrue(violationsMessage.isEmpty());
        
        try{
           TimeUnit.MILLISECONDS.sleep(10);
        }catch(InterruptedException e){
            e.printStackTrace();
        }
        
        Message message2 = new Message(customer, "Ja då. Vi gör kaos på Yaki Da");
        violationsMessage = messageRegistry.create(message2);
        assertTrue(violationsMessage.isEmpty());
        
        try{
           TimeUnit.MILLISECONDS.sleep(10);
        }catch(InterruptedException e){
            e.printStackTrace();
        }
        
        Message message3 = new Message(employee, "Vad bra. Jag kommer!!!");
        violationsMessage = messageRegistry.create(message3);
        assertTrue(violationsMessage.isEmpty());
        
        Conversation conversation = new Conversation(employee, customer, message);
        customer.addConversation(conversation);
        employee.addConversation(conversation);
        conversation.addMessage(message2);
        conversation.addMessage(message3);
        Set<ViolationWrapper> violationsConv = conversationRegistry.create(conversation);
        assertTrue(violationsConv.isEmpty());
        
        violationsEmpl = employeeRegistry.update(employee);
        assertTrue(violationsEmpl.isEmpty());
        violationsCust = customerRegistry.update(customer);
        assertTrue(violationsCust.isEmpty());
        
        List<Conversation> listConvRes = conversationRegistry.findRangeOfConversationsForPerson(customer.getId(), 0, 10);
        assertTrue(listConvRes.size() == 1);
        listConvRes = conversationRegistry.findRangeOfConversationsForPerson(employee.getId(), 0, 10);
        assertTrue(listConvRes.size() == 1);
        
        
        Conversation convResult = conversationRegistry.find(conversation.getId());
        Assert.assertTrue(convResult.equals(conversation));
        Assert.assertTrue(messageRegistry.findRangeWithMessagesForConversation(convResult.getId(), 0, 3).get(0).equals(message));
        
        messageRegistry.delete(message2.getId());
        
        Message messageRes2 = messageRegistry.find(message2.getId());
        assertTrue(messageRes2 == null);
        messageRes2 = messageRegistry.find(message.getId());
        assertTrue(messageRes2.equals(message));
        messageRes2 = messageRegistry.find(message3.getId());
        assertTrue(messageRes2.equals(message3));
        
        conversationRegistry.delete(conversation.getId());
        
        Message messageRes = messageRegistry.find(message.getId());
        assertTrue(messageRes == null);
        
        Message messageRes3 = messageRegistry.find(message3.getId());
        assertTrue(messageRes3 == null);
        
        Conversation convRes = conversationRegistry.find(conversation.getId());
        assertTrue(convRes == null);
        
        listConvRes = conversationRegistry.findRangeOfConversationsForPerson(employee.getId(), 0, 10);
        assertTrue(listConvRes.isEmpty());
        
        listConvRes = conversationRegistry.findRangeOfConversationsForPerson(customer.getId(), 0, 10);
        assertTrue(listConvRes.isEmpty());
    }
}
