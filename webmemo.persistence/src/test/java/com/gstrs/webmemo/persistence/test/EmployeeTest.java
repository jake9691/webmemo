package com.gstrs.webmemo.persistence.test;

import com.gstrs.webmemo.model.entities.Conversation;
import com.gstrs.webmemo.model.entities.Employee;
import com.gstrs.webmemo.model.entities.Memo;
import com.gstrs.webmemo.model.entities.Message;
import com.gstrs.webmemo.model.entities.Order;
import com.gstrs.webmemo.model.entities.Organization;
import com.gstrs.webmemo.model.entities.Person;
import com.gstrs.webmemo.model.entities.Reception;
import com.gstrs.webmemo.model.entities.Reminder;
import com.gstrs.webmemo.model.entities.Salesman;
import com.gstrs.webmemo.model.entities.Store;
import com.gstrs.webmemo.persistence.IWebMemo;
import com.gstrs.webmemo.persistence.validation.ViolationWrapper;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import javax.ejb.EJB;
import static junit.framework.Assert.*;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 *
 * @author Jacob
 */
@RunWith(Arquillian.class)
public class EmployeeTest {

    @Deployment
    public static Archive<?> createDeployment() {
        return ShrinkWrap.create(WebArchive.class, "webmemo.war")
                .addPackage("com.gstrs.webmemo.persistence")
                .addPackage("com.gstrs.webmemo.persistence.impl")
                .addAsResource("test-persistence.xml", "META-INF/persistence.xml")
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");

    }

    @Before
    public void before() throws Exception {
    }

    final static Logger logger = LoggerFactory.getLogger(EmployeeTest.class);

    
    @EJB
    private IWebMemo webmemo;
    
    @Test
    public void testMoveAllEmployeeFromStore1ToStore2(){
        Organization org = new Organization("MoveEmployeeOrg");
        Set<ViolationWrapper> violationsOrg = webmemo.getOrganizationRegistry().create(org);
        assertTrue(violationsOrg.isEmpty());
        
        Store store1 = new Store("MoveEmployeeStore1", org);
        Set<ViolationWrapper> violationsStore = webmemo.getStoreRegistry().create(store1);
        assertTrue(violationsStore.isEmpty());
        
        Store store2 = new Store("MoveEmployeeStore2", org);
        violationsStore = webmemo.getStoreRegistry().create(store2);
        assertTrue(violationsStore.isEmpty());
        
        Employee employee = new Employee("Jake", "Bake", "JB", "1122", store1);
        Set<ViolationWrapper> violationsEmpl = webmemo.getEmployeeRegistry().create(employee);
        assertTrue(violationsEmpl.isEmpty());

        Employee employee2 = new Employee("Jake", "Bake2", "JB2", "12", store1);
        violationsEmpl = webmemo.getEmployeeRegistry().create(employee2);
        assertTrue(violationsEmpl.isEmpty());
        
        Employee employee3 = new Employee("Jake", "Bake3", "JB3", "1234", store1);
        violationsEmpl = webmemo.getEmployeeRegistry().create(employee3);
        assertTrue(violationsEmpl.isEmpty());
        
        webmemo.getEmployeeRegistry().moveAllEmployeeFromStore1ToStore2(store1, store2);
        
        Employee empRes = webmemo.getEmployeeRegistry().find(employee.getId());
        assertTrue(empRes.getWorkingInStore().equals(store2));
        
        Employee empRes2 = webmemo.getEmployeeRegistry().find(employee2.getId());
        assertTrue(empRes2.getWorkingInStore().equals(store2));
        
        Employee empRes3 = webmemo.getEmployeeRegistry().find(employee3.getId());
        assertTrue(empRes3.getWorkingInStore().equals(store2));
        
    }

    @Test
    public void testValidation(){
        Organization org = new Organization("IvarCO");
        Set<ViolationWrapper> violationsOrg = webmemo.getOrganizationRegistry().create(org);
        assertTrue(violationsOrg.isEmpty());
        
        Store store = new Store("skövde", org);
        Set<ViolationWrapper> violationsStore = webmemo.getStoreRegistry().create(store);
        assertTrue(violationsStore.isEmpty());
        
        Employee employee = new Employee("Jake", "Bake", "JB", "1122", store);
        Set<ViolationWrapper> violationsEmpl = webmemo.getEmployeeRegistry().create(employee);
        assertTrue(violationsEmpl.isEmpty());
        
        Employee employee2 = new Employee("Jake", "Bake", "JB", "999999", store);
        violationsEmpl = webmemo.getEmployeeRegistry().create(employee2);
        assertTrue(violationsEmpl.size() > 0);
        
        Employee employee3 = new Employee("Jake", "Bake", "JB1", "1122", store);
        violationsEmpl = webmemo.getEmployeeRegistry().create(employee3);
        assertTrue(violationsEmpl.size() > 0);
        
        Employee employee4 = new Employee("Jake", "Bake", "JB2", "1", store);
        violationsEmpl = webmemo.getEmployeeRegistry().create(employee4);
        assertTrue(violationsEmpl.size() > 0);
        
        Employee employee5 = new Employee("Jake", "Bake", "JB3", "1234567", store);
        violationsEmpl = webmemo.getEmployeeRegistry().create(employee5);
        assertTrue(violationsEmpl.size() > 0);
    }
    
    @Test
    public void testDeleteValidation(){
        Organization org = new Organization("deleteEmployeeOrg");
        Set<ViolationWrapper> violationsOrg = webmemo.getOrganizationRegistry().create(org);
        assertTrue(violationsOrg.isEmpty());
        
        Store store = new Store("deleteEmployeeStore", org);
        Set<ViolationWrapper> violationsStore = webmemo.getStoreRegistry().create(store);
        assertTrue(violationsStore.isEmpty());
        
        Employee employee = new Employee("Jake", "Bake", "JB", "1122", store);
        Set<ViolationWrapper> violationsEmpl = webmemo.getEmployeeRegistry().create(employee);
        assertTrue(violationsEmpl.isEmpty());
        
        org.addStore(store);
        org.addEmployee(employee);
        
        violationsOrg = webmemo.getOrganizationRegistry().update(org);
        assertTrue(violationsOrg.isEmpty());
        
        Memo memo = new Memo(employee);
        memo.setResponsible(employee);
        Set<ViolationWrapper> violationsMemo = webmemo.getMemoRegistry().create(memo);
        assertTrue(violationsMemo.isEmpty());
        
        store.addMemo(memo);
        
        violationsStore = webmemo.getStoreRegistry().update(store);
        assertTrue(violationsStore.isEmpty());
        
        Order order = new Order(employee);
        Set<ViolationWrapper> violationsOrder = webmemo.getOrderRegistry().create(order);
        assertTrue(violationsOrder.isEmpty());
        
        memo.setOrder(order);
        
        Reception reception = new Reception(employee);
        Set<ViolationWrapper> violationsReception = webmemo.getReceptionRegistry().create(reception);
        assertTrue(violationsReception.isEmpty());
        memo.setReception(reception);
        
        violationsMemo = webmemo.getMemoRegistry().update(memo);
        assertTrue(violationsMemo.isEmpty());
        
        Reminder reminder = new Reminder(employee, "Hej", memo, new Date(), Reminder.Type.EMPLOYEE_CONFIRM);
        Set<ViolationWrapper> violationsReminder = webmemo.getReminderRegistry().create(reminder);
        assertTrue(violationsReminder.isEmpty());
        
        Message message = new Message(employee, "FUCK THSI");
        Set<ViolationWrapper> violationsMessage = webmemo.getMessageRegistry().create(message);
        assertTrue(violationsMessage.isEmpty());
        
        Conversation conv = new Conversation(employee, employee, message);
        Set<ViolationWrapper> violationsConv = webmemo.getConversationRegistry().create(conv);
        assertTrue(violationsConv.isEmpty());
        
        Message m = new Message(employee, "Hej");
        Set<ViolationWrapper> violations = webmemo.getMessageRegistry().create(m);
        assertTrue(violations.isEmpty());
        
        conv.addMessage(m);
        violationsConv = webmemo.getConversationRegistry().update(conv);
        assertTrue(violationsConv.isEmpty());
        
        employee.addConversation(conv);
        violationsEmpl = webmemo.getEmployeeRegistry().update(employee);
        assertTrue(violationsEmpl.isEmpty());
        
        
        violationsEmpl = webmemo.getEmployeeRegistry().delete(employee.getId());
        assertTrue(violationsEmpl.size() > 0);
        
        violationsMemo = webmemo.getMemoRegistry().delete(memo.getId());
        assertTrue(violationsMemo.isEmpty());
        
        violationsEmpl = webmemo.getEmployeeRegistry().delete(employee.getId());
        assertTrue(violationsEmpl.isEmpty());
        
        Employee employeeRes = webmemo.getEmployeeRegistry().find(employee.getId());
        assertTrue(employeeRes == null);
        
        Conversation convRes = webmemo.getConversationRegistry().find(conv.getId());
        assertTrue(convRes == null);
        
        Message messageRes = webmemo.getMessageRegistry().find(message.getId());
        assertTrue(messageRes == null);
    }
    
    @Test
    public void testDeleteEmployeeConversation(){
        Organization org = new Organization("deleteConverEmplMess");
        Set<ViolationWrapper> violations = webmemo.getOrganizationRegistry().create(org);
        assertTrue(violations.isEmpty());
        
        Store store = new Store("deleteconveMessEmpl", org);
        violations = webmemo.getStoreRegistry().create(store);
        assertTrue(violations.isEmpty());
        
        Employee employee1 = new Employee("emp1", "emp1", "EE1", "01", store);
        violations = webmemo.getEmployeeRegistry().create(employee1);
        assertTrue(violations.isEmpty());
        
        Employee employee2 = new Employee("emp2", "emp2", "EE2", "02", store);
        violations = webmemo.getEmployeeRegistry().create(employee2);
        assertTrue(violations.isEmpty());
        
        Salesman salesman = new Salesman("sale", "my", "0703477893");
        violations = webmemo.getSalesmanRegistry().create(salesman);
        assertTrue(violations.isEmpty());
        
        Message message1 = new Message(employee1, "Hello everyone");
        violations = webmemo.getMessageRegistry().create(message1);
        assertTrue(violations.isEmpty());
        
        Conversation conv = new Conversation(employee1, employee2, message1);
        conv.addParticipant(salesman);
        violations = webmemo.getConversationRegistry().create(conv);
        assertTrue(violations.isEmpty());
        
        
        try{
           TimeUnit.MILLISECONDS.sleep(10);
        }catch(InterruptedException e){
            e.printStackTrace();
        }
        
        Message message2 = new Message(employee2, "Yeah, I'm in :)");
        violations = webmemo.getMessageRegistry().create(message2);
        assertTrue(violations.isEmpty());
        
        try{
           TimeUnit.MILLISECONDS.sleep(10);
        }catch(InterruptedException e){
            e.printStackTrace();
        }
        
        Message message3 = new Message(salesman, "I'm also in");
        violations = webmemo.getMessageRegistry().create(message3);
        assertTrue(violations.isEmpty());
        
        conv.addMessage(message2);
        conv.addMessage(message3);
        violations = webmemo.getConversationRegistry().update(conv);
        assertTrue(violations.isEmpty());
        
        
        violations = webmemo.getSalesmanRegistry().delete(salesman.getId());
        assertTrue(violations.isEmpty());
        
        List<Message> messageListRes = webmemo.getMessageRegistry().findRangeWithMessagesForConversation(conv.getId(), 0, 10);
        assertTrue(messageListRes.size() == 3);
        assertTrue(messageListRes.get(0).equals(message1));
        assertTrue(messageListRes.get(0).getSender() != null);
        assertTrue(messageListRes.get(1).equals(message2));
        assertTrue(messageListRes.get(1).getSender() != null);
        assertTrue(messageListRes.get(2).equals(message3));
        assertTrue(messageListRes.get(2).getSender() == null);
        
        List<Person> participantList = webmemo.getConversationRegistry().findRangeOfParticipantsForConversation(conv.getId(), 0, 10);
        assertTrue(participantList.size() == 2);
        assertTrue(participantList.contains(employee1));
        assertTrue(participantList.contains(employee2));
        
    }
}
