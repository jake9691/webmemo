/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gstrs.webmemo.persistence;

import javax.ejb.EJB;
import javax.ejb.Local;

/**
 *
 * @author davida
 */
@Local
public interface IWebMemo {

    IConversationRegistry getConversationRegistry();

    ICustomerRegistry getCustomerRegistry();

    IEmployeeRegistry getEmployeeRegistry();

    IMemoRegistry getMemoRegistry();

    IMessageRegistry getMessageRegistry();

    IOrderRegistry getOrderRegistry();

    IOrganizationRegistry getOrganizationRegistry();

    IProviderRegistry getProviderRegistry();

    IReceptionRegistry getReceptionRegistry();

    IReminderRegistry getReminderRegistry();

    IStoreRegistry getStoreRegistry();

    IUserRegistry getUserRegistry();
    
    ISalesmanRegistry getSalesmanRegistry();

    @EJB
    void setConversationRegistry(IConversationRegistry conversationRegistry);

    @EJB
    void setCustomerRegistry(ICustomerRegistry customerRegistry);

    @EJB
    void setEmployeeRegistry(IEmployeeRegistry employeeRegistry);

    @EJB
    void setMemoRegistry(IMemoRegistry memoRegistry);

    @EJB
    void setMessageRegistry(IMessageRegistry messageRegistry);

    @EJB
    void setOrderRegistry(IOrderRegistry orderRegistry);

    @EJB
    void setOrganizationRegistry(IOrganizationRegistry organizationRegistry);

    @EJB
    void setProviderRegistry(IProviderRegistry providerRegistry);

    @EJB
    void setReceptionRegistry(IReceptionRegistry receptionRegistry);

    @EJB
    void setReminderRegistry(IReminderRegistry reminderRegistry);

    @EJB
    void setStoreRegistry(IStoreRegistry storeRegistry);

    @EJB
    void setUserRegistry(IUserRegistry userRegistry);
    
    @EJB
    void setSalesmanRegistry(ISalesmanRegistry salesmanRegistry);
}
