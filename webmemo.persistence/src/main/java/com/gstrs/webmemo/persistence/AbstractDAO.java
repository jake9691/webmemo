package com.gstrs.webmemo.persistence;

import com.gstrs.webmemo.persistence.validation.ViolationWrapper;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Here is the implementation of the general crud functionality. All the registries inherit from abstractDAO.
 * 
 * Executes the bean validation and could return a list with violations. If a violations is detected, no object is created.
 * 
 * @author Jacob
 * @param <T>
 * @param <K> 
 */
public abstract class AbstractDAO<T, K> implements IDAO<T, K> {

    private final Class<T> clazz;
    private List<Locale> localeList;

    final static Logger logger = LoggerFactory.getLogger(AbstractDAO.class);

    public AbstractDAO(Class<T> clazz) {
        this.clazz = clazz;
    }

    protected abstract EntityManager getEntityManager();
    
    /**
     * Do the validaiton for uniqiness and possible also other constraints not possible to do with bean validation or JPA/SQL
     * 
     * @param t
     * @return 
     */
    protected abstract Set<ViolationWrapper> validateConstraints(T t);
    
    /**
     * Validate that the object doesn't has object related to it that would cause an error when trying to delete.
     * 
     * @param t
     * @return 
     */
    protected abstract Set<ViolationWrapper> validateDeleteConstraints(T t);
    
    @Inject
    private ValidatorFactory validatorFactory;

    @Override
    public Set<ViolationWrapper> create(T t) {
        Validator validator = validatorFactory.getValidator();
        Set<ConstraintViolation<T>> constraintViolations = validator.validate(t);
        Set<ViolationWrapper> violWrapper = new HashSet<>();
        for(ConstraintViolation v : constraintViolations){
            violWrapper.add(new ViolationWrapper(v, localeList));
        }
        violWrapper.addAll(validateConstraints(t));
        if (violWrapper.isEmpty()) {
            getEntityManager().persist(t);
        } else {
            for (ViolationWrapper viol : violWrapper) {
                logger.info("violation on create: " + viol.getMessage());
                logger.info("   violationTemplate: " + viol.getMessageTemplate());
            }
        }
        
        return violWrapper;
    }
    
    @Override
    public void setLocaleList(List<Locale> localeList) {
        this.localeList = localeList;
    }

    protected List<Locale> getLocaleList() {
        return this.localeList;
    }
    
    @Override
    public Set<ViolationWrapper> delete(K id) {
        T t = getEntityManager().getReference(clazz, id);
        Set<ViolationWrapper> violations = validateDeleteConstraints(t);
        if (violations.isEmpty()) {
            getEntityManager().remove(t);
        } else {
            for (ViolationWrapper viol : violations) {
                logger.info("violation on delete: " + viol.getMessage());
                logger.info("   violationTemplate: " + viol.getMessageTemplate());
            }
        }
        
        return violations;
    }

    @Override
    public Set<ViolationWrapper> update(T t) {
        Validator validator = validatorFactory.getValidator();
        Set<ConstraintViolation<T>> constraintViolations = validator.validate(t);
        Set<ViolationWrapper> violWrapper = new HashSet<>();
        for(ConstraintViolation v : constraintViolations){
            violWrapper.add(new ViolationWrapper(v, localeList));
        }
        violWrapper.addAll(validateConstraints(t));
        if (violWrapper.isEmpty()) {
            getEntityManager().merge(t);
        } else {
            for(ViolationWrapper viol : violWrapper) {
                logger.info("violation on update: " + viol.getMessage());
                logger.info("   violationTemplate: " + viol.getMessageTemplate());
            }
        }
        
        return violWrapper;
    }

    @Override
    public T find(K id) {
        return id == null ? null : getEntityManager().find(clazz, id);
    }

    @Override
    public List<T> findAll() {
        List<T> found = new ArrayList<>();
        TypedQuery<T> q = getEntityManager().createQuery("SELECT t FROM " + clazz.getSimpleName() + " t", clazz);
        found.addAll(q.getResultList());
        return found;
    }

    @Override
    public List<T> findRange(int firstPosition, int numberOfResults) {
        List<T> found = new ArrayList<>();
        TypedQuery<T> q = getEntityManager().createQuery("SELECT t FROM " + clazz.getSimpleName() + " t", clazz);
        q.setFirstResult(firstPosition);
        q.setMaxResults(numberOfResults);
        found.addAll(q.getResultList());
        return found;
    }

    @Override
    public int count() {
        TypedQuery<Long> q = getEntityManager().createQuery("SELECT count(t) FROM " + clazz.getSimpleName() + " t", Long.class);
        return q.getSingleResult().intValue();
    }
}
