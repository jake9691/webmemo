/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gstrs.webmemo.persistence;

import com.gstrs.webmemo.model.entities.Reminder;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Jacob
 */
@Local
public interface IReminderRegistry extends IDAO<Reminder, Long>{
    public List<Reminder> findRangeOfRemindersForEmployee(long employeeId, int firstPosition, int numberOfResults);
    public List<Reminder> findRangeOfRemindersForStore(long StoreId, int firstPosition, int numberOfResults);
}
