/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gstrs.webmemo.persistence.impl;

import com.gstrs.webmemo.model.entities.Customer;
import com.gstrs.webmemo.model.entities.Employee;
import com.gstrs.webmemo.model.entities.Organization;
import com.gstrs.webmemo.model.entities.Reminder;
import com.gstrs.webmemo.model.entities.Store;
import com.gstrs.webmemo.model.entities.User;
import com.gstrs.webmemo.persistence.AbstractDAO;
import com.gstrs.webmemo.persistence.IStoreRegistry;
import com.gstrs.webmemo.persistence.validation.ViolationWrapper;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Jacob
 */
@Stateless
public class StoreRegistry extends AbstractDAO<Store, Long> implements IStoreRegistry {

    @PersistenceContext
    private EntityManager entitymanager;
    
    public StoreRegistry() {
        super(Store.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return entitymanager;
    }
    
    @Override
    public Set<ViolationWrapper> delete(Long id) {
        TypedQuery<Organization> q = entitymanager.createQuery("SELECT o FROM Organization AS o, IN(o.storeList) s WHERE s.id = :id", Organization.class);
        q.setParameter("id", id);
        Store t = entitymanager.getReference(Store.class, id);
        for (Organization o : q.getResultList()) {
            o.removeStore(t);
            entitymanager.merge(o);
        }
        TypedQuery<Reminder> q2 = entitymanager.createQuery("SELECT r FROM Reminder r WHERE r.store.id = :id", Reminder.class);
        q2.setParameter("id", id);
        for (Reminder r : q2.getResultList()) {
            entitymanager.remove(r);
        }
        
        TypedQuery<User> q3 = entitymanager.createQuery("SELECT u FROM User u WHERE u.store.id = :store", User.class);
        q3.setParameter("store", id);
        for (User user : q3.getResultList()) {
            entitymanager.remove(user);
        }
        
        return super.delete(id);
    }

    @Override
    protected Set<ViolationWrapper> validateConstraints(Store t) {
        TypedQuery<Store> q = entitymanager.createQuery("SELECT s FROM Store s WHERE s.lowerCaseName = :storename AND s.organization = :organization AND s.id != :id", Store.class);
        q.setParameter("storename", t.getName() == null ? null : t.getName().toLowerCase());
        q.setParameter("organization", t.getOrganization());
        q.setParameter("id", t.getId());
        Set<ViolationWrapper> uniquenessViolations = new HashSet<>();
        if (q.getResultList().size() > 0) {
            uniquenessViolations.add(new ViolationWrapper("webmemo.entities.store.name-org.Unique", super.getLocaleList(), Store.class, false));
        }
        return uniquenessViolations;
    }

    @Override
    public Set<ViolationWrapper> validateDeleteConstraints(Store store) {
        TypedQuery<Employee> q = entitymanager.createQuery("SELECT e FROM Employee e WHERE e.workingInStore = :store", Employee.class);
        q.setParameter("store", store);
        Set<ViolationWrapper> violations = new HashSet<>();
        if (q.getResultList().size() > 0) {
            violations.add(new ViolationWrapper("webmemo.entities.store.employee.Delete", super.getLocaleList(), Store.class, false));
        }
        TypedQuery<Customer> q2 = entitymanager.createQuery("SELECT c FROM Customer c WHERE c.customerCreatedInStore = :store", Customer.class);
        q2.setParameter("store", store);
        if (q2.getResultList().size() > 0) {
            violations.add(new ViolationWrapper("webmemo.entities.store.customer.Delete", super.getLocaleList(), Store.class, false));
        }

        return violations;
    }

    @Override
    public List<Store> findRangeOfStoreForOrganization(long organizationId, int firstPosition, int numberOfResults) {
        TypedQuery<Store> q = entitymanager.createQuery("SELECT DISTINCT s FROM Organization AS o, IN(o.storeList) s WHERE o.id = :id ORDER BY s.name", Store.class);
        q.setParameter("id", organizationId);
        q.setFirstResult(firstPosition);
        q.setMaxResults(numberOfResults);
        return q.getResultList();
    }
}
