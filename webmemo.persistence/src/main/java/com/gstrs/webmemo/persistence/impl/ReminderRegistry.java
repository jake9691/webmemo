/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gstrs.webmemo.persistence.impl;

import com.fasterxml.jackson.databind.ser.std.StdContainerSerializers;
import com.gstrs.webmemo.model.entities.Reminder;
import com.gstrs.webmemo.persistence.AbstractDAO;
import com.gstrs.webmemo.persistence.IReminderRegistry;
import com.gstrs.webmemo.persistence.validation.ViolationWrapper;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Jacob
 */
@Stateless
public class ReminderRegistry extends AbstractDAO<Reminder, Long> implements IReminderRegistry {

    @PersistenceContext
    private EntityManager entitymanager;
    
    public ReminderRegistry() {
        super(Reminder.class);
    }
    

    @Override
    protected EntityManager getEntityManager() {
        return entitymanager;
    }

    @Override
    protected Set<ViolationWrapper> validateConstraints(Reminder t) {
        Set<ViolationWrapper> violations = new HashSet<>();
        if(t.getReminderType().equals(Reminder.Type.CUSTOMER_NOTIFIED)){
            if(t.getEmployee() == null){
                violations.add(new ViolationWrapper("webmemo.entities.reminder.employee.NotNull", super.getLocaleList(), Reminder.class, true));
            }
            if(t.getMemo() == null){
                violations.add(new ViolationWrapper("webmemo.entities.reminder.memo.NotNull", super.getLocaleList(), Reminder.class, true));
            }else if(t.getMemo().getCustomer() == null){
                violations.add(new ViolationWrapper("webmemo.entities.reminder.customer.NotNull", super.getLocaleList(), Reminder.class, true));
            }
            
        }else if(t.getReminderType().equals(Reminder.Type.EMPLOYEE_CONFIRM)){
            if(t.getEmployee() == null){
                violations.add(new ViolationWrapper("webmemo.entities.reminder.employee.NotNull", super.getLocaleList(), Reminder.class, true));
            }
        }else if(t.getReminderType().equals(Reminder.Type.STORE_INFO)){
            if(t.getStore() == null){
                violations.add(new ViolationWrapper("webmemo.entities.reminder.store.NotNull", super.getLocaleList(), Reminder.class, true));
            }
        }
        return violations;
    }

    @Override
    protected Set<ViolationWrapper> validateDeleteConstraints(Reminder t) {
        return new HashSet<>();
    }
    
    @Override
    public List<Reminder> findRangeOfRemindersForEmployee(long employeeId, int firstPosition, int numberOfResults){
        TypedQuery<Reminder> q = entitymanager.createQuery("SELECT r FROM Reminder r WHERE r.employee.id = :id", Reminder.class);
        q.setParameter("id", employeeId);
        q.setFirstResult(firstPosition);
        q.setMaxResults(numberOfResults);
        return q.getResultList();
    }
    
    @Override
    public List<Reminder> findRangeOfRemindersForStore(long StoreId, int firstPosition, int numberOfResults){
        TypedQuery<Reminder> q = entitymanager.createQuery("SELECT r FROM Reminder r WHERE r.store.id = :id ORDER BY r.dateCreated DESC", Reminder.class);
        q.setParameter("id", StoreId);
        q.setFirstResult(firstPosition);
        q.setMaxResults(numberOfResults);
        List<Reminder> reminders = q.getResultList();
        Collections.sort(reminders);
        return reminders;
    }
}
