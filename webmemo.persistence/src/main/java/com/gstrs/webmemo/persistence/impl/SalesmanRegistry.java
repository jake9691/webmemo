/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gstrs.webmemo.persistence.impl;

import com.gstrs.webmemo.model.entities.Conversation;
import com.gstrs.webmemo.model.entities.Message;
import com.gstrs.webmemo.model.entities.Provider;
import com.gstrs.webmemo.model.entities.Salesman;
import com.gstrs.webmemo.persistence.AbstractDAO;
import com.gstrs.webmemo.persistence.ISalesmanRegistry;
import com.gstrs.webmemo.persistence.validation.ViolationWrapper;
import java.util.HashSet;
import java.util.Set;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 * 
 * HAD NOT TIME TO IMPLEMENT SALESMAN IN GUI AND BACKEND!!!
 *
 * @author Jacob
 */
@Stateless
public class SalesmanRegistry extends AbstractDAO<Salesman, Long> implements ISalesmanRegistry {

    @PersistenceContext
    private EntityManager entitymanager;

    public SalesmanRegistry() {
        super(Salesman.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return entitymanager;
    }

    @Override
    public Set<ViolationWrapper> delete(Long id) {
        TypedQuery<Provider> q = entitymanager.createQuery("SELECT DISTINCT p FROM Provider AS p, IN(p.salesmanList) s WHERE s.id = :id", Provider.class);
        q.setParameter("id", id);
        Salesman salesman = entitymanager.getReference(Salesman.class, id);
        for (Provider p : q.getResultList()) {
            p.removeSalesman(salesman);
            entitymanager.merge(p);
        }
        TypedQuery<Conversation> q2 = entitymanager.createQuery("SELECT DISTINCT c FROM Conversation AS c, IN(c.participantList) p WHERE p.id = :id", Conversation.class);
        q2.setParameter("id", id);
        for (Conversation c : q2.getResultList()) {
            c.removeParticipant(salesman);
            if (c.getParticipantList().size() < 2) {
                entitymanager.remove(c);
            }
        }
        TypedQuery<Message> q3 = entitymanager.createQuery("SELECT m FROM Message m WHERE m.sender.id = :id", Message.class);
        q3.setParameter("id", id);
        for (Message m : q3.getResultList()) {
            m.removeSender();
            entitymanager.merge(m);
        }
        return super.delete(id);
    }

    @Override
    protected Set<ViolationWrapper> validateConstraints(Salesman t) {
        TypedQuery<Conversation> q2 = entitymanager.createQuery("SELECT DISTINCT c FROM Salesman AS s, IN(s.conversationList) c WHERE s = :salesman", Conversation.class);
        q2.setParameter("salesman", t);
        Set<ViolationWrapper> uniquenessViolations = new HashSet();
        if (q2.getResultList().size() > 1) {
            uniquenessViolations.add(new ViolationWrapper("webmemo.entities.salesman.conversationList.Size", super.getLocaleList(), Salesman.class, true));
        }
        return uniquenessViolations;
    }

    @Override
    protected Set<ViolationWrapper> validateDeleteConstraints(Salesman t) {
        return new HashSet<>();
    }
}
