/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gstrs.webmemo.persistence.impl;

import com.gstrs.webmemo.model.entities.Memo;
import com.gstrs.webmemo.model.entities.Reminder;
import com.gstrs.webmemo.model.entities.Store;
import com.gstrs.webmemo.persistence.AbstractDAO;
import com.gstrs.webmemo.persistence.IMemoRegistry;
import com.gstrs.webmemo.persistence.util.Tuple;
import com.gstrs.webmemo.persistence.validation.ViolationWrapper;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

/**
 *
 * @author Jacob
 */
@Stateless
public class MemoRegistry extends AbstractDAO<Memo, Long> implements IMemoRegistry {

    @PersistenceContext
    private EntityManager entitymanager;

    public MemoRegistry() {
        super(Memo.class);
    }

    @Override
    public Set<ViolationWrapper> delete(Long id) {
        TypedQuery<Reminder> q = entitymanager.createQuery("SELECT r FROM Reminder r WHERE r.memo.id = :id", Reminder.class);
        q.setParameter("id", id);
        for(Reminder r : q.getResultList()){
            entitymanager.remove(r);
        }
        TypedQuery<Store> q2 = entitymanager.createQuery("SELECT DISTINCT s FROM Store AS s, IN(s.memoList) m WHERE m.id = :id", Store.class);
        q2.setParameter("id", id);
        Memo t = entitymanager.getReference(Memo.class, id);
        for(Store s : q2.getResultList()){
            s.removeMemo(t);
            entitymanager.merge(s);
        }
        return super.delete(id);
    }

    @Override
    protected EntityManager getEntityManager() {
        return entitymanager;
    }

    @Override
    public List<Memo> findRangeOfMemoForStore(long storeId, int firstPosition, int numberOfResults) {
        TypedQuery<Memo> q = entitymanager.createQuery("SELECT DISTINCT m FROM Store AS s, IN(s.memoList) m WHERE s.id = :id ORDER BY m.dateCreated DESC", Memo.class);
        q.setParameter("id", storeId);
        q.setFirstResult(firstPosition);
        q.setMaxResults(numberOfResults);
        List<Memo> memoList = q.getResultList();
        Collections.sort(memoList);
        return memoList;
    }
    
    @Override
    public List<Memo> findRangeOfMemoWithCustomer(long customerId, int firstPosition, int numberOfResults) {
        TypedQuery<Memo> q = entitymanager.createQuery("SELECT m FROM Memo m WHERE m.customer.id = :id ORDER BY m.dateCreated DESC", Memo.class);
        q.setParameter("id", customerId);
        q.setFirstResult(firstPosition);
        q.setMaxResults(numberOfResults);
        List<Memo> memoList = q.getResultList();
        Collections.sort(memoList);
        return memoList;
    }

    @Override
    public List<Memo> findRangeOfMemoWithResponsible(long responsibleId, int firstPosition, int numberOfResults) {
        TypedQuery<Memo> q = entitymanager.createQuery("SELECT m FROM Memo m WHERE m.responsible.id = :id ORDER BY m.dateCreated DESC", Memo.class);
        q.setParameter("id", responsibleId);
        q.setFirstResult(firstPosition);
        q.setMaxResults(numberOfResults);
        List<Memo> memoList = q.getResultList();
        Collections.sort(memoList);
        return memoList;
    }

    @Override
    public List<Memo> findRangeOfMemoWithCreator(long creatorId, int firstPosition, int numberOfResults) {
        TypedQuery<Memo> q = entitymanager.createQuery("SELECT m FROM Memo m WHERE m.creator.id = :id ORDER BY m.dateCreated DESC", Memo.class);
        q.setParameter("id", creatorId);
        q.setFirstResult(firstPosition);
        q.setMaxResults(numberOfResults);
        List<Memo> memoList = q.getResultList();
        Collections.sort(memoList);
        return memoList;
    }
    
    @Override
    public List<Memo> findRangeOfMemos(long creatorId, int firstPosition, int numberOfResults) {
        TypedQuery<Memo> q = entitymanager.createQuery("SELECT m FROM Memo m WHERE m.creator.id = :id OR m.responsible.id = :id ORDER BY m.dateCreated DESC", Memo.class);
        q.setParameter("id", creatorId);
        q.setFirstResult(firstPosition);
        q.setMaxResults(numberOfResults);
        List<Memo> memoList = q.getResultList();
        Collections.sort(memoList);
        return memoList;
    }
    
    
    
    @Override
    public List<Memo> searchRangeOfMemos(String search, Date since, long creatorId, long responsibleId, 
            long providerId, Memo.Status status, Boolean isReclaim, int firstPosition, int numberOfResults) {
        List<Tuple<String, Object>> searchStringParamList = new ArrayList<>();
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT m FROM Memo m ");
        builder.append("LEFT JOIN m.customer customer ");
        builder.append("LEFT JOIN m.item item ");
        builder.append("LEFT JOIN m.creator creator ");
        builder.append("LEFT JOIN m.responsible responsible ");
        builder.append("LEFT JOIN m.order order ");
        builder.append("LEFT JOIN m.reception reception ");
        if(search != null){
            search = search.replaceAll("[%_]", "");
            builder.append("WHERE ");
            createSearchWhereClause(builder, search, searchStringParamList);
        }
        
        Boolean applyWhereClause = search == null;
        
        List<Tuple<String, Object>> parameterList = new ArrayList<>();
        if(creatorId != 0){
            builder.append(AddAndString("creator.id = :creatorId",applyWhereClause));
            parameterList.add(new Tuple("creatorId", creatorId));
        }
        if(responsibleId != 0){
            builder.append(AddAndString("responsible.id = :responsibleId",applyWhereClause));
            parameterList.add(new Tuple("responsibleId", responsibleId));
        }
        if(providerId != 0){
            builder.append(AddAndString("item.provider.id = :providerId",applyWhereClause));
            parameterList.add(new Tuple("providerId", providerId));
        }
        if(since != null){
            builder.append(AddAndString("m.dateCreated >= :since",applyWhereClause));
        }
        if(status != null){
            builder.append(AddAndString("m.status = :status",applyWhereClause));
            parameterList.add(new Tuple("status", status));
        }
        if(isReclaim != null){
            builder.append(AddAndString("m.isReclaim = :isReclaim",applyWhereClause));
            parameterList.add(new Tuple("isReclaim", isReclaim));
        }
        builder.append(" ORDER BY m.dateCreated DESC ");
        TypedQuery<Memo> query = entitymanager.createQuery(builder.toString(), Memo.class);
        
        for(Tuple<String, Object> pair : parameterList){
            query.setParameter(pair.getLeft(), pair.getRight());
        }
        for(Tuple<String, Object> pair : searchStringParamList){
            query.setParameter(pair.getLeft(), "%" + pair.getRight() + "%");
        }
        if(since != null){
            query.setParameter("since", since, TemporalType.DATE);
        }
        query.setFirstResult(firstPosition);
        query.setMaxResults(numberOfResults);
        List<Memo> memoResult = query.getResultList();
        Collections.sort(memoResult);
        return memoResult;
    }
    
    private String AddAndString(String condition, Boolean applyWhereClause){
        if(applyWhereClause){
            applyWhereClause = false;
            return "WHERE " + condition + " ";
        }else{
            return "AND " + condition + " ";
        }
    }
    
    private void createSearchWhereClause(StringBuilder builder, String searchWords, List<Tuple<String, Object>> parameterList) {
        String[] searchWord = searchWords.split("[\\s]");
        int count = 0;
        for (String word : searchWord) {
            String parameter = "searchword" + count;
            if(count > 0){
                builder.append(" AND ");
            }
            builder.append(" ( ");
            appendString(builder, "LOWER(m.freeText) LIKE :" , parameter);
            appendString(builder, "OR LOWER(m.reclaimId) LIKE :" , parameter);
            appendString(builder, "OR LOWER(item.agreedPrice) LIKE :" , parameter);
            appendString(builder, "OR LOWER(item.color) LIKE :" , parameter);
            appendString(builder, "OR LOWER(item.itemNumber) LIKE :" , parameter);
            appendString(builder, "OR LOWER(item.name) LIKE :" , parameter);
            appendString(builder, "OR LOWER(item.providerItemNumber) LIKE :" , parameter);
            appendString(builder, "OR LOWER(item.quantity) LIKE :" , parameter);
            appendString(builder, "OR LOWER(item.size) LIKE :" , parameter);
            appendString(builder, "OR LOWER(customer.association) LIKE :" , parameter);
            appendString(builder, "OR LOWER(customer.email) LIKE :" , parameter);
            appendString(builder, "OR LOWER(customer.extraPhone) LIKE :" , parameter);
            appendString(builder, "OR customer.lowerCaseLastName LIKE :" , parameter);
            appendString(builder, "OR customer.lowerCaseName LIKE :" , parameter);
            appendString(builder, "OR customer.mobilePhone LIKE :" , parameter);
            appendString(builder, "OR LOWER(order.orderId) LIKE :" , parameter);
            appendString(builder, "OR LOWER(order.providerId) LIKE :" , parameter);
            appendString(builder, "OR LOWER(order.remarks) LIKE :" , parameter);
            appendString(builder, "OR LOWER(reception.remarks) LIKE :" , parameter);
            appendString(builder, "OR LOWER(reception.storageLocation) LIKE :" , parameter);
            builder.append(" ) ");
            parameterList.add(new Tuple(parameter, word.toLowerCase()));
            count++;
        }
    }
    
    private void appendString(StringBuilder builder, String string, String parameter){
        builder.append(string);
        builder.append(parameter);
        builder.append(" ");
    }

    @Override
    protected Set<ViolationWrapper> validateConstraints(Memo t) {
        return new HashSet<>();
    }

    @Override
    protected Set<ViolationWrapper> validateDeleteConstraints(Memo t) {
        return new HashSet<>();
    }


}
