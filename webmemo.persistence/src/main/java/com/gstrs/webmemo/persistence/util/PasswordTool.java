/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gstrs.webmemo.persistence.util;

import org.mindrot.jbcrypt.BCrypt;


/**
 *
 * @author davida
 */
public class PasswordTool {
    
    public String hash(String password) {
        return BCrypt.hashpw(password, BCrypt.gensalt());
    }
    
    public boolean check(String possible, String hash) {
        return BCrypt.checkpw(possible, hash);
    }
}
