package com.gstrs.webmemo.persistence;

import com.gstrs.webmemo.persistence.validation.ViolationWrapper;
import java.util.List;
import java.util.Locale;
import java.util.Set;

/**
 * Basic contract for containers of Products, Customers, etc
 *
 * @author hajo
 * @param <T> type of elements in container
 * @param <K> K is type of id (primary key)
 */
public interface IDAO<T, K> {

    public Set<ViolationWrapper> create(T t);
    
    public Set<ViolationWrapper> delete(K id);

    public Set<ViolationWrapper> update(T t);
    
    public void setLocaleList(List<Locale> localeList);

    public T find(K id);

    public List<T> findAll();

    public List<T> findRange(int firstPosition, int numberOfResults);

    public int count();
}
