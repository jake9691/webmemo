/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gstrs.webmemo.persistence;

import com.gstrs.webmemo.model.entities.Organization;
import com.gstrs.webmemo.model.entities.User;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Jacob
 */
@Local
public interface IOrganizationRegistry extends IDAO<Organization, Long> {

    public List<Organization> findRangeOfOrganizationForUser(User user, int firstPosition, int numberOfResults);
}
