/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gstrs.webmemo.persistence.impl;

import com.gstrs.webmemo.model.entities.Item;
import com.gstrs.webmemo.model.entities.Organization;
import com.gstrs.webmemo.model.entities.Provider;
import com.gstrs.webmemo.persistence.AbstractDAO;
import com.gstrs.webmemo.persistence.IProviderRegistry;
import com.gstrs.webmemo.persistence.validation.ViolationWrapper;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Jacob
 */
@Stateless
public class ProviderRegistry extends AbstractDAO<Provider, Long> implements IProviderRegistry {

    @PersistenceContext
    private EntityManager entitymanager;

    public ProviderRegistry() {
        super(Provider.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return entitymanager;
    }

    @Override
    public Set<ViolationWrapper> delete(Long id) {
        TypedQuery<Organization> q = entitymanager.createQuery("SELECT o FROM Organization AS o, IN(o.providerList) p WHERE p.id = :id", Organization.class);
        q.setParameter("id", id);
        Provider t = entitymanager.getReference(Provider.class, id);
        for (Organization o : q.getResultList()) {
            o.removeProvider(t);
            entitymanager.merge(o);
        }
        return super.delete(id);
    }

    @Override
    protected Set<ViolationWrapper> validateConstraints(Provider t) {
        TypedQuery<Provider> q = entitymanager.createQuery("SELECT p FROM Provider p WHERE p.providerAbbreviationId = :providerabr AND p.organization = :organization AND p.id != :id", Provider.class);
        q.setParameter("providerabr", t.getProviderAbbreviationId());
        q.setParameter("organization", t.getOrganization());
        q.setParameter("id", t.getId());
        Set<ViolationWrapper> uniquenessViolations = new HashSet<>();
        if (q.getResultList().size() > 0) {
            uniquenessViolations.add(new ViolationWrapper("webmemo.entities.provider.abbid-org.Unique", super.getLocaleList(), Provider.class, false));
        }

        TypedQuery<Provider> q2 = entitymanager.createQuery("SELECT p FROM Provider p WHERE p.lowerCaseName = :name AND p.organization = :organization AND p.id != :id", Provider.class);
        q2.setParameter("name", t.getName() == null ? null : t.getName().toLowerCase());
        q2.setParameter("organization", t.getOrganization());
        q2.setParameter("id", t.getId());
        if (q2.getResultList().size() > 0) {
            uniquenessViolations.add(new ViolationWrapper("webmemo.entities.provider.name-org.Unique", super.getLocaleList(), Provider.class, false));
        }
        return uniquenessViolations;
    }

    @Override
    protected Set<ViolationWrapper> validateDeleteConstraints(Provider t) {
        TypedQuery<Item> q = entitymanager.createQuery("SELECT i FROM Item i WHERE i.provider = :provider", Item.class);
        q.setParameter("provider", t);
        Set<ViolationWrapper> violations = new HashSet<>();
        if (q.getResultList().size() > 0) {
            violations.add(new ViolationWrapper("webmemo.entities.provider.item.Delete", super.getLocaleList(), Provider.class, false));
        }
        return violations;
    }

    @Override
    public List<Provider> findRangeOfProviderForOrganization(long providerId, int firstPosition, int numberOfResults) {
        TypedQuery<Provider> q = entitymanager.createQuery("SELECT p FROM Provider p WHERE p.organization.id = :id ORDER BY p.providerAbbreviationId", Provider.class);
        q.setParameter("id", providerId);
        q.setFirstResult(firstPosition);
        q.setMaxResults(numberOfResults);
        return q.getResultList();
    }
}
