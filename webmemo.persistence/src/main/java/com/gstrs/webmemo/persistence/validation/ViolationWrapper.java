/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gstrs.webmemo.persistence.validation;

import java.io.IOException;
import java.io.Serializable;
import javax.validation.ConstraintViolation;
import javax.xml.bind.annotation.XmlRootElement;
import static com.gstrs.webmemo.persistence.validation.Util.*;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import javax.xml.bind.annotation.XmlTransient;



/**
 * Used to wrap a violations. SInce we couldn't convert the ViolationsConstraint<Class>
 * that is returned by the bean validation we did our own wrapper that we can convert to json.
 * We also did so it could read from the ValidationMessages.properties file. That means that we could use
 * it to wrap errors from the web application layer also.
 * 
 * 
 * @author Jacob
 */
@XmlRootElement
public class ViolationWrapper implements Serializable {

    private String key, objectKey;
    private String message;
    private boolean isField;
    private String messageTemplate;
    
    private transient Properties prop;

    public ViolationWrapper() {

    }
    
    public ViolationWrapper(String messageTemplate, List<Locale> localeList, Class clazz, boolean isField) {
        init(messageTemplate, messageTemplate.split("\\."), localeList, clazz);
        this.isField = isField;
    }
    
    private void init(String messageTemplate, String[] array, List<Locale> localeList, Class clazz){
        this.key = array[3];
        this.objectKey = clazz.getSimpleName();
        this.messageTemplate = messageTemplate;
        prop = new Properties();
        try{
            loadProperty(prop, localeList);
            this.message = prop.getProperty(messageTemplate);
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    public ViolationWrapper(ConstraintViolation violation, List<Locale> localeList) {
        String messageTemplate = violation.getMessageTemplate().replaceAll("[{}]", "");
        String[] array = messageTemplate.split("\\.");
        init(messageTemplate, array, localeList, violation.getRootBeanClass());
        this.isField = true;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isField() {
        return isField;
    }

    public boolean isIsField() {
        return isField;
    }

    public void setIsField(boolean isField) {
        this.isField = isField;
    }
    
    @XmlTransient
    public String getMessageTemplate() {
        return messageTemplate;
    }

    public String getObjectKey() {
        return objectKey;
    }

    public void setObjectKey(String objectKey) {
        this.objectKey = objectKey;
    }
    
}
