/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gstrs.webmemo.persistence;

import com.gstrs.webmemo.model.entities.Organization;
import com.gstrs.webmemo.model.entities.User;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Jacob
 */
@Local
public interface IUserRegistry extends IDAO<User, Long> {

    public User getByUsername(String username);
    
    public boolean validatePassword(User user, String password);
        
    public void changePassword(User user, String oldPassword, String newPassword);
    
    public List<User> findUsersForOrganization(Organization org);
    
    public User findUserForStore(Long storeId);
}
