/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gstrs.webmemo.persistence.impl;

import com.gstrs.webmemo.model.entities.Reception;
import com.gstrs.webmemo.persistence.AbstractDAO;
import com.gstrs.webmemo.persistence.IReceptionRegistry;
import com.gstrs.webmemo.persistence.validation.ViolationWrapper;
import java.util.HashSet;
import java.util.Set;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Jacob
 */
@Stateless
public class ReceptionRegistry extends AbstractDAO<Reception, Long> implements IReceptionRegistry {

    @PersistenceContext
    private EntityManager entitymanager;
   
    public ReceptionRegistry() {
        super(Reception.class);
    }
    
    @Override
    protected EntityManager getEntityManager() {
        return entitymanager;
    }

    @Override
    protected Set<ViolationWrapper> validateConstraints(Reception t) {
        return new HashSet<>();
    }

    @Override
    protected Set<ViolationWrapper> validateDeleteConstraints(Reception t) {
        return new HashSet<>();
    }
}
