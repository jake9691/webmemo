/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gstrs.webmemo.persistence;

import com.gstrs.webmemo.model.entities.Provider;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Jacob
 */
@Local
public interface IProviderRegistry extends IDAO<Provider, Long>{
    public List<Provider> findRangeOfProviderForOrganization(long organizationId, int firstPosition, int numberOfResults);
}
