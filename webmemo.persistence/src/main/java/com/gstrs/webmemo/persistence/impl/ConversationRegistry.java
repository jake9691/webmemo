/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gstrs.webmemo.persistence.impl;

import com.gstrs.webmemo.model.entities.Conversation;
import com.gstrs.webmemo.model.entities.Person;
import com.gstrs.webmemo.persistence.AbstractDAO;
import com.gstrs.webmemo.persistence.IConversationRegistry;
import com.gstrs.webmemo.persistence.validation.ViolationWrapper;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Jacob
 */
@Stateless
public class ConversationRegistry extends AbstractDAO<Conversation, Long> implements IConversationRegistry {

    @PersistenceContext
    private EntityManager entitymanager;
    
    public ConversationRegistry() {
        super(Conversation.class);
    }
    
    @Override
    public Set<ViolationWrapper> delete(Long id){
        TypedQuery<Person> q = entitymanager.createQuery("SELECT DISTINCT p FROM Person AS p, IN(p.conversationList) c WHERE c.id = :id", Person.class);
        q.setParameter("id", id);
        for(Person p : q.getResultList()){
            entitymanager.remove(p);
        }
        return super.delete(id);
    }

    @Override
    protected EntityManager getEntityManager() {
        return entitymanager;
    }
    
    @Override
    public List<Conversation> findRangeOfConversationsForPerson(long personId, int firstPosition, int numberOfResults){
        TypedQuery<Conversation> q = entitymanager.createQuery("SELECT DISTINCT c FROM Person AS p, IN(p.conversationList) c WHERE p.id = :id ORDER BY c.dateCreated DESC", Conversation.class);
        q.setParameter("id", personId);
        q.setFirstResult(firstPosition);
        q.setMaxResults(numberOfResults);
        List<Conversation> convList = q.getResultList();
        Collections.sort(convList);
        return convList;
    }
    
    @Override
    public List<Person> findRangeOfParticipantsForConversation(long conversationId, int firstPosition, int numberOfResults){
        TypedQuery<Person> q = entitymanager.createQuery("SELECT p FROM Person p JOIN p.conversationList c WHERE c.id = :id ORDER BY p.name", Person.class);
        q.setParameter("id", conversationId);
        q.setFirstResult(firstPosition);
        q.setMaxResults(numberOfResults);
        return q.getResultList();
    }

    @Override
    protected Set<ViolationWrapper> validateConstraints(Conversation t) {
        return new HashSet<>();
    }

    @Override
    protected Set<ViolationWrapper> validateDeleteConstraints(Conversation t) {
        return new HashSet<>();
    }
}
