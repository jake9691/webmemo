/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gstrs.webmemo.persistence.impl;

import com.gstrs.webmemo.model.entities.Conversation;
import com.gstrs.webmemo.model.entities.Customer;
import com.gstrs.webmemo.model.entities.Employee;
import com.gstrs.webmemo.model.entities.Memo;
import com.gstrs.webmemo.model.entities.Message;
import com.gstrs.webmemo.model.entities.Order;
import com.gstrs.webmemo.model.entities.Organization;
import com.gstrs.webmemo.model.entities.Person;
import com.gstrs.webmemo.model.entities.Reception;
import com.gstrs.webmemo.model.entities.Reminder;
import com.gstrs.webmemo.model.entities.Store;
import com.gstrs.webmemo.persistence.AbstractDAO;
import com.gstrs.webmemo.persistence.IEmployeeRegistry;
import com.gstrs.webmemo.persistence.validation.ViolationWrapper;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Jacob
 */
@Stateless
public class EmployeeRegistry extends AbstractDAO<Employee, Long> implements IEmployeeRegistry{
    
    @PersistenceContext
    private EntityManager entitymanager;

    public EmployeeRegistry() {
        super(Employee.class);
    }
    
    @Override
    protected EntityManager getEntityManager() {
        return entitymanager;
    }

    @Override
    public Set<ViolationWrapper> delete(Long id){
        TypedQuery<Reminder> q = entitymanager.createQuery("SELECT r FROM Reminder r WHERE r.employee.id = :id", Reminder.class);
        q.setParameter("id", id);
        for(Reminder r : q.getResultList()){
            entitymanager.remove(r);
        }
        TypedQuery<Organization> q2 = entitymanager.createQuery("SELECT DISTINCT o FROM Organization AS o, IN(o.employeeList) e WHERE e.id = :id", Organization.class);
        q2.setParameter("id", id);
        Employee e = entitymanager.getReference(Employee.class, id);
        for(Organization o : q2.getResultList()){
            o.removeEmployee(e);
            entitymanager.merge(o);
        }
        TypedQuery<Conversation> q3 = entitymanager.createQuery("SELECT DISTINCT c FROM Conversation AS c, IN(c.participantList) p WHERE p.id = :id", Conversation.class);
        q3.setParameter("id", id);
        for(Conversation c : q3.getResultList()){
            c.removeParticipant(e);
            if(c.getParticipantList().size() < 2){
                entitymanager.remove(c);
            }
        }
        TypedQuery<Message> q4 = entitymanager.createQuery("SELECT m FROM Message m WHERE m.sender.id = :id", Message.class);
        q4.setParameter("id", id);
        for(Message m : q4.getResultList()){
            m.removeSender();
            entitymanager.merge(m);
        }
        return super.delete(id);
    }
    
    @Override
    protected Set<ViolationWrapper> validateConstraints(Employee t) {
        TypedQuery<Employee> q = entitymanager.createQuery("SELECT e FROM Employee e WHERE e.pinCode = :pincode AND e.workingInStore = :store AND e.id != :id", Employee.class);
        q.setParameter("pincode", t.getPinCode());
        q.setParameter("store", t.getWorkingInStore());
        q.setParameter("id", t.getId());
        Set<ViolationWrapper> uniquenessViolations = new HashSet<>();
        if (q.getResultList().size() > 0) {
            uniquenessViolations.add(new ViolationWrapper("webmemo.entities.employee.pincode-store.Unique", super.getLocaleList(), Employee.class, false));
        }
        
        TypedQuery<Employee> q2 = entitymanager.createQuery("SELECT e FROM Employee e WHERE e.abbreviation = :abbreviation AND e.workingInStore = :store AND e.id != :id", Employee.class);
        q2.setParameter("abbreviation", t.getAbbreviation());
        q2.setParameter("store", t.getWorkingInStore());
        q2.setParameter("id", t.getId());
        if (q2.getResultList().size() > 0) {
            uniquenessViolations.add(new ViolationWrapper("webmemo.entities.employee.abbreviation-store.Unique", super.getLocaleList(),Employee.class, false));
        }
        return uniquenessViolations;
    }

    @Override
    public void moveAllEmployeeFromStore1ToStore2(Store store1, Store store2) {
        TypedQuery<Employee> q = entitymanager.createQuery("SELECT e FROM Employee e WHERE e.workingInStore = :store", Employee.class);
        q.setParameter("store", store1);
        for(Employee e: q.getResultList()){
            e.setWorkingInStore(store2);
            super.update(e);
        }
    }
    
    public Person findPerson(Long id){
        return id == null ? null : getEntityManager().find(Person.class, id);
    }

    @Override
    protected Set<ViolationWrapper> validateDeleteConstraints(Employee t) {
        TypedQuery<Memo> q = entitymanager.createQuery("SELECT m FROM Memo m WHERE m.responsible = :employee OR m.creator = :employee", Memo.class);
        q.setParameter("employee", t);
        Set<ViolationWrapper> violations = new HashSet<>();
        if(q.getResultList().size() > 0){
            violations.add(new ViolationWrapper("webmemo.entities.employee.memo.Delete", super.getLocaleList(),Employee.class, false));
        }
        TypedQuery<Order> q3 = entitymanager.createQuery("SELECT o FROM Order o WHERE o.creator = :employee", Order.class);
        q3.setParameter("employee", t);
        if(q3.getResultList().size() > 0){
            violations.add(new ViolationWrapper("webmemo.entities.employee.order-creator.Delete", super.getLocaleList(),Employee.class, false));
        }
        TypedQuery<Reception> q4 = entitymanager.createQuery("SELECT r FROM Reception r WHERE r.creator = :employee", Reception.class);
        q4.setParameter("employee", t);
        if(q4.getResultList().size() > 0){
            violations.add(new ViolationWrapper("webmemo.entities.employee.reception-creator.Delete", super.getLocaleList(),Employee.class, false));
        }
        return violations;
    }
    
    @Override
    public List<Employee> findRangeOfEmployeeForStore(long storeId, int firstPosition, int numberOfResults){
        TypedQuery<Employee> q = entitymanager.createQuery("SELECT e FROM Employee e WHERE e.workingInStore.id = :id ORDER BY e.name", Employee.class);
        q.setParameter("id", storeId);
        q.setFirstResult(firstPosition);
        q.setMaxResults(numberOfResults);
        return q.getResultList();
    }
    
    @Override
    public List<Employee> findRangeOfEmployeeForOrganization(long organizationId, int firstPosition, int numberOfResults){
        TypedQuery<Employee> q = entitymanager.createQuery("SELECT e FROM Employee e WHERE e.workingInStore.organization.id = :id ORDER BY e.name", Employee.class);
        q.setParameter("id", organizationId);
        q.setFirstResult(firstPosition);
        q.setMaxResults(numberOfResults);
        return q.getResultList();
    }
    
    @Override
    public Employee findEmployeeWithStoreAndPinCode(Long storeId, String pinCode){
        if(pinCode == null || storeId == null){
            return null;
        }
        TypedQuery<Employee> q = entitymanager.createQuery("SELECT e FROM Employee e WHERE e.workingInStore.id = :storeId AND e.pinCode = :pinCode", Employee.class);
        q.setParameter("storeId", storeId);
        q.setParameter("pinCode", pinCode);
        return q.getSingleResult();
    }
}
