/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gstrs.webmemo.persistence.util;

import java.util.List;
import javax.persistence.TypedQuery;

/**
 *
 * @author davida
 */
public class QueryHelper {
    
    public static <T> T first(TypedQuery<T> q) {
        q.setMaxResults(1);
        List<T> l = q.getResultList();
        
        if (l == null || l.isEmpty()) {
            return null;
        }
        
        return l.get(0);
    }
}
