/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gstrs.webmemo.persistence.impl;

import com.gstrs.webmemo.model.entities.Organization;
import com.gstrs.webmemo.model.entities.Provider;
import com.gstrs.webmemo.model.entities.Store;
import com.gstrs.webmemo.model.entities.User;
import com.gstrs.webmemo.persistence.AbstractDAO;
import com.gstrs.webmemo.persistence.IOrganizationRegistry;
import com.gstrs.webmemo.persistence.validation.ViolationWrapper;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Jacob
 */
@Stateless
public class OrganizationRegistry extends AbstractDAO<Organization, Long> implements IOrganizationRegistry {

    @PersistenceContext
    private EntityManager entitymanager;

    public OrganizationRegistry() {
        super(Organization.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return entitymanager;
    }

    @Override
    public Set<ViolationWrapper> delete(Long id) {
        TypedQuery<User> q = entitymanager.createQuery("SELECT u FROM User AS u, IN(u.organizationList) o WHERE o.id = :id", User.class);
        q.setParameter("id", id);
        Organization org = entitymanager.getReference(Organization.class, id);
        for (User u : q.getResultList()) {
            u.removeOrganization(org);
            entitymanager.merge(u);
        }
        return super.delete(id);
    }

    @Override
    protected Set<ViolationWrapper> validateConstraints(Organization t) {
        TypedQuery<Organization> q = entitymanager.createQuery("SELECT o FROM Organization o WHERE o.lowerCaseName = :name AND o.id != :id", Organization.class);
        q.setParameter("name", t.getName() == null ? null : t.getName().toLowerCase());
        q.setParameter("id", t.getId());
        Set<ViolationWrapper> uniquenessViolations = new HashSet<>();
        if (q.getResultList().size() > 0) {
            uniquenessViolations.add(new ViolationWrapper("webmemo.entities.organization.name.Unique", super.getLocaleList(), Organization.class, true));
        }
        return uniquenessViolations;
    }

    @Override
    protected Set<ViolationWrapper> validateDeleteConstraints(Organization t) {
        TypedQuery<Store> q = entitymanager.createQuery("SELECT s FROM Store s WHERE s.organization = :organization", Store.class);
        q.setParameter("organization", t);
        Set<ViolationWrapper> violations = new HashSet<>();
        if (q.getResultList().size() > 0) {
            violations.add(new ViolationWrapper("webmemo.entities.organization.store.Delete", super.getLocaleList(), Organization.class, false));
        }
        TypedQuery<Provider> q2 = entitymanager.createQuery("SELECT p FROM Provider p WHERE p.organization = :organization", Provider.class);
        q2.setParameter("organization", t);
        if (q2.getResultList().size() > 0) {
            violations.add(new ViolationWrapper("webmemo.entities.organization.provider.Delete", super.getLocaleList(), Organization.class, false));
        }
        return violations;
    }

    @Override
    public List<Organization> findRangeOfOrganizationForUser(User user, int firstPosition, int numberOfResults) {
        TypedQuery<Organization> q = entitymanager.createQuery("SELECT DISTINCT o FROM User AS u, IN(u.organizationList) o WHERE u = :user ORDER BY o.name", Organization.class);
        q.setParameter("user", user);
        q.setFirstResult(firstPosition);
        q.setMaxResults(numberOfResults);
        return q.getResultList();
    }
}
