/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gstrs.webmemo.persistence;

import com.gstrs.webmemo.model.entities.Conversation;
import com.gstrs.webmemo.model.entities.Person;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Jacob
 */
@Local
public interface IConversationRegistry extends IDAO<Conversation, Long>{
    public List<Conversation> findRangeOfConversationsForPerson(long personId, int firstPosition, int numberOfResults);
    public List<Person> findRangeOfParticipantsForConversation(long conversationId, int firstPosition, int numberOfResults);
}
