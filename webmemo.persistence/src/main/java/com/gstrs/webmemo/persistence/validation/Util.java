/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gstrs.webmemo.persistence.validation;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

/**
 *
 * @author Jacob
 */
public class Util {
    protected static synchronized void loadProperty(Properties prop, List<Locale> localeList) throws IOException {
        prop.clear();
        InputStream input = null;
        if(localeList != null){
            Iterator<Locale> it = localeList.iterator();
            while(it.hasNext() && input == null){
                Locale  locale = it.next();
                if (locale != null) {
                    String propFileName = "ValidationMessages_" + locale.getLanguage().toLowerCase() + ".properties";
                    input = Util.class.getClassLoader().getResourceAsStream(propFileName);
                }
            }
        }
        if (input == null) {
            String propFileName2 = "ValidationMessages.properties";
            input = Util.class.getClassLoader().getResourceAsStream(propFileName2);
            if (input == null) {
                throw new FileNotFoundException("property file '" + propFileName2 + "' not found in the classpath");
            }
        }
        prop.load(input);

    }
}
