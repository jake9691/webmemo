/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gstrs.webmemo.persistence.impl;

import com.gstrs.webmemo.model.entities.Conversation;
import com.gstrs.webmemo.model.entities.Message;
import com.gstrs.webmemo.persistence.AbstractDAO;
import com.gstrs.webmemo.persistence.IMessageRegistry;
import com.gstrs.webmemo.persistence.validation.ViolationWrapper;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Jacob
 */
@Stateless
public class MessageRegistry extends AbstractDAO<Message, Long> implements IMessageRegistry {

    @PersistenceContext
    private EntityManager entitymanager;

    public MessageRegistry() {
        super(Message.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return entitymanager;
    }

    @Override
    public Set<ViolationWrapper> delete(Long id) {
        TypedQuery<Conversation> q2 = entitymanager.createQuery("SELECT DISTINCT c FROM Conversation AS c, IN(c.messageList) m WHERE m.id = :id", Conversation.class);
        q2.setParameter("id", id);
        Message t = entitymanager.getReference(Message.class, id);
        for (Conversation c : q2.getResultList()) {
            c.removeMessage(t);
            entitymanager.merge(c);
        }
        return super.delete(id);
    }

    @Override
    public List<Message> findRangeWithMessagesForConversation(long conversationId, int first, int n) {
        TypedQuery<Message> q = entitymanager.createQuery("SELECT DISTINCT m FROM Conversation AS c, IN(c.messageList) m WHERE c.id = :id ORDER BY m.dateCreated DESC", Message.class);
        q.setParameter("id", conversationId);
        q.setFirstResult(first);
        q.setMaxResults(n);
        List<Message> messageList = q.getResultList();
        Collections.sort(messageList);
        return messageList;
    }

    @Override
    protected Set<ViolationWrapper> validateConstraints(Message t) {
        return new HashSet<>();
    }

    @Override
    protected Set<ViolationWrapper> validateDeleteConstraints(Message t) {
        return new HashSet<>();
    }
}
