/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gstrs.webmemo.persistence;

import com.gstrs.webmemo.model.entities.Employee;
import com.gstrs.webmemo.model.entities.Person;
import com.gstrs.webmemo.model.entities.Store;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Jacob
 */
@Local
public interface IEmployeeRegistry extends IDAO<Employee, Long> {

    public void moveAllEmployeeFromStore1ToStore2(Store store1, Store store2);

    public Person findPerson(Long id);

    public List<Employee> findRangeOfEmployeeForOrganization(long organizationId, int firstPosition, int numberOfResults);

    public List<Employee> findRangeOfEmployeeForStore(long storeId, int firstPosition, int numberOfResults);
    
    public Employee findEmployeeWithStoreAndPinCode(Long storeId, String pinCode);
}
