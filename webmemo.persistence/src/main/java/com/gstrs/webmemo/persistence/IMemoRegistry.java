/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gstrs.webmemo.persistence;

import com.gstrs.webmemo.model.entities.Memo;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Jacob
 */
@Local
public interface IMemoRegistry extends IDAO<Memo, Long> {

    public List<Memo> findRangeOfMemoForStore(long storeId, int firstPosition, int numberOfResults);

    public List<Memo> findRangeOfMemoWithResponsible(long responsibleId, int firstPosition, int numberOfResults);

    public List<Memo> findRangeOfMemoWithCustomer(long customerId, int firstPosition, int numberOfResults);

    public List<Memo> findRangeOfMemoWithCreator(long creatorId, int firstPosition, int numberOfResults);
    
    public List<Memo> findRangeOfMemos(long creatorId, int firstPosition, int numberOfResults);
    
    public List<Memo> searchRangeOfMemos(String search, Date since, long creatorId, long responsibleId, 
            long providerId, Memo.Status status, Boolean isReclaim, int firstPosition, int numberOfResults);
}
