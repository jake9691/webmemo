/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gstrs.webmemo.persistence.impl;

import com.gstrs.webmemo.model.entities.Order;
import com.gstrs.webmemo.persistence.AbstractDAO;
import com.gstrs.webmemo.persistence.IOrderRegistry;
import com.gstrs.webmemo.persistence.validation.ViolationWrapper;
import java.util.HashSet;
import java.util.Set;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Jacob
 */
@Stateless
public class OrderRegistry extends AbstractDAO<Order, Long> implements IOrderRegistry {

    @PersistenceContext
    private EntityManager entitymanager;

    public OrderRegistry() {
        super(Order.class);
    }
    
    @Override
    protected EntityManager getEntityManager() {
        return entitymanager;
    }

    @Override
    protected Set<ViolationWrapper> validateConstraints(Order t) {
        return new HashSet<>();
    }

    @Override
    protected Set<ViolationWrapper> validateDeleteConstraints(Order t) {
        return new HashSet<>();
    }
}
