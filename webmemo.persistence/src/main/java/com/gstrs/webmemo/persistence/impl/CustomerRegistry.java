/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gstrs.webmemo.persistence.impl;

import com.gstrs.webmemo.model.entities.Conversation;
import com.gstrs.webmemo.model.entities.Customer;
import com.gstrs.webmemo.model.entities.Memo;
import com.gstrs.webmemo.model.entities.Message;
import com.gstrs.webmemo.model.entities.Organization;
import com.gstrs.webmemo.model.entities.Store;
import com.gstrs.webmemo.persistence.AbstractDAO;
import com.gstrs.webmemo.persistence.ICustomerRegistry;
import com.gstrs.webmemo.persistence.validation.ViolationWrapper;
import com.gstrs.webmemo.persistence.util.Tuple;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Jacob
 */
@Stateless
public class CustomerRegistry extends AbstractDAO<Customer, Long> implements ICustomerRegistry {

    @PersistenceContext
    private EntityManager entitymanager;

    public CustomerRegistry() {
        super(Customer.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return entitymanager;
    }
    
    @Override
    public Set<ViolationWrapper> delete(Long id){
        TypedQuery<Organization> q = entitymanager.createQuery("SELECT DISTINCT o FROM Organization AS o, IN(o.customerList) c WHERE c.id = :id", Organization.class);
        q.setParameter("id", id);
        Customer customer = entitymanager.getReference(Customer.class, id);
        for(Organization o : q.getResultList()){
            o.removeCustomer(customer);
            entitymanager.merge(o);
        }
        TypedQuery<Conversation> q2 = entitymanager.createQuery("SELECT DISTINCT c FROM Conversation AS c, IN(c.participantList) p WHERE p.id = :id", Conversation.class);
        q2.setParameter("id", id);
        for(Conversation c : q2.getResultList()){
            c.removeParticipant(customer);
            if(c.getParticipantList().size() < 2){
                entitymanager.remove(c);
            }
        }
        TypedQuery<Message> q3 = entitymanager.createQuery("SELECT m FROM Message m WHERE m.sender.id = :id", Message.class);
        q3.setParameter("id", id);
        for(Message m : q3.getResultList()){
            m.removeSender();
            entitymanager.merge(m);
        }
        return super.delete(id);
    }

    @Override
    protected Set<ViolationWrapper> validateConstraints(Customer t) {
        TypedQuery<Customer> q = entitymanager.createQuery("SELECT c FROM Customer c WHERE c.mobilePhone = :mobile AND c.lowerCaseName = :name AND c.lowerCaseLastName = :lastname AND c.id != :id", Customer.class);
        q.setParameter("mobile", t.getMobilePhone());
        q.setParameter("name", t.getName() == null ? null : t.getName().toLowerCase());
        q.setParameter("lastname", t.getLastName() == null ? null : t.getLastName().toLowerCase());
        q.setParameter("id", t.getId());
        Set<ViolationWrapper> uniquenessViolations = new HashSet<>();
        if (q.getResultList().size() > 0) {
            uniquenessViolations.add(new ViolationWrapper("webmemo.entities.customer.mobile-name-lastname.Unique", super.getLocaleList(), Customer.class, false));
        }
        TypedQuery<Conversation> q2 = entitymanager.createQuery("SELECT DISTINCT c FROM Customer AS cust, IN(cust.conversationList) c WHERE cust = :customer", Conversation.class);
        q2.setParameter("customer", t);
        if(q2.getResultList().size() > 1){
            uniquenessViolations.add(new ViolationWrapper("webmemo.entities.customer.conversationList.Size", super.getLocaleList(), Customer.class, true));
        }
        return uniquenessViolations;
    }
    
    
    @Override
    protected Set<ViolationWrapper> validateDeleteConstraints(Customer t) {
        TypedQuery<Memo> q = entitymanager.createQuery("SELECT m FROM Memo m WHERE m.customer = :customer", Memo.class);
        q.setParameter("customer", t);
        Set<ViolationWrapper> violations = new HashSet<>();
        if(q.getResultList().size() > 0){
            violations.add(new ViolationWrapper("webmemo.entities.customer.memo.Delete", super.getLocaleList(), Customer.class, false));
        }
        return violations;
    }   
    
    @Override
    public void moveAllCustomerFromStore1ToStore2(Store store1, Store store2) {
        TypedQuery<Customer> q = entitymanager.createQuery("SELECT c FROM Customer c WHERE c.customerCreatedInStore = :store", Customer.class);
        q.setParameter("store", store1);
        for (Customer c : q.getResultList()) {
            c.setCustomerCreatedInStore(store2);
            super.update(c);
        }
    }
    
    @Override
    public List<Customer> findRangeOfCustomerForStore(long storeId, int firstPosition, int numberOfResults){
        TypedQuery<Customer> q = entitymanager.createQuery("SELECT c FROM Customer c WHERE c.customerCreatedInStore.id = :id ORDER BY c.name", Customer.class);
        q.setParameter("id", storeId);
        q.setFirstResult(firstPosition);
        q.setMaxResults(numberOfResults);
        return q.getResultList();
    }
    
    @Override
    public List<Customer> findRangeOfCustomerForOrganization(long organizationId, int firstPosition, int numberOfResults){
        TypedQuery<Customer> q = entitymanager.createQuery("SELECT c FROM Customer c WHERE c.customerCreatedInStore.organization.id = :id ORDER BY c.name", Customer.class);
        q.setParameter("id", organizationId);
        q.setFirstResult(firstPosition);
        q.setMaxResults(numberOfResults);
        return q.getResultList();
    }

    @Override
    public int customerCountForOrganization(long organizationId) {
        TypedQuery<Long> q = entitymanager.createQuery("SELECT COUNT(c) FROM Customer c WHERE c.customerCreatedInStore.organization.id = :id", Long.class);
        
        q.setParameter("id", organizationId);
        return q.getSingleResult().intValue();
    }
    
    @Override
    public List<Customer> searchForCustomer(String search, Long storeId, int offset, int count) {
//        if (storeId == null || storeId < 1) {
//            return null;
//        } else {
            List<Tuple<String, Object>> searchStringParamList = new ArrayList<>();
            StringBuilder builder = new StringBuilder();
            builder.append("SELECT customer FROM Customer customer ");
            
            if (search != null) {
                search = search.replaceAll("[%_]", "");
                builder.append("WHERE ");
                createSearchWhereClause(builder, search, searchStringParamList);
            }
            Boolean applyWhereClause = search == null;

            List<Tuple<String, Object>> parameterList = new ArrayList<>();
            
            if(storeId != null && storeId > 0){
                builder.append(AddAndString("customer.customerCreatedInStore.id = :storeId", applyWhereClause));
                parameterList.add(new Tuple("storeId", storeId));
            }
            
            builder.append(" ORDER BY customer.dateCreated DESC ");
            TypedQuery<Customer> query = entitymanager.createQuery(builder.toString(), Customer.class);
         
            for(Tuple<String, Object> pair : parameterList){
                query.setParameter(pair.getLeft(), pair.getRight());
            }
            for(Tuple<String, Object> pair : searchStringParamList){
                query.setParameter(pair.getLeft(), "%" + pair.getRight() + "%");
            }
            
            query.setFirstResult(offset);
            query.setMaxResults(count);
            List<Customer> customerResult = query.getResultList();
            Collections.sort(customerResult);
            return customerResult;
//        }
    }

    private String AddAndString(String condition, Boolean applyWhereClause){
        if(applyWhereClause){
            applyWhereClause = false;
            return "WHERE " + condition + " ";
        }else{
            return "AND " + condition + " ";
        }
    }
    
    private void createSearchWhereClause(StringBuilder builder, String searchWords, List<Tuple<String, Object>> parameterList) {
        String[] searchWord = searchWords.split("[\\s]");
        int count = 0;
        for (String word : searchWord) {
            String parameter = "searchword" + count;
            if(count > 0){
                builder.append(" AND ");
            }
            builder.append(" ( ");
            appendString(builder, "customer.lowerCaseLastName LIKE :" , parameter);
            appendString(builder, "OR customer.lowerCaseName LIKE :" , parameter);
            appendString(builder, "OR customer.mobilePhone LIKE :" , parameter);
            builder.append(" ) ");
            parameterList.add(new Tuple(parameter, word.toLowerCase()));
            count++;
        }
    }
    
    private void appendString(StringBuilder builder, String string, String parameter){
        builder.append(string);
        builder.append(parameter);
        builder.append(" ");
    }
}
