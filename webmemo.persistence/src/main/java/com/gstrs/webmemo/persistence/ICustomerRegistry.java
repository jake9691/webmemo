/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gstrs.webmemo.persistence;

import com.gstrs.webmemo.model.entities.Customer;
import com.gstrs.webmemo.model.entities.Store;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Jacob
 */
@Local
public interface ICustomerRegistry extends IDAO<Customer, Long> {

    public void moveAllCustomerFromStore1ToStore2(Store store1, Store store2);

    public List<Customer> findRangeOfCustomerForOrganization(long organizationId, int firstPosition, int numberOfResults);
    
    public int customerCountForOrganization(long organizationId);
    
    public List<Customer> findRangeOfCustomerForStore(long storeId, int firstPosition, int numberOfResults);
    
    public List<Customer> searchForCustomer(String search, Long storeId, int offset, int count);
}
