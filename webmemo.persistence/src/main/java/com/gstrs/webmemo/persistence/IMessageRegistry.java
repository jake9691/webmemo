/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gstrs.webmemo.persistence;

import com.gstrs.webmemo.model.entities.Conversation;
import com.gstrs.webmemo.model.entities.Message;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Jacob
 */
@Local
public interface IMessageRegistry extends IDAO<Message, Long>{
    public List<Message> findRangeWithMessagesForConversation(long conversationId, int first, int n);
}
