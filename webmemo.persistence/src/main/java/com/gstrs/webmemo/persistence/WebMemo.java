/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gstrs.webmemo.persistence;

import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 * A Facade for all the registries.
 *
 * @author Jacob
 */
@Stateless
public class WebMemo implements IWebMemo {

    private IUserRegistry userRegistry;

    private IOrganizationRegistry organizationRegistry;

    private IProviderRegistry providerRegistry;

    private IStoreRegistry storeRegistry;

    private ICustomerRegistry customerRegistry;

    private IEmployeeRegistry employeeRegistry;

    private IConversationRegistry conversationRegistry;

    private IMessageRegistry messageRegistry;

    private IReminderRegistry reminderRegistry;

    private IMemoRegistry memoRegistry;

    private IOrderRegistry orderRegistry;

    private IReceptionRegistry receptionRegistry;
    
    private ISalesmanRegistry salesmanRegistry;

    public WebMemo() {

    }

    @EJB
    @Override
    public void setUserRegistry(IUserRegistry userRegistry) {
        this.userRegistry = userRegistry;
    }

    @EJB
    @Override
    public void setOrganizationRegistry(IOrganizationRegistry organizationRegistry) {
        this.organizationRegistry = organizationRegistry;
    }

    @EJB
    @Override
    public void setProviderRegistry(IProviderRegistry providerRegistry) {
        this.providerRegistry = providerRegistry;
    }

    @EJB
    @Override
    public void setStoreRegistry(IStoreRegistry storeRegistry) {
        this.storeRegistry = storeRegistry;
    }

    @EJB
    @Override
    public void setCustomerRegistry(ICustomerRegistry customerRegistry) {
        this.customerRegistry = customerRegistry;
    }

    @EJB
    @Override
    public void setEmployeeRegistry(IEmployeeRegistry employeeRegistry) {
        this.employeeRegistry = employeeRegistry;
    }

    @EJB
    @Override
    public void setConversationRegistry(IConversationRegistry conversationRegistry) {
        this.conversationRegistry = conversationRegistry;
    }

    @EJB
    @Override
    public void setMessageRegistry(IMessageRegistry messageRegistry) {
        this.messageRegistry = messageRegistry;
    }

    @EJB
    @Override
    public void setReminderRegistry(IReminderRegistry reminderRegistry) {
        this.reminderRegistry = reminderRegistry;
    }

    @EJB
    @Override
    public void setMemoRegistry(IMemoRegistry memoRegistry) {
        this.memoRegistry = memoRegistry;
    }

    @EJB
    @Override
    public void setOrderRegistry(IOrderRegistry orderRegistry) {
        this.orderRegistry = orderRegistry;
    }

    @EJB
    @Override
    public void setReceptionRegistry(IReceptionRegistry receptionRegistry) {
        this.receptionRegistry = receptionRegistry;
    }
    
    @EJB
    @Override
    public void setSalesmanRegistry(ISalesmanRegistry salesmanRegistry) {
        this.salesmanRegistry = salesmanRegistry;
    }

    @Override
    public IUserRegistry getUserRegistry() {
        return userRegistry;
    }

    @Override
    public IOrganizationRegistry getOrganizationRegistry() {
        return organizationRegistry;
    }

    @Override
    public IProviderRegistry getProviderRegistry() {
        return providerRegistry;
    }

    @Override
    public IStoreRegistry getStoreRegistry() {
        return storeRegistry;
    }

    @Override
    public ICustomerRegistry getCustomerRegistry() {
        return customerRegistry;
    }

    @Override
    public IEmployeeRegistry getEmployeeRegistry() {
        return employeeRegistry;
    }

    @Override
    public IConversationRegistry getConversationRegistry() {
        return conversationRegistry;
    }

    @Override
    public IMessageRegistry getMessageRegistry() {
        return messageRegistry;
    }

    @Override
    public IReminderRegistry getReminderRegistry() {
        return reminderRegistry;
    }

    @Override
    public IMemoRegistry getMemoRegistry() {
        return memoRegistry;
    }

    @Override
    public IOrderRegistry getOrderRegistry() {
        return orderRegistry;
    }

    @Override
    public IReceptionRegistry getReceptionRegistry() {
        return receptionRegistry;
    }

    @Override
    public ISalesmanRegistry getSalesmanRegistry() {
        return salesmanRegistry;
    }
}
