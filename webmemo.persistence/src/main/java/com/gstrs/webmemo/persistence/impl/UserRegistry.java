/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gstrs.webmemo.persistence.impl;

import com.gstrs.webmemo.model.entities.Organization;
import com.gstrs.webmemo.model.entities.Permission;
import com.gstrs.webmemo.model.entities.User;
import com.gstrs.webmemo.persistence.AbstractDAO;
import com.gstrs.webmemo.persistence.IUserRegistry;
import java.util.HashSet;
import com.gstrs.webmemo.persistence.util.PasswordTool;
import com.gstrs.webmemo.persistence.util.QueryHelper;
import com.gstrs.webmemo.persistence.validation.ViolationWrapper;
import java.util.List;
import java.util.Set;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Jacob
 */
@Stateless
public class UserRegistry extends AbstractDAO<User, Long> implements IUserRegistry {

    @PersistenceContext
    private EntityManager entitymanager;
    
    public UserRegistry() {
        super(User.class);
    }
    
    @Override
    public Set<ViolationWrapper> create(User user) {
        PasswordTool tool = new PasswordTool();

        user.setPassword(tool.hash(user.getPassword()));
        
        return super.create(user);
    }

    @Override
    public Set<ViolationWrapper> update(User user) {
        return super.update(user);
    }

    @Override
    public boolean validatePassword(User user, String password) {
        PasswordTool tool = new PasswordTool();

        return tool.check(password, user.getPassword());
    }

    @Override
    protected EntityManager getEntityManager() {
        return entitymanager;
    }

    @Override
    public User getByUsername(String username) {
        TypedQuery<User> q = entitymanager.createQuery("SELECT u FROM User u WHERE u.username = :username", User.class).setParameter("username", username);
        return QueryHelper.first(q);
    }

    @Override
    public List<User> findUsersForOrganization(Organization org) {
        TypedQuery<User> q = entitymanager.createQuery("SELECT DISTINCT u FROM User u INNER JOIN u.organizationList o WHERE o = :org", User.class);
        q.setParameter("org", org);
        return q.getResultList();
    }

    @Override
    protected Set<ViolationWrapper> validateConstraints(User t) {
        TypedQuery<User> q = entitymanager.createQuery("SELECT u FROM User u WHERE u.username = :username AND u.id != :id", User.class);
        q.setParameter("username", t.getUsername());
        q.setParameter("id", t.getId());
        Set<ViolationWrapper> uniquenessViolations = new HashSet<>();
        if (q.getResultList().size() > 0) {
            uniquenessViolations.add(new ViolationWrapper("webmemo.entities.user.username.Unique", super.getLocaleList(), User.class, true));
        }
        if (t.getPermission().equals(Permission.EMPLOYEE) || t.getPermission().equals(Permission.EMPLOYEE_EXTENDED)) {
            if (t.getStore() == null) {
                uniquenessViolations.add(new ViolationWrapper("webmemo.entities.user.store.NotNull", super.getLocaleList(), User.class, true));
            }
        }
        return uniquenessViolations;
    }

    @Override
    protected Set<ViolationWrapper> validateDeleteConstraints(User t) {
        return new HashSet<>();
    }

    @Override
    public void changePassword(User user, String oldPassword, String newPassword) {
        if (validatePassword(user, oldPassword)) {
            PasswordTool tool = new PasswordTool();

            user.setPassword(tool.hash(newPassword));
            update(user);
        }
    }

    @Override
    public User findUserForStore(Long storeId) {
        TypedQuery<User> query = entitymanager.createQuery("SELECT u FROM User u WHERE u.store.id = :storeId",User.class)
                .setParameter("storeId", storeId);
        
        return QueryHelper.first(query);
    }
}
