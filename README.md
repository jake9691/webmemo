# WebMemo

### Purpose

WebMemo is a management system for companies and organizations. The system makes it possible to register organizations. It then enables organizations to add stores belonging to the organization, and then assign employees to those stores. The purpose is to create Memos. A Memo is a memory note, but in most cases it is also an order from a customer.

Example of Use Case:

1. Customer wants to order a product that is not in stock in a store (for example a football).

2. Employee quickly enter customer and product information into WebMemo

3. Customer leaves the store

4. At a later point the employee uses WebMemo to summarize all orders for a particular provider(for example Adidas).

5. The employee forwards the order to the provider(Adidas).

6. Eventually the products arrive to the store.

7. Employee finds the customers for the products using WebMemo

8. The employee notify the customers, usually be a textmessage.


### Requirements

Server side: 

 * Java 1.8
 * GlassFish 4.1 with JDBC Postgres Driver 4.1
 * PostgreSQL 

Server Side Development: 

 * Netbeans 8.0.1
 * Maven
 * JDK 1.8

For the client side requirements, I refer to [the client readme document](client/README.md).

### Building 

Before you build the server side code, it is important that you first build the client side using the guide I referred to above. When building the client side is out of the way, you can build the application by right-clicking webmemo.app and selecting "Build". This should execute maven, get all the dependencies, build all the projects, as well as running all the tests. If the build succeeded and the tests ran (see Note) then you should be able to run the Webmemo.web project, which contains the REST service layer part of the application.

If you don't have a local postgres installation, you can use our global dev server. It has the JDNI name "java:app/jdbc/webmemopsql", which you will have to set in the webmemo.persistence .../META-INF/persistence.xml document. 

###Note: 

We have had some trouble running all the tests in sequence. Most of us were only able to run a single Test file successfully at a time. If we chose to test multiple files at a time, only the first one in the list succeeded.