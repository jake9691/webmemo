/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gstrs.webmemo.model.test;

import com.gstrs.webmemo.model.utils.ValidationAndFormatting;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Jacob
 */
public class ValidationAndFormattingTest {

    @Test
    public void testIsValidEmailAddress() {
        assertTrue(ValidationAndFormatting.isValidEmailAddress("email@domain.com"));
        assertTrue(ValidationAndFormatting.isValidEmailAddress("firstname.lastname@domain.com"));
        assertTrue(ValidationAndFormatting.isValidEmailAddress("email@subdomain.domain.com"));
        assertTrue(ValidationAndFormatting.isValidEmailAddress("firstname+lastname@domain.com"));
        assertTrue(ValidationAndFormatting.isValidEmailAddress("1234567890@domain.com"));
        assertTrue(ValidationAndFormatting.isValidEmailAddress("email@domain-one.com"));
        assertTrue(ValidationAndFormatting.isValidEmailAddress("_______@domain.com"));
        assertTrue(ValidationAndFormatting.isValidEmailAddress("email@domain.name"));
        assertTrue(ValidationAndFormatting.isValidEmailAddress("email@domain.co.jp"));
        assertTrue(ValidationAndFormatting.isValidEmailAddress("firstname-lastname@domain.com"));

        assertFalse(ValidationAndFormatting.isValidEmailAddress(""));
        assertFalse(ValidationAndFormatting.isValidEmailAddress("plainaddress"));
        assertFalse(ValidationAndFormatting.isValidEmailAddress("#@%^%#$@#$@#.com"));
        assertFalse(ValidationAndFormatting.isValidEmailAddress("@domain.com"));
        assertFalse(ValidationAndFormatting.isValidEmailAddress("Joe Smith <email@domain.com>"));
        assertFalse(ValidationAndFormatting.isValidEmailAddress("email.domain.com"));
        assertFalse(ValidationAndFormatting.isValidEmailAddress("email@domain@domain.com"));
        assertFalse(ValidationAndFormatting.isValidEmailAddress(".email@domain.com"));
        assertFalse(ValidationAndFormatting.isValidEmailAddress("email@domain.com (Joe Smith)"));
        assertFalse(ValidationAndFormatting.isValidEmailAddress("email@domain"));
        assertFalse(ValidationAndFormatting.isValidEmailAddress("email@111.222.333.44444"));
    }

    @Test
    public void testIsValidPhoneNumber() {
        assertTrue(ValidationAndFormatting.isValidPhoneNumber("0703477630"));
        assertTrue(ValidationAndFormatting.isValidPhoneNumber("+46703477630"));
        assertTrue(ValidationAndFormatting.isValidPhoneNumber("+47703477630"));
        assertTrue(ValidationAndFormatting.isValidPhoneNumber("0046703477630"));
        assertTrue(ValidationAndFormatting.isValidPhoneNumber("050171730"));
        assertTrue(ValidationAndFormatting.isValidPhoneNumber("004650171730"));
        assertTrue(ValidationAndFormatting.isValidPhoneNumber("+4650171730"));
        assertTrue(ValidationAndFormatting.isValidPhoneNumber("0850829000"));
        assertTrue(ValidationAndFormatting.isValidPhoneNumber("0046850829000"));
        assertTrue(ValidationAndFormatting.isValidPhoneNumber("+46850829000"));
        assertTrue(ValidationAndFormatting.isValidPhoneNumber("031987619"));
        assertTrue(ValidationAndFormatting.isValidPhoneNumber("004631987619"));
        assertTrue(ValidationAndFormatting.isValidPhoneNumber("+4631987619"));

        assertFalse(ValidationAndFormatting.isValidPhoneNumber(""));
        assertFalse(ValidationAndFormatting.isValidPhoneNumber("0546312"));
        assertFalse(ValidationAndFormatting.isValidPhoneNumber("3456fd36541"));

    }

    @Test
    public void testFormatPhoneNumber() {
        assertTrue(ValidationAndFormatting.formatPhoneNumber("+46703477630").equals("+46703477630"));
        assertTrue(ValidationAndFormatting.formatPhoneNumber("070-34 776 30").equals("0703477630"));
        assertTrue(ValidationAndFormatting.formatPhoneNumber("0046 8 508 290 00").equals("0046850829000"));
        assertTrue(ValidationAndFormatting.formatPhoneNumber("+46 501 717 30").equals("+4650171730"));
    }
}
