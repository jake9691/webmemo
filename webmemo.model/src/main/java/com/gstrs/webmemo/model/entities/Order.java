package com.gstrs.webmemo.model.entities;

import com.gstrs.webmemo.model.abstracts.AbstractEntity;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * Added to the memo when its ordered from the provider.
 * 
 * Just contains simple data about who ordered it, at what date, and whats the orcdernumber for the item in the stores data system is. And a few things more
 * 
 * @author Jacob
 */
@Entity
@Table(name = "ORDER_TABLE")
public class Order extends AbstractEntity {

    @Column
    private String providerId; //The providers id for the order

    @Column
    private String orderId; //The stores id for the order

    @NotNull(message = "{webmemo.entities.order.creator.NotNull}")
    @ManyToOne
    private Employee creator;
    
    @Size(max = 2047, message = "{webmemo.entities.order.remarks.Size}")
    @Column(length = 2047)
    private String remarks;

    public Order() {

    }

    public Order(Employee creator) {
        super(new Date());
        this.creator = creator;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Employee getCreator() {
        return creator;
    }

    public void setCreator(Employee creator) {
        this.creator = creator;
    }
}
