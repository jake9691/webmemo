/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gstrs.webmemo.model.entities;

import com.gstrs.webmemo.model.abstracts.AbstractEntity;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.*;
import static javax.persistence.TemporalType.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlTransient;

/**
 * 
 * A User that is used for managing permission to the data.
 *
 * @author Jacob
 */
@Entity
@Table(name = "USER_TABLE")
public class User extends AbstractEntity {
    
    @NotNull(message = "{webmemo.entities.user.permission.NotNull}")
    @Enumerated(EnumType.STRING)
    private Permission permission;

    @NotNull(message = "{webmemo.entities.user.username.NotNull}")
    @Size(min = 5, message = "{webmemo.entities.user.username.Size}")
    @Pattern(regexp = "[a-z0-9_@#&\\-.]*", message = "{webmemo.entities.user.username.Pattern}")
    @Column(unique = true)
    private String username;
    
    @NotNull(message = "{webmemo.entities.user.password.NotNull}")
    @Size(min = 6, message = "{webmemo.entities.user.password.Size}")
    @Column
    private String password;
    
    @ManyToOne
    private Store store;
    
    @Temporal(TIMESTAMP)
    private Date lastLogin;

    @ManyToMany
    @JoinTable(name = "USER_OWNS_ORGANIZATIONS",
            joinColumns = {
                @JoinColumn(name = "USER_ID", referencedColumnName = "ID")},
            inverseJoinColumns = {
                @JoinColumn(name = "ORGANIZATION_ID", referencedColumnName = "ID")})
    List<Organization> organizationList;

    
    public User() {
    }
    
    public User(String username, Permission permission) {
        super(new Date());
        this.username = username;
        this.permission = permission;
    }
    
    public User(String username, String password, Permission permission) {
        super(new Date());
        this.password = password;
        this.username = username;
        this.permission = permission;
    }
    
    public User(String username, String password, Permission permission, Store store) {
        super(new Date());
        this.password = password;
        this.username = username;
        this.permission = permission;
        this.store = store;
    }

    public Permission getPermission() {
        return this.permission;
    }

    public void setPermission(Permission permission) {
        this.permission = permission;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    
    @XmlTransient
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }
    
    @XmlTransient
    public List<Organization> getOrganizationList() {
        return organizationList;
    }

    public boolean addOrganization(Organization organization) {
        if (organizationList == null) {
            organizationList = new ArrayList<>();
        }
        return organizationList.add(organization);
    }
    
    public boolean removeOrganization(Organization organization) {
        if(organizationList == null){
            return false;
        }
        return this.organizationList.remove(organization);
    }

    public void setOrganizationList(List<Organization> organizationList) {
        this.organizationList = organizationList;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }
    
}
