/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gstrs.webmemo.model.entities;

import com.gstrs.webmemo.model.abstracts.AbstractContact;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlTransient;

/**
 * 
 * Represent an organizaiton which could have many stores. To simplify it a bit you can think of it as: a memo is created within a store.
 * Modeling the organization and stores just makes it possible to sell the "webMemo" service to several organization.
 *
 * @author Jacob
 */
@Entity
@Table(name = "ORGANIZATION", uniqueConstraints = { @UniqueConstraint( columnNames = "LOWERCASENAME")})
public class Organization extends AbstractContact {
    
    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "organization")
    @JoinColumn(name = "ORGANIZATION_FK")
    private List<Store> storeList;
    
    @OneToMany(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "ORGANIZATION_FK")
    private List<Employee> employeeList;
    
    @OneToMany(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "ORGANIZATION_FK")
    private List<Customer> customerList;
    
    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "organization")
    private List<Provider> providerList;
    
    public Organization() {

    }
    
    public Organization(String name) {
        super(name);
    }
    
    @XmlTransient
    public List<Store> getStoreList() {
        return storeList;
    }

    public void setStoreList(List<Store> storeList) {
        this.storeList = storeList;
    }

    public boolean addStore(Store store) {
        if(storeList == null){
            storeList = new ArrayList<>();
        }
        return this.storeList.add(store);
    }

    public boolean removeStore(Store store) {
        if(storeList == null){
            return false;
        }
        return this.storeList.remove(store);
    }
    
    @XmlTransient
    public List<Employee> getEmployeeList() {
        return employeeList;
    }

    public void setEmployeeList(List<Employee> employeeList) {
        this.employeeList = employeeList;
    }

    public boolean addEmployee(Employee employee) {
        if(employeeList == null){
            employeeList = new ArrayList<>();
        }
        return this.employeeList.add(employee);
    }

    public boolean removeEmployee(Employee employee) {
        if(employeeList == null){
            return false;
        }
        return this.employeeList.remove(employee);
    }
    
    @XmlTransient
    public List<Customer> getCustomerList() {
        return customerList;
    }

    public void setCustomerList(List<Customer> customerList) {
        this.customerList = customerList;
    }

    public boolean addCustomer(Customer customer) {
        if(customerList == null){
            customerList = new ArrayList<>();
        }
        return this.customerList.add(customer);
    }

    public boolean removeCustomer(Customer customer) {
        if(customerList == null){
            return false;
        }
        return this.customerList.remove(customer);
    }
    
    @XmlTransient
    public List<Provider> getProviderList() {
        return providerList;
    }

    public void setProviderList(List<Provider> providerList) {
        this.providerList = providerList;
    }
    
    public boolean addProvider(Provider provider) {
        if(providerList == null){
            providerList = new ArrayList<>();
        }
        return this.providerList.add(provider);
    }

    public boolean removeProvider(Provider provider) {
        if(providerList == null){
            return false;
        }
        return this.providerList.remove(provider);
    }
    
}
