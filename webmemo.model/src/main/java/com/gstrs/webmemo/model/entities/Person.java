package com.gstrs.webmemo.model.entities;

import com.gstrs.webmemo.model.abstracts.AbstractContact;
import com.gstrs.webmemo.model.interfaces.IPerson;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlTransient;

/**
 * A person here was no intended to be an entity. But since JPA only can map things that is entity to each other a person had to be created in the database.
 * The reason is that a conversation contains persons. That means that a conversation could contain both employees and customer etc.
 *
 * @author Jacob
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "PERSONTYPE")
@Table(name = "PERSON")
public class Person extends AbstractContact implements IPerson {

    @NotNull(message = "{webmemo.entities.person.lastName.NotNull}")
    @Size(min = 1, message = "{webmemo.entities.person.lastName.Size}")
    @Column
    private String lastName;
    
    @Column
    private String lowerCaseLastName;
    
    @ManyToMany(mappedBy = "participantList")
    private List<Conversation> conversationList;

    public Person() {

    }

    public Person(String name, String lastname) {
        super(name);
        this.lastName = lastname;
        this.lowerCaseLastName = lastname == null ? null : lastname.toLowerCase();
    }

    @Override
    public String getLastName() {
        return lastName;
    }

    @Override
    public void setLastName(String lastname) {
        this.lastName = lastname;
        this.lowerCaseLastName = lastname == null ? null : lastname.toLowerCase();
    }
    
    @XmlTransient
    public List<Conversation> getConversationList() {
        return conversationList;
    }

    public boolean addConversation(Conversation conv) {
        if (conversationList == null) {
            conversationList = new ArrayList<>();
        }
        return conversationList.add(conv);
    }
    
    public boolean removeConversation(Conversation conv) {
        if(conversationList == null){
            return false;
        }
        return conversationList.remove(conv);
    }
    
    @XmlTransient
    public String getLowerCaseLastName() {
        return lowerCaseLastName;
    }

    public void setLowerCaseLastName(String lowerCaseLastName) {
        this.lowerCaseLastName = lowerCaseLastName;
    }

    public void setConversationList(List<Conversation> conversationList) {
        this.conversationList = conversationList;
    }
    
}
