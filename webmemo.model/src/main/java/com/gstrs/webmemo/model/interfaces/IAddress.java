package com.gstrs.webmemo.model.interfaces;

/**
 *
 * @author Jacob
 */
public interface IAddress {

    public void setAddress(String address);

    public String getAddress();

    public void setPostalCode(String postalCode);

    public String getPostalCode();

    public void setCity(String city);

    public String getCity();
}
