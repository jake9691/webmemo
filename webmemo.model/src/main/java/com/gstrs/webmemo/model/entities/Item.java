package com.gstrs.webmemo.model.entities;

import com.gstrs.webmemo.model.abstracts.AbstractEntity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 * Item is a part of a memo. The item is the product. It could be a football, a shoe or a pair of socks
 *
 * @author Jacob
 */
@Entity
@Table(name = "ITEM")
public class Item extends AbstractEntity {

    @Column
    private String name;
    @Column
    private String itemNumber;
    @Column
    private String providerItemNumber;
    @Column
    private String agreedPrice;
    @Column
    private String color;
    @Column
    private String quantity;

    @Size(max = 2047, message = "{webmemo.entities.item.size.Size}")
    @Column(name = "SIZE_COL", length = 2047)
    private String size;

    @ManyToOne
    private Provider provider;

    public Item() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getItemNumber() {
        return itemNumber;
    }

    public void setItemNumber(String itemNumber) {
        this.itemNumber = itemNumber;
    }

    public String getProviderItemNumber() {
        return providerItemNumber;
    }

    public void setProviderItemNumber(String providerItemNumber) {
        this.providerItemNumber = providerItemNumber;
    }

    public String getAgreedPrice() {
        return agreedPrice;
    }

    public void setAgreedPrice(String agreedPrice) {
        this.agreedPrice = agreedPrice;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }
    
}
