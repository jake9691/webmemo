package com.gstrs.webmemo.model.utils;


/**
 * TODO. THINK WE SHOULD REMOVE THIS EVENTUALLY. WE WILL SEE WHEN WE START TO IMPLEMENT
 * ADDED IT TO THE BEAN VALIDATION INSTEAD
 * 
 * @author Jacob
 */
public class ValidationAndFormatting {

    public static boolean isValidEmailAddress(String email) {
        return email.matches("\\b[A-z0-9._%+-]+@[A-z0-9.-]+\\.[A-z]{2,4}\\b");
    }

    public static boolean isValidPhoneNumber(String number) {
        return (number.matches("^\\+\\d{2}\\d{7,9}$|^(0{2}\\d{2}\\d{7,9})$|^(?!0{2})0\\d{7,9}$"));
    }

    public static String formatPhoneNumber(String phone) {
        return phone.replaceAll("[^0123456789+]*", "");
    }
}
