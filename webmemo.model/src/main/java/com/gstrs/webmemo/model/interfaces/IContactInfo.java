package com.gstrs.webmemo.model.interfaces;

/**
 * See the abstract class "abstractContact" for more info
 *
 * @author Jacob
 */
public interface IContactInfo {

    public void setName(String name);

    public String getName();

    public void setMobilePhone(String mobile);

    public String getMobilePhone();

    public void setExtraPhoneNumber(String extra);

    public String getExtraPhoneNumber();

    public void setEmail(String email);

    public String getEmail();
}
