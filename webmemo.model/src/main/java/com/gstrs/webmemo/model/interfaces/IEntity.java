/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gstrs.webmemo.model.interfaces;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Davíð
 */
public interface IEntity extends Serializable, Comparable<IEntity> {

    public long getId();
    
    public void setId(long id);

    public Date getDateCreated();

    public Date getModified();

    public Date getDeleted();
    
    public boolean isDeleted();
    
    public void setIsDeleted(boolean isDeleted);
}
