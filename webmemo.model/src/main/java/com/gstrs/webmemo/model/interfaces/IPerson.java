package com.gstrs.webmemo.model.interfaces;

/**
 * See the enity Person for more info
 *
 * @author Jacob
 */
public interface IPerson extends IContactInfo {

    public String getLastName();

    public void setLastName(String lastName);
}
