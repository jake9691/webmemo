/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gstrs.webmemo.model.entities;

/**
 * 
 * Permission for a user
 *
 * @author Jacob
 */
public enum Permission {
    ADMINISTRATOR,
    ORGANIZATIONOWNER,
    EMPLOYEE_EXTENDED,
    EMPLOYEE,
    ANONYMOUS;
}
