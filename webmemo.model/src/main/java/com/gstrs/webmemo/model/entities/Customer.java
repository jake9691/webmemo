package com.gstrs.webmemo.model.entities;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * 
 * @author Jacob
 */
@Entity
@DiscriminatorValue("CUSTOMER")
@Table(name = "CUSTOMER")
public class Customer extends Person {

    @Column
    private String association;

    @NotNull(message = "{webmemo.entities.customer.customerCreatedInStore.NotNull}")
    @ManyToOne
    private Store customerCreatedInStore;

    @Embedded
    @Valid
    private Address address;

    public Customer() {

    }
    
    public Customer(String name, String lastname, String mobilePhone, Store customerCreatedInStore) {
        super(name, lastname);
        super.setMobilePhone(mobilePhone);
        this.customerCreatedInStore = customerCreatedInStore;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getAssociation() {
        return association;
    }

    /**
     * An association could be a football association like 'AIK'....
     *
     * @param association
     */
    public void setAssociation(String association) {
        this.association = association;
    }

    public Store getCustomerCreatedInStore() {
        return customerCreatedInStore;
    }

    public void setCustomerCreatedInStore(Store customerCreatedInStore) {
        this.customerCreatedInStore = customerCreatedInStore;
    }
}
