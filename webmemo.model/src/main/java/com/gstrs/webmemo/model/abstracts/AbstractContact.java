package com.gstrs.webmemo.model.abstracts;

import com.gstrs.webmemo.model.interfaces.IContactInfo;
import com.gstrs.webmemo.model.utils.ValidationAndFormatting;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Represent a person(customer, employee or salesman), a company or something that you might want to contact
 *
 * @author Jacob
 */
@MappedSuperclass
public abstract class AbstractContact extends AbstractEntity implements IContactInfo {

    @NotNull(message = "{webmemo.abstract.abstractcontact.name.NotNull}")
    @Size(min = 1, message = "{webmemo.abstract.abstractcontact.name.Size}")
    @Column
    private String name;
    
    //This is just because you cant make a case insensitive unique constraint in JPA. So we have to store the lowercase of name
    @Column
    private String lowerCaseName;

    @Column
    @Pattern(regexp = "\\b[A-z0-9._%+-]+@[A-z0-9.-]+\\.[A-z]{2,4}\\b", message = "{webmemo.abstract.abstractcontact.email.Pattern}")
    private String email;
    
    @Column
    @Pattern(regexp = "^\\+\\d{2}\\d{7,9}$|^(0{2}\\d{2}\\d{7,9})$|^(?!0{2})0\\d{7,9}$", message = "{webmemo.abstract.abstractcontact.mobilePhone.Pattern}")
    private String mobilePhone;
    
    @Column
    private String extraPhone;

    public AbstractContact() {

    }

    public AbstractContact(String name) {
        super(new Date());
        this.name = name;
        
        if (name != null) {
            this.lowerCaseName = name.toLowerCase();
        }    
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
        this.lowerCaseName = name == null ? null : name.toLowerCase();
    }

    @Override
    public String getMobilePhone() {
        return mobilePhone;
    }

    @Override
    public void setMobilePhone(String mobile) {
        if(mobile == null){
            this.mobilePhone = null;
        }else{
            this.mobilePhone = ValidationAndFormatting.formatPhoneNumber(mobile);
        }
    }

    @Override
    public void setExtraPhoneNumber(String extra) {
        if(extra == null){
            this.extraPhone = null;
        }else{
            this.extraPhone = ValidationAndFormatting.formatPhoneNumber(extra);
        }
        
    }

    @Override
    public String getExtraPhoneNumber() {
        return this.extraPhone;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String getEmail() {
        return this.email;
    }
    
    @XmlTransient
    public String getLowerCaseName() {
        return lowerCaseName;
    }

    public String getExtraPhone() {
        return extraPhone;
    }

    public void setLowerCaseName(String lowerCaseName) {
        this.lowerCaseName = lowerCaseName;
    }

    public void setExtraPhone(String extraPhone) {
        this.extraPhone = extraPhone;
    }
    
}
