package com.gstrs.webmemo.model.entities;

import com.gstrs.webmemo.model.abstracts.AbstractEntity;
import com.gstrs.webmemo.model.interfaces.IMessage;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * 
 * @author Jacob
 */
@Entity
@Table(name = "MESSAGE")
public class Message extends AbstractEntity implements IMessage {
    
    //Could be null if a person is deleted. Then the message will be deleted when the conversation is.
    @ManyToOne
    private Person sender;

    @Size(min = 1, max = 265, message = "{webmemo.entities.message.message.Size}")//otherwise to long to be a textmessage
    @Column(length = 265)
    private String message;

    public Message(){
        
    }
    
    public Message(Person sender, String message) {
        super(new Date());
        this.sender = sender;
        this.message = message;
    }

    @Override
    public Person getSender() {
        return sender;
    }

    @Override
    public String getMessage() {
        return message;
    }
    
    @Override
    public void removeSender(){
        sender = null;
    }

    public void setSender(Person sender) {
        this.sender = sender;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
}
