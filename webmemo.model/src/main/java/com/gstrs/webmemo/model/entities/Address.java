package com.gstrs.webmemo.model.entities;

import com.gstrs.webmemo.model.interfaces.IAddress;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 * 
 * @author Jacob
 */
@Embeddable
public class Address implements IAddress, Serializable {

    @NotNull(message = "{webmemo.entities.address.address.NotNull}")
    @Size(min = 1, message = "{webmemo.entities.address.address.Size}")
    @Column
    private String address;
    @Column
    private String postalCode;
    @Column
    private String city;

    public Address() {
    }
    
    public Address(String address) {
        this.address = address;
    }
    
    public Address(String address, String postalCode, String city) {
        super();
        this.address = address;
        this.postalCode = postalCode;
        this.city = city;
    }

    @Override
    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String getAddress() {
        return address;
    }

    @Override
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    @Override
    public String getPostalCode() {
        return postalCode;
    }

    @Override
    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String getCity() {
        return city;
    }
}
