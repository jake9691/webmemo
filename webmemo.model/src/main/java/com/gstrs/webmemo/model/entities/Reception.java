/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gstrs.webmemo.model.entities;

import com.gstrs.webmemo.model.abstracts.AbstractEntity;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * added to a memo when the item is recieved in the store.
 *
 * @author Jacob
 */
@Entity
@Table(name = "RECEPTION")
public class Reception extends AbstractEntity {

    @NotNull(message = "{webmemo.entities.reception.creator.NotNull}")
    @ManyToOne
    private Employee creator;

    @Size(max = 2047, message = "{webmemo.entities.reception.remarks.Size}")
    @Column(length = 2047)
    private String remarks;
    
    @NotNull(message = "{webmemo.entities.reception.isCostumerNotified.NotNull}")
    @Column(nullable = false)
    private boolean isCostumerNotified;
    
    @Column
    private String storageLocation;
   
    public Reception() {

    }

    public Reception(Employee creator) {
        super(new Date());
        this.creator = creator;
    }

    public Employee getCreator() {
        return creator;
    }

    public void setCreator(Employee creator) {
        this.creator = creator;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public void setIsCostumerNotified(boolean isCostumerNotified) {
        this.isCostumerNotified = isCostumerNotified;
    }
    
    public String getStorageLocation() {
        return storageLocation;
    }

    public void setStorageLocation(String storageLocation) {
        this.storageLocation = storageLocation;
    }

    public boolean isIsCostumerNotified() {
        return isCostumerNotified;
    }
    
}
