package com.gstrs.webmemo.model.entities;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 
 * A Provider usually has several salesman. It is useful for the people in the store to have their contact information if they need to contact them.
 * 
 * HAD NOT TIME TO IMPLEMENT THIS IN GUI!!!
 * 
 * @author Jacob
 */
@Entity
@DiscriminatorValue("SALESMAN")
@Table(name = "SALESMAN")
public class Salesman extends Person {
    
    public Salesman() {

    }
    
    public Salesman(String name, String lastname, String mobilePhone) {
        super(name, lastname);
        super.setMobilePhone(mobilePhone);
    }
    
}
