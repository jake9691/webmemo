/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gstrs.webmemo.model.abstracts;

import com.gstrs.webmemo.model.interfaces.IEntity;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * The super class entity that all the entities inherits from.
 * Manages the id, and the dates for created and modified etc
 *
 * @author Davíð
 */
@XmlRootElement
@MappedSuperclass
public abstract class AbstractEntity implements IEntity{
    
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreated;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateModified;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateDeleted;
    
    @Column(nullable = false)
    private boolean isDeleted;

    public AbstractEntity() {
    }
    
    public AbstractEntity(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    @Override
    public long getId() {
        return id;
    }
    
    @Override
    public void setId(long id) {
        this.id = id;
    }

    @PrePersist
    protected void onPrePersist() {
        if(dateCreated == null){
            dateCreated = new Date();
        }
    }

    @PreUpdate
    protected void onPreUpdate() {
        dateModified = new Date();
    }

    @PreRemove
    protected void onPreDelete() {
        dateDeleted = new Date();
    }
    
    @Override
    public Date getDateCreated() {
        return this.dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }
    
    

    @Override
    public Date getModified() {
        return dateModified;
    }

    @Override
    public Date getDeleted() {
        return dateDeleted;
    }

    @Override
    public boolean isDeleted() {
        return isDeleted;
    }

    @Override
    public void setIsDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 13 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AbstractEntity other = (AbstractEntity) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(IEntity o) {
        return dateCreated.compareTo(o.getDateCreated());
    }
}
