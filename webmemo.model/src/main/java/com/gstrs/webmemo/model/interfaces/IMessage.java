package com.gstrs.webmemo.model.interfaces;

import java.util.Date;

/**
 *
 * @author Jacob
 */
public interface IMessage{

    public Date getDateCreated();

    public IContactInfo getSender();

    public String getMessage();
    
    public void removeSender();
}
