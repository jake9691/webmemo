package com.gstrs.webmemo.model.entities;

import com.gstrs.webmemo.model.abstracts.AbstractEntity;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * A Memo is a Memory note. It makes it possible for employees to, for example, write down "orders" from costumers for an items that is not in stock etc.
 * 
 * The flow of a memo can be a little bit confusing. But here it goes:
 * 
 * 1. A customer wants to order something that is not currently in stock.
 * 2. An employee creates a memo with a customer and an item. (Note that a memo is just a Memo, A note to remember to do the actual stuff in their other systems.)
 * 3. The customer leaves the store. The employee continues with his work.
 * 4. He could get more customer that wants something that is not in stock. He adds more Memos.
 * 5. Eventually the employee decides to order the memos. (The providers of the items usually charge a shipment cost. 
 *    So you usually want to order as many things at the same time as possible to reduce the shipment cost)
 * 6. He order the memos and then an Order object is added to the Memo with some more data.
 * 7. Eventually the items arrive to the store and someone is unboxing it.
 * 8. Now when we have the items in the store we find the memos and the related customers and notify them that their item has arrived.
 * 
 * 
 * @author Jacob
 */
@Entity
@Table(name = "MEMO")
public class Memo extends AbstractEntity {

    public enum Status {

        CREATED,
        ORDERED,
        RECEIVED;
    }

    @NotNull(message = "{webmemo.entities.memo.responsible.NotNull}")
    @ManyToOne
    private Employee responsible;

    @NotNull(message = "{webmemo.entities.memo.creator.NotNull}")
    @ManyToOne
    private Employee creator;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date remindDate;

    @Enumerated(EnumType.STRING)
    private Status status;
    
    @Valid 
    @ManyToOne
    private Customer customer;

    @Column
    private boolean isReclaim;

    @Column
    private String reclaimId;
    
    @Valid
    @NotNull(message = "{webmemo.entities.memo.item.NotNull}")
    @OneToOne(cascade = CascadeType.ALL)
    private Item item;
    
    @Valid
    @OneToOne(cascade = {CascadeType.MERGE,CascadeType.REMOVE})
    private Order order;
    
    @Valid
    @OneToOne(cascade = {CascadeType.MERGE,CascadeType.REMOVE})
    private Reception reception;

    @Column
    private String tag;

    @Size(max = 2047, message = "{webmemo.entities.memo.freeText.Size}")
    @Column(length = 2047)
    private String freeText;

    public Memo() {

    }

    public Memo(Employee creator) {
        super(new Date());
        this.creator = creator;
        this.item = new Item();
        this.status = Status.CREATED;
    }

    public Employee getResponsible() {
        return responsible;
    }

    public Employee getCreator() {
        return creator;
    }

    public Date getRemindDate() {
        return remindDate;
    }

    public Status getStatus() {
        return status;
    }

    public Customer getCustomer() {
        return customer;
    }

    public Item getItem() {
        return item;
    }

    public Order getOrder() {
        return order;
    }

    public String getTag() {
        return tag;
    }

    public void setResponsible(Employee responsible) {
        this.responsible = responsible;
    }

    public void setRemindDate(Date promisedDate) {
        this.remindDate = promisedDate;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public void setOrder(Order order) {
        if(order != null && reception == null){
            this.status = Status.ORDERED;
        }else if(reception == null && order == null){
            this.status = Status.CREATED;
        }
        this.order = order;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public void setReclaim(boolean isReclaim) {
        this.isReclaim = isReclaim;
    }

    public String getReclaimId() {
        return reclaimId;
    }

    public void setReclaimId(String reclaimId) {
        this.reclaimId = reclaimId;
    }

    public Reception getReception() {
        return reception;
    }

    public void setReception(Reception reception) {
        if(reception != null && order != null){
            this.status = Status.RECEIVED;
        }else if(reception == null && order != null){
            this.status = Status.ORDERED;
        }else if(reception == null && order == null){
            this.status = Status.CREATED;
        }
        this.reception = reception;
    }

    public String getFreeText() {
        return freeText;
    }

    public void setFreeText(String freeText) {
        this.freeText = freeText;
    }

    public void setCreator(Employee creator) {
        this.creator = creator;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public void setIsReclaim(boolean isReclaim) {
        this.isReclaim = isReclaim;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public boolean isIsReclaim() {
        return isReclaim;
    }
    
}
