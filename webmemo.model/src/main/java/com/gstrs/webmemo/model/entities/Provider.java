package com.gstrs.webmemo.model.entities;

import com.gstrs.webmemo.model.abstracts.AbstractContact;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlTransient;

/**
 * A provider is the one selling an item. It could be Adidas, Nike or some other brand.
 * 
 * An item contains a referene to a provider. 
 *
 * @author Jacob
 */
@Entity
@Table(name = "PROVIDER", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"ORGANIZATION_FK", "PROVIDERABBREVIATIONID"}),
    @UniqueConstraint(columnNames = {"LOWERCASENAME", "ORGANIZATION_FK"})})
public class Provider extends AbstractContact {

    @NotNull(message = "{webmemo.entities.provider.providerAbbrevationId.NotNull}")
    @Size(min = 2, message = "{webmemo.entities.provider.providerAbbrevationId.Size}")
    @Pattern(regexp = "[^a-z]*", message = "{webmemo.entities.provider.providerAbbrevationId.Pattern}") //No small characters. Will not work for swedish letter åäö. But should be good enough...
    @Column
    private String providerAbbreviationId;

    //ORGANIZATION_FK is created from the list reference in organization
    @Valid
    @Embedded
    private Address address;
    
    @OneToMany(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "PROVIDER_FK")
    private List<Salesman> salesmanList;

    @NotNull(message = "{webmemo.entities.provider.organization.NotNull}")
    @ManyToOne
    @JoinColumn(name = "ORGANIZATION_FK")
    private Organization organization;

    public Provider() {
    }

    public Provider(String name, String providerAbbreviationId, Organization organization) {
        super(name);
        this.providerAbbreviationId = providerAbbreviationId;
        this.organization = organization;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getProviderAbbreviationId() {
        return providerAbbreviationId;
    }

    public void setProviderAbbreviationId(String providerAbbrevationId) {
        this.providerAbbreviationId = providerAbbrevationId;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }
    
    @XmlTransient
    public List<Salesman> getSalesmanList() {
        return salesmanList;
    }

    public void setSalesmanList(List<Salesman> salesmanList) {
        this.salesmanList = salesmanList;
    }
    
    public boolean addSalesman(Salesman salesman){
        if(salesmanList == null){
            salesmanList = new ArrayList<>();
        }
        return salesmanList.add(salesman);
    }
    
    public boolean removeSalesman(Salesman salesman){
        if(salesmanList == null){
            return false;
        }
        return salesmanList.remove(salesman);
    }
    
}
