package com.gstrs.webmemo.model.entities;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jacob
 */
@Entity
@DiscriminatorValue("EMPLOYEE")
@Table(name = "EMPLOYEE", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"PINCODE", "STORE_FK"}),
    @UniqueConstraint(columnNames = {"ABBREVIATION", "STORE_FK"})})
public class Employee extends Person {

    @NotNull(message = "{webmemo.entities.employee.abbrevation.NotNull}")
    @Size(min = 2, max = 3, message = "{webmemo.entities.employee.abbrevation.Size}")
    @Pattern(regexp = "[^a-z]*", message = "{webmemo.entities.employee.abbrevation.Pattern}")
    @Column
    private String abbreviation;

    @Column(nullable = false)
    private boolean isResponsible;
    
    @ManyToOne
    @NotNull(message = "{webmemo.entities.employee.workingInStore.NotNull}")
    @JoinColumn(name = "STORE_FK")
    private Store workingInStore;

    @NotNull(message = "{webmemo.entities.employee.pinCode.NotNull}")
    @Size(min = 2, max = 6, message = "{webmemo.entities.employee.pinCode.Size}")
    @Pattern(regexp = "[0-9]*", message = "{webmemo.entities.employee.pinCode.Pattern}")
    @XmlTransient
    @Column
    private String pinCode;

    public Employee() {
    }

    public Employee(String name, String lastname, String abbreviation, String pinCode, Store workingInStore) {
        super(name, lastname);
        this.workingInStore = workingInStore;
        this.abbreviation = abbreviation;
        this.pinCode = pinCode;
        this.isResponsible = true;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbrevation) {
        this.abbreviation = abbrevation;
    }

    public void setIsResponsible(boolean isResponsible) {
        this.isResponsible = isResponsible;
    }
    
    @XmlTransient
    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public Store getWorkingInStore() {
        return workingInStore;
    }

    public void setWorkingInStore(Store workingInStore) {
        this.workingInStore = workingInStore;
    }

    public boolean isIsResponsible() {
        return isResponsible;
    }
}
