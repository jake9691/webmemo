package com.gstrs.webmemo.model.entities;

import java.util.List;

import com.gstrs.webmemo.model.abstracts.AbstractContact;
import java.util.ArrayList;
import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlTransient;

/**
 * A store for an organization
 *
 * @author Jacob
 */
@Entity
@Table(name = "STORE", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"LOWERCASENAME", "ORGANIZATION_FK"})})
public class Store extends AbstractContact {
    
    @OneToMany(mappedBy = "workingInStore")
    private List<Employee> employeeList;
    
    @Valid
    @Embedded
    private Address address;
    
    @OneToMany(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "STORE_FK")
    private List<Memo> memoList;
    
    @NotNull(message = "{webmemo.entities.store.organization.NotNull}")
    @ManyToOne
    @JoinColumn(name = "ORGANIZATION_FK")
    private Organization organization;

    public Store() {

    }

    public Store(String name, Organization organization) {
        super(name);
        this.organization = organization;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public boolean addEmployee(Employee employee) {
        if (employeeList == null) {
            employeeList = new ArrayList<>();
        }
        return employeeList.add(employee);
    }

    public boolean removeEmployee(Employee employee) {
        if(employeeList == null){
            return false;
        }
        return employeeList.remove(employee);
    }
    
    @XmlTransient
    public List<Employee> getEmployeeList() {
        return employeeList;
    }
    
    @XmlTransient
    public List<Memo> getMemoList() {
        return memoList;
    }

    public void setMemoList(List<Memo> memoList) {
        this.memoList = memoList;
    }

    public boolean addMemo(Memo memo) {
        if (memoList == null) {
            memoList = new ArrayList<>();
        }
        return this.memoList.add(memo);
    }

    public boolean removeMemo(Memo memo) {
        if(memoList == null){
            return false;
        }
        return this.memoList.remove(memo);
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public void setEmployeeList(List<Employee> employeeList) {
        this.employeeList = employeeList;
    }
    
}
