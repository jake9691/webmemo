/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gstrs.webmemo.model.entities;

import com.gstrs.webmemo.model.abstracts.AbstractEntity;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * A reminder is created to help employees to do their work. They have to remember to order certain things before a date.
 * Or they should be informed that another employee has unboxed an item that they ordered etc.
 * 
 * @author Jacob
 */
@Entity
@Table(name = "REMINDER")
public class Reminder extends AbstractEntity {

    @NotNull(message = "{webmemo.entities.reminder.message.NotNull}")
    @Size(min = 1, message = "{webmemo.entities.reminder.message.Size}")
    @Column(nullable = false)
    private String message;

    @ManyToOne
    private Employee employee;
    
    @NotNull(message = "webmemo.entities.reminder.validFrom.NotNull")
    @Temporal(TemporalType.TIMESTAMP)
    private Date validFrom;

    @Temporal(TemporalType.TIMESTAMP)
    private Date validTo;

    @ManyToOne
    private Memo memo;
    
    @ManyToOne
    private Store store;
    
    @Enumerated(EnumType.STRING)
    private Type reminderType;

    public enum Type {

        EMPLOYEE_CONFIRM,
        STORE_INFO,
        CUSTOMER_NOTIFIED
    }

    public Reminder() {

    }

    public Reminder(Employee employee, String message, Date validFrom) {
        super(new Date());
        this.employee = employee;
        this.message = message;
        this.reminderType = Type.EMPLOYEE_CONFIRM;
        this.validFrom = validFrom;
    }

    public Reminder(Employee employee, String message, Memo memo, Date validFrom, Type reminderType) {
        super(new Date());
        this.employee = employee;
        this.message = message;
        this.memo = memo;
        this.reminderType = reminderType;
        this.validFrom = validFrom;
    }
    
    public Reminder(Store store, String message, Date validFrom) {
        super(new Date());
        this.store = store;
        this.message = message;
        this.reminderType = Type.STORE_INFO;
        this.validFrom = validFrom;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Memo getMemo() {
        return memo;
    }

    public void setMemo(Memo memo) {
        this.memo = memo;
    }

    public Type getReminderType() {
        return reminderType;
    }

    public void setReminderType(Type reminderType) {
        this.reminderType = reminderType;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidTo() {
        return validTo;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }
    
    
}
