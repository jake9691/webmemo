/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gstrs.webmemo.model.entities;

import com.gstrs.webmemo.model.abstracts.AbstractEntity;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlTransient;

/**
 * A conversation is very similar to a chat room. It is here the employees communicates with the customer.
 * 
 * It is also possible to make it a channel for communication between employees. But that is not the main goal.
 *
 * @author Jacob
 */
@Entity
@Table(name = "CONVERSATION")
public class Conversation extends AbstractEntity {

    @NotNull(message = "{webmemo.entities.conversation.participantList.NotNull}")
    @Size(min = 1, message = "{webmemo.entities.conversation.participantList.Size}")
    @ManyToMany()
    @JoinTable(
            name = "PERSON_PARTOF_CONVERSATION",
            joinColumns = {
                @JoinColumn(name = "CONVERSATION_ID", referencedColumnName = "ID")},
            inverseJoinColumns = {
                @JoinColumn(name = "PERSON_ID", referencedColumnName = "ID")})
    private List<Person> participantList;

    @NotNull(message = "{webmemo.entities.conversation.messageList.NotNull}")
    @OneToMany(cascade = CascadeType.REMOVE)
    @OrderBy(value = "dateCreated") //TODO might have to be changed to 'dateCreated DESC' if the order is wrong
    @JoinColumn(name = "CONVERSATION_FK")
    private List<Message> messageList;

    public Conversation() {

    }
    
    public Conversation(Person person1, Person person2) {
        super(new Date());
        this.addParticipant(person1);
        this.addParticipant(person2);
    }

    public Conversation(Person person1, Person person2, Message message) {
        super(new Date());
        this.addParticipant(person1);
        this.addParticipant(person2);
        this.addMessage(message);
    }

    public boolean addParticipant(Person participant) {
        if (participantList == null) {
            participantList = new ArrayList<>();
        }
        if (participant == null || participantList.contains(participant)) {
            return false;
        }
        return participantList.add(participant);
    }

    public boolean addMessage(Message message) {
        if (messageList == null) {
            messageList = new ArrayList<>();
        }
        if(message == null){
            return false;
        }
        return messageList.add(message);
    }

    public boolean removeMessage(Message message) {
        if(messageList == null){
            return false;
        }
        return this.messageList.remove(message);
    }
    
    @XmlTransient
    public List<Person> getParticipantList() {
        return participantList;
    }

    public boolean removeParticipant(Person person) {
        if(participantList == null){
            return false;
        }
        return this.participantList.remove(person);
    }
    
    @XmlTransient
    public List<Message> getMessageList() {
        return messageList;
    }

    public void setParticipantList(List<Person> participantList) {
        this.participantList = participantList;
    }

    public void setMessageList(List<Message> messageList) {
        this.messageList = messageList;
    }
    
    
}
