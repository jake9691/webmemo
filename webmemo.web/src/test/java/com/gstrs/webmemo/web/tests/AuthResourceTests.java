/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gstrs.webmemo.web.tests;

import com.gstrs.webmemo.model.entities.Permission;
import com.gstrs.webmemo.model.entities.User;
import com.gstrs.webmemo.persistence.IUserRegistry;
import com.gstrs.webmemo.web.auth.Token;
import com.gstrs.webmemo.web.resources.AuthResource;
import javax.ejb.EJB;
import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.core.Response;
import org.junit.After;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

/**
 *
 * @author davida
 */
@RunWith(JUnit4.class)
public class AuthResourceTests {
    User u;
    
    @EJB
    private IUserRegistry repo; 
    
    @Before
    public void before() {
        u = new User("admin", Permission.ADMINISTRATOR);
        u.setPassword("admin");
        repo.create(u);
    }
    
    @After
    public void after() {
        repo.delete(u.getId());
    }
    
    
    @Test
    public void testLogin_success() {
        AuthResource resource = new AuthResource();
        
        JsonObject obj = Json.createObjectBuilder()
                .add("username", "admin")
                .add("password", "admin")
                .build();
        
        Response s = resource.login(obj);
        
        Token t = (Token)s.getEntity();
        
        assertTrue(t != null);
    }
    
}
