/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gstrs.webmemo.web.tests;

import com.gstrs.webmemo.model.entities.Permission;
import com.gstrs.webmemo.web.auth.AbstractAuthorizationProvider.Route;
import com.gstrs.webmemo.web.auth.xml.XmlAuthorizationProvider;
import java.util.Collection;
import java.util.Map;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

/**
 *
 * @author davida
 */
@RunWith(JUnit4.class)
public class XmlAuthProviderTests {
    
    
    @Test
    public void testInitializeWithXml() {
        XmlAuthorizationProvider provider = new XmlAuthorizationProvider("authorization.xml");
        
        Map<String, Collection<Route>> map = provider.getPermissionsMap();
        
        assertTrue(map.containsKey("user"));
    }
    
    @Test
    public void testQueryWithXml() {
        XmlAuthorizationProvider provider = new XmlAuthorizationProvider("authorization.xml");
        
        assertFalse(provider.query("user", "create", Permission.EMPLOYEE));
        assertTrue(provider.query("user", "create", Permission.ADMINISTRATOR));
    }
}
