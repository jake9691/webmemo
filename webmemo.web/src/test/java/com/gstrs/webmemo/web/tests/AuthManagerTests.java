/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gstrs.webmemo.web.tests;

import com.gstrs.webmemo.model.entities.Organization;
import com.gstrs.webmemo.model.entities.User;
import com.gstrs.webmemo.persistence.IUserRegistry;
import com.gstrs.webmemo.persistence.validation.ViolationWrapper;
import com.gstrs.webmemo.web.auth.AuthManager;
import com.gstrs.webmemo.web.auth.Token;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import javax.persistence.EntityTransaction;
import javax.transaction.UserTransaction;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;


/**
 *
 * @author davida
 */
@RunWith(JUnit4.class)
public class AuthManagerTests {
    AuthManager manager; 
    
    private void setRegistry() {
    manager.setUserRegistry(new IUserRegistry() {

            @Override
            public User getByUsername(String username) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public boolean validatePassword(User user, String password) {
                return "test".equals(password);
            }

            @Override
            public List<User> findUsersForOrganization(Organization org) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public User find(Long id) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public List<User> findAll() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public List<User> findRange(int first, int n) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public int count() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

        @Override
        public Set<ViolationWrapper> create(User t) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public Set<ViolationWrapper> delete(Long id) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public Set<ViolationWrapper> update(User t) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void changePassword(User user, String oldPassword, String newPassword) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void setLocaleList(List<Locale> localeList) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public User findUserForStore(Long storeId) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        });
    }
    
    @Before
    public void setUp() throws Exception {
        manager = new AuthManager(1);
        setRegistry();
    }
    
    @Test
    public void testAuthInvalidToken() {
        // just generate the token..
        Token token = manager.generateToken(new User());
        
        assertFalse(manager.isTokenValid(token.getToken()));
    }
    
    @Test
    public void testAuthValidToken() {
        Token t = manager.login(new User(), "test");
        
        assertTrue(manager.isTokenValid(t.getToken()));
    }
    
    @Test
    public void testTokenInvalidated() {
        manager = new AuthManager(-1);
        setRegistry();
        Token token = manager.login(new User(), "test");
        
        assertFalse(manager.isTokenValid(token.getToken()));
    }
    
    
}
