/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gstrs.webmemo.web.auth;

import com.gstrs.webmemo.model.entities.Permission;
import java.io.IOException;
import java.lang.reflect.Method;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

/**
 *  Rejects unauthorized requests where applicable.
 * @author davida
 */
@ApplicationScoped
public class AuthFilter implements ContainerRequestFilter {
    
    @Context
    private ResourceInfo resourceInfo;
    
    private AuthManager manager;
    
    @Inject 
    public void setAuthManager(AuthManager p) {
        manager = p;
    }
    
    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        String method = resourceInfo.getResourceMethod().getName();
        String clazz = resourceInfo.getResourceClass().getName();
        
        String token = Token.extractTokenString(requestContext.getHeaders());
        
        if (!manager.hasTokenAccess(token, clazz, method)) {
             requestContext.abortWith(Response.status(Response.Status.FORBIDDEN).build());
        }
    }
    
}
