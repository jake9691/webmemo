/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gstrs.webmemo.web.resources;

import com.gstrs.webmemo.model.entities.Customer;
import com.gstrs.webmemo.model.entities.Employee;
import com.gstrs.webmemo.model.entities.Memo;
import com.gstrs.webmemo.model.entities.Organization;
import com.gstrs.webmemo.model.entities.Permission;
import com.gstrs.webmemo.model.entities.Reminder;
import com.gstrs.webmemo.model.entities.Store;
import com.gstrs.webmemo.model.entities.User;
import com.gstrs.webmemo.persistence.IDAO;
import com.gstrs.webmemo.persistence.IWebMemo;
import com.gstrs.webmemo.persistence.validation.ViolationWrapper;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.json.JsonObject;
import javax.json.stream.JsonParsingException;
import javax.ws.rs.Path;
import javax.ws.rs.core.GenericEntity;
import static com.gstrs.webmemo.web.resources.ResourceUtils.*;
import java.util.Locale;
import javax.ws.rs.GET;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

/**
 *
 * @author davida
 */
@ApplicationScoped
@Path("/store")
public class StoreResource extends AbstractResource<Store> {

    @EJB
    private IWebMemo webMemo;

    @Override
    protected IDAO<Store, Long> getDao() {
        return webMemo.getStoreRegistry();
    }
    
     @Override
    protected Store parseJson(JsonObject json, Store store) throws JsonParsingException {
        Organization org = webMemo.getOrganizationRegistry().find(getJsonNumber(getJsonObject(json, "organization"),"id"));
        store.setOrganization(org);
        store.setName(getJsonString(json, "name"));
        store.setAddress(parseAddress(getJsonObject(json,"address"), store.getAddress()));
        store.setEmail(getJsonString(json, "email"));
        store.setExtraPhoneNumber(getJsonString(json, "extraPhoneNumber"));
        store.setMobilePhone(getJsonString(json, "mobilePhone"));
        return store;
    }

    @Override
    protected Store newEntity(JsonObject json) {
        return new Store(null,null);
    }

    @Override
    protected Set<ViolationWrapper> addDependenciesOnCreate(JsonObject json, Store store, List<Locale> localeList) {
        User user = new User();
        user.setUsername(getJsonString(json, "username"));
        user.setPassword(getJsonString(json, "password"));
        user.setPermission(Permission.EMPLOYEE);
        user.setStore(store);
        return webMemo.getUserRegistry().create(user);
        
    }
    
    @Override
    protected Set<ViolationWrapper> addDependenciesOnUpdate(JsonObject json, Store entity) {
        Set<ViolationWrapper> violations = new HashSet<>();
        User user = webMemo.getUserRegistry().findUserForStore(entity.getId());
        String newPassword = getJsonString(json, "password");
        String oldPassword =  getJsonString(json, "currentPassword");
        if (newPassword != null && webMemo.getUserRegistry().validatePassword(user, oldPassword)) {
            webMemo.getUserRegistry().changePassword(user, oldPassword, newPassword);
        } else if (newPassword != null) {
            violations.add(new ViolationWrapper("webmemo.entities.user.password.NotNull",null, Store.class, false));
        }
        if (!violations.isEmpty())
            return violations;
        
        return webMemo.getUserRegistry().update(user);
    }

    @Override
    protected GenericEntity prepareResult(Store obj) {
        return new GenericEntity<Store>(obj) {
        };
    }

    @Override
    protected GenericEntity prepareResult(List<Store> obj) {
        return new GenericEntity<List<Store>>(obj) {
        };
    }
    
    @GET
    @Path("{id}/employees")
    @Produces({"application/json"})
    public Response findRangeOfEmployeeForStore(@Context HttpHeaders headers, @PathParam("id") long id, @QueryParam("offset") int offset, @QueryParam("count") int count){
        if(count == 0){
            count = 10;
        }
        List<Employee> employees = webMemo.getEmployeeRegistry().findRangeOfEmployeeForStore(id, offset, count);
        return Response.ok(new GenericEntity<List<Employee>>(employees){}).build();
    }
    
    @GET
    @Path("{id}/customers")
    @Produces({"application/json"})
    public Response findRangeOfCustomerForStore(@Context HttpHeaders headers, @PathParam("id") long id, @QueryParam("offset") int offset, @QueryParam("count") int count){
        if(count == 0){
            count = 10;
        }
        List<Customer> customers = webMemo.getCustomerRegistry().findRangeOfCustomerForStore(id, offset, count);
        return Response.ok(new GenericEntity<List<Customer>>(customers){}).build();
    }
    
    @GET
    @Path("{id}/memos")
    @Produces({"application/json"})
    public Response findRangeOfMemoForStore(@Context HttpHeaders headers, @PathParam("id") long id, @QueryParam("offset") int offset, @QueryParam("count") int count){
        if(count == 0){
            count = 10;
        }
        List<Memo> memos = webMemo.getMemoRegistry().findRangeOfMemoForStore(id, offset, count);
        return Response.ok(new GenericEntity<List<Memo>>(memos){}).build();
    }
    
    @GET
    @Path("{id}/reminders")
    @Produces({"application/json"})
    public Response findRangeOfRemindersForStore(@Context HttpHeaders headers, @PathParam("id") long id, @QueryParam("offset") int offset, @QueryParam("count") int count){
        if(count == 0){
            count = 10;
        }
        List<Reminder> reminders = webMemo.getReminderRegistry().findRangeOfRemindersForStore(id, offset, count);
        return Response.ok(new GenericEntity<List<Reminder>>(reminders){}).build();
    }
}
