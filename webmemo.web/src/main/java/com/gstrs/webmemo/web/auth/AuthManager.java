/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gstrs.webmemo.web.auth;

import com.gstrs.webmemo.model.entities.Permission;
import com.gstrs.webmemo.model.entities.User;
import com.gstrs.webmemo.persistence.IUserRegistry;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
/**
 *
 * An authentication manager, which is responsible for storing the valid session
 * tokens in memory, and validating requests. This un-refactored implementation 
 * has the drawback that all the sessions will be lost if the web server goes down
 * or is restarted, as well as leading to some troubles when running the web
 * on a cluster. Step 1 would be to refactor the "activeTokens" field out of the 
 * class and into an injected implementation. (Could be moved to a database, KV store
 * or something similar).
 * @author davida
 */
@ApplicationScoped
public class AuthManager {
    

    /**
     * Borrowed from http://stackoverflow.com/questions/41107/how-to-generate-a-random-alpha-numeric-string
     */
    public final class SessionIdentifierGenerator {
        private SecureRandom random = new SecureRandom();

        public String nextSessionId() {
            return new BigInteger(130, random).toString(32);
        }
    }
    
    private AuthorizationProvider provider;    
    private Map<String, Token> activeTokens; 
    private IUserRegistry registry;
    
    /**
     * The amount of time in minutes until the 
     * token is invalid for this session. 
     */
    private long tokenExpiry; 
    
    public AuthManager() {
        this(30);
        activeTokens = new HashMap<>();
    }
            
    public AuthManager(long tokenExpiry) { 
        this.tokenExpiry = tokenExpiry;
        activeTokens = new HashMap<>() ;
    } 
    
    @EJB
    public void setUserRegistry(IUserRegistry memo) {
        this.registry = memo;
    }
    
    @Inject
    public void setAuthorizationProvider(AuthorizationProvider provider) {
        this.provider = provider;
    }
    
    public boolean hasAccess(String resource, String route, Permission permission) {
        return provider.query(resource, route, permission);
    }
    
    public boolean isTokenValid(String token) {
        if (token == null) return false;
        if (!activeTokens.containsKey(token)) return false;
        Date now = new Date();
        
        if (activeTokens.get(token).getExpiryDate().getTime() < now.getTime()) {
            activeTokens.remove(token);
            return false;
        } 
        
        return true;
    }
    
    
    /**
     * Authenticates a user an registers an auth token 
     */
    public Token login(User user, String plaintextPassword) {
        if (registry.validatePassword(user, plaintextPassword)) {
            Token token = generateToken(user);
            activeTokens.put(token.getToken(), token);
            return token;
        }
        return null;
    }
    
    public void invalidate(String token) {
        activeTokens.remove(token);
    }
    
    public Token generateToken(User user) {

        SessionIdentifierGenerator generator = new SessionIdentifierGenerator();

        Token token = new Token(generator.nextSessionId(), user, Date.from(Instant.now().plus(tokenExpiry, ChronoUnit.MINUTES)));
        
        return token;
    }
    
    public boolean hasTokenAccess(String token, String resource, String route) {
        boolean anonymousAccess = hasAccess(resource, route, Permission.ANONYMOUS);
        if (isTokenValid(token)) {
            Token t = activeTokens.get(token);
            
            return hasAccess(resource, route, t.getUser().getPermission()) || anonymousAccess;
        }
        
        return anonymousAccess;
    }
    
    public Token getToken(String token) {
        if (!isTokenValid(token)) return null;
        
        if (activeTokens.containsKey(token)) {
            return activeTokens.get(token);
        }
        
        return null;
    }
}
