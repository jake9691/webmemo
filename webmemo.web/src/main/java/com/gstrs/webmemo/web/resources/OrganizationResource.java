/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gstrs.webmemo.web.resources;

import com.gstrs.webmemo.model.entities.Customer;
import com.gstrs.webmemo.model.entities.Employee;
import com.gstrs.webmemo.model.entities.Organization;
import com.gstrs.webmemo.model.entities.Provider;
import com.gstrs.webmemo.model.entities.Store;
import com.gstrs.webmemo.model.entities.User;
import com.gstrs.webmemo.persistence.IDAO;
import com.gstrs.webmemo.persistence.IWebMemo;
import com.gstrs.webmemo.persistence.validation.ViolationWrapper;
import java.util.HashSet;
import com.gstrs.webmemo.web.auth.AuthManager;
import com.gstrs.webmemo.web.auth.Token;
import java.util.List;
import java.util.Set;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.json.JsonObject;
import javax.json.stream.JsonParsingException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import static com.gstrs.webmemo.web.resources.ResourceUtils.*;
import java.util.Locale;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

/**
 *
 * @author davida
 */
@ApplicationScoped
@Path("/organization")
public class OrganizationResource extends AbstractResource<Organization> {
    
    @Inject
    private AuthManager manager;
    
    @EJB
    private IWebMemo webMemo;

    @Override
    protected IDAO<Organization,Long> getDao() {
        return webMemo.getOrganizationRegistry();
    }
    
    @Override
    protected Organization newEntity(JsonObject json){
        return new Organization(null);
    }
    
    
    
    @Override
    protected Organization parseJson(JsonObject json, Organization org) throws JsonParsingException {
        org.setName(getJsonString(json, "name"));
        org.setEmail(getJsonString(json, "email"));
        org.setExtraPhoneNumber(getJsonString(json, "extraPhoneNumber"));
        org.setMobilePhone(getJsonString(json, "mobilePhone"));
        return org;
    }
    
    @Override
    protected Set<ViolationWrapper> addDependenciesOnCreate(JsonObject json, Organization org, List<Locale> localeList) {
        Set<ViolationWrapper> violations = new HashSet<>();
        User user = webMemo.getUserRegistry().find(getJsonNumber(json,"userId"));
         if(user != null){
             user.addOrganization(org);
             webMemo.getUserRegistry().setLocaleList(localeList);
             violations = webMemo.getUserRegistry().update(user);
         }else{
             violations.add(new ViolationWrapper("webmemo.resources.organization.dependencies.userId", localeList, Organization.class, false));
         }
         
         return violations;
    }
    
    @Override
    protected Set<ViolationWrapper> addDependenciesOnUpdate(JsonObject json, Organization entity) {
        return new HashSet<>();
    }

    @Override
    protected GenericEntity prepareResult(Organization obj) {
        return new GenericEntity<Organization>(obj) {};
    }

    @Override
    protected GenericEntity prepareResult(List<Organization> obj) {
        return new GenericEntity<List<Organization>>(obj) {};
    }

    @GET
    @Path("{id}/stores")
    @Produces({"application/json"})
    public Response findRangeOfStoresForOrganization(@Context HttpHeaders headers, @PathParam("id") long id, @QueryParam("offset") int offset, @QueryParam("count") int count) {
        if(count == 0){
            count = 10;
        }
               
        List<Store> stores = webMemo.getStoreRegistry().findRangeOfStoreForOrganization(id, offset, count);
        return Response.ok(new GenericEntity<List<Store>>(stores){}).build();
    }
    
    
    @GET
    @Path("user")
    @Produces({"application/json"})
    public Response findAllForUser(@Context HttpHeaders headers, @QueryParam("offset") int offset, @QueryParam("count") int count) {
        if (count == 0) {
            count = 10;
        }
        
        String t = Token.extractTokenString(headers.getRequestHeaders());
        
        Token token = manager.getToken(t);
        
        if (token == null) {
            return Response.status(401).build();
        }
        
        User u = token.getUser();
        
        List<Organization> o = webMemo.getOrganizationRegistry().findRangeOfOrganizationForUser(u, offset, count);
        
        return Response.ok(prepareResult(o)).build();
    }
    
    @GET
    @Path("{id}/employees")
    @Produces({"application/json"})
    public Response findRangeOfEmployeeForOrganization(@Context HttpHeaders headers, @PathParam("id") long id, @QueryParam("offset") int offset, @QueryParam("count") int count){
        if(count == 0){
            count = 10;
        }
        List<Employee> employees = webMemo.getEmployeeRegistry().findRangeOfEmployeeForOrganization(id, offset, count);
        return Response.ok(new GenericEntity<List<Employee>>(employees){}).build();
    }
    
    @GET
    @Path("{id}/customers")
    @Produces({"application/json"})
    public Response findRangeOfCustomerForOrganization(@Context HttpHeaders headers, @PathParam("id") long id, @QueryParam("offset") int offset, @QueryParam("count") int count){
        if(count == 0){
            count = 10;
        }
        List<Customer> customers = webMemo.getCustomerRegistry().findRangeOfCustomerForOrganization(id, offset, count);
        return Response.ok(new GenericEntity<List<Customer>>(customers){}).build();
    }
    
    @GET
    @Path("{id}/customers/count")
    @Produces({"text/plain"})
    public Response customerCountForOrganization(@PathParam("id") long id) {
        return Response.ok(webMemo.getCustomerRegistry().customerCountForOrganization(id)).build();
    }
    
    @GET
    @Path("{id}/providers")
    @Produces({"application/json"})
    public Response findRangeOfProviderForOrganization(@Context HttpHeaders headers, @PathParam("id") long id, @QueryParam("offset") int offset, @QueryParam("count") int count){
        if(count == 0){
            count = 10;
        }
        List<Provider> providers = webMemo.getProviderRegistry().findRangeOfProviderForOrganization(id, offset, count);
        return Response.ok(new GenericEntity<List<Provider>>(providers){}).build();
    }
}
