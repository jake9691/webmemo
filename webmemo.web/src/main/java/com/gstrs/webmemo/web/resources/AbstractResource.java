/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gstrs.webmemo.web.resources;

import com.gstrs.webmemo.model.interfaces.IEntity;
import com.gstrs.webmemo.persistence.IDAO;
import com.gstrs.webmemo.persistence.validation.ViolationWrapper;
import java.net.URI;
import java.util.List;
import java.util.Set;
import javax.json.JsonObject;
import javax.json.stream.JsonParsingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import static com.gstrs.webmemo.web.resources.ResourceUtils.*;
import java.util.Locale;

/**
 *
 * @author davida
 * @param <T>
 */
public abstract class AbstractResource<T extends IEntity> {

    @Context
    private UriInfo info;

    protected abstract IDAO<T, Long> getDao();

    protected abstract T parseJson(JsonObject json, T entity) throws JsonParsingException;
    
    protected abstract T newEntity(JsonObject json);

    protected abstract GenericEntity prepareResult(T obj);

    protected abstract GenericEntity prepareResult(List<T> obj);

    protected abstract Set<ViolationWrapper> addDependenciesOnCreate(JsonObject json, T entity, List<Locale> localeList);
    
    protected abstract Set<ViolationWrapper> addDependenciesOnUpdate(JsonObject json, T entity);

    @GET
    @Produces({"application/json"})
    @Path("{id}")
    public Response find(@PathParam("id") long id) {
        T result = getDao().find(id);
        if (result == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok(prepareResult(result)).build();
    }

    @GET
    @Produces({"application/json"})
    public Response findAll() {
        List<T> list = getDao().findAll();
        return Response.ok(prepareResult(list)).build();
    }

    @POST
    @Produces({"application/json"})
    @Consumes({"application/json"})
    public Response create(@Context HttpHeaders headers, JsonObject json) {
        try {
            T entity = newEntity(json);
            entity = parseJson(json, entity);
            IDAO dao = getDao();
            List<Locale> localeList = headers.getAcceptableLanguages();
            dao.setLocaleList(localeList);
            Set<ViolationWrapper> violations = dao.create(entity);
            if (violations.isEmpty()) {
                violations.addAll(addDependenciesOnCreate(getJsonObject(json, "dependencies"), entity, localeList));
            }
            
            if (violations.isEmpty()) {
                URI u = info.getAbsolutePathBuilder().path(String.valueOf(entity.getId())).build();
                return Response.created(u)
                               .entity(entity)
                               .build();
            }
            return getErrorResponse(violations);
        } catch (Exception e) {
            e.printStackTrace();
            return Response.serverError().build();
        }
    }

    protected Response getErrorResponse(Set<ViolationWrapper> violations) {
        return Response.status(Response.Status.BAD_REQUEST).entity(new GenericEntity<Set<ViolationWrapper>>(violations) {
        }).build();
    }

    @PUT
    @Produces({"application/json"})
    @Consumes({"application/json"})
    @Path("{id}")
    public Response update(@Context HttpHeaders headers, @PathParam("id") long id, JsonObject json) {
        try {
            IDAO<T, Long> dao = getDao();
            
            T entity = dao.find(id);
            
            if (entity == null) {
                return create(headers, json);
            }
            
            entity = parseJson(json, entity);

            URI u = info.getRequestUriBuilder().build(entity);
            
            dao.setLocaleList(headers.getAcceptableLanguages());

            Set<ViolationWrapper> violations = dao.update(entity);
            
            if (violations.isEmpty()) {
                violations.addAll(addDependenciesOnUpdate(getJsonObject(json, "dependencies"), entity));
            }

            if (violations.isEmpty()) {
                return Response.ok(entity).build();
            }
            return getErrorResponse(violations);

        } catch (Exception e) {
            e.printStackTrace();
            return Response.serverError().build();
        }
    }

    @DELETE
    @Produces({"application/json"})
    @Consumes({"application/json"})
    @Path("{id}")
    public Response delete(@Context HttpHeaders headers, @PathParam("id") long id) {
        IDAO dao = getDao();
        dao.setLocaleList(headers.getAcceptableLanguages());
        Set<ViolationWrapper> violations = getDao().delete(id);
        if (violations.isEmpty()) {
            return Response.noContent().build();
        }
        return getErrorResponse(violations);
    }
    
    @GET
    @Path("count")
    @Produces({"text/plain"})
    public Response count() {
        return Response.ok(getDao().count()).build();
    }
}
