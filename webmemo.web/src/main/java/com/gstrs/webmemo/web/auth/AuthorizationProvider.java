/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gstrs.webmemo.web.auth;

import com.gstrs.webmemo.model.entities.Permission;
import javax.inject.Named;

/**
 *
 * @author davida
 */
@Named
public interface AuthorizationProvider {

    boolean query(String resourceName, String routeName, Permission perm);
    
}
