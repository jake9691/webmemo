/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gstrs.webmemo.web.resources;

import com.gstrs.webmemo.model.entities.Organization;
import com.gstrs.webmemo.model.entities.Provider;
import com.gstrs.webmemo.persistence.IDAO;
import com.gstrs.webmemo.persistence.IWebMemo;
import com.gstrs.webmemo.persistence.validation.ViolationWrapper;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.json.JsonObject;
import javax.json.stream.JsonParsingException;
import javax.ws.rs.Path;
import javax.ws.rs.core.GenericEntity;
import static com.gstrs.webmemo.web.resources.ResourceUtils.*;
import java.util.Locale;

/**
 *
 * @author davida
 */
@ApplicationScoped
@Path("/provider")
public class ProviderResource extends AbstractResource<Provider> {

    @EJB
    private IWebMemo webMemo;

    @Override
    protected IDAO<Provider, Long> getDao() {
        return webMemo.getProviderRegistry();
    }
    
        @Override
    protected Provider parseJson(JsonObject json, Provider provider) throws JsonParsingException {
        Organization org = webMemo.getOrganizationRegistry().find(getJsonNumber(getJsonObject(json,"organization"), "id"));
        provider.setOrganization(org);
        provider.setName(getJsonString(json, "name"));
        provider.setProviderAbbreviationId(getJsonString(json, "providerAbbreviationId"));
        provider.setAddress(ResourceUtils.parseAddress(getJsonObject(json,"address"), provider.getAddress()));
        provider.setEmail(getJsonString(json, "email"));
        provider.setExtraPhoneNumber(getJsonString(json, "extraPhoneNumber"));
        provider.setMobilePhone(getJsonString(json, "mobilePhone"));
        return provider;
    }

    @Override
    protected Provider newEntity(JsonObject json) {
        //Dont need to set anything since that is done at the parsing
        return new Provider(null, null, null);
    }

    @Override
    protected Set<ViolationWrapper> addDependenciesOnCreate(JsonObject json, Provider provider, List<Locale> localeList) {
        return new HashSet<>();
    }
    
    @Override
    protected Set<ViolationWrapper> addDependenciesOnUpdate(JsonObject json, Provider entity) {
        return new HashSet<>();
    }

    @Override
    protected GenericEntity prepareResult(Provider obj) {
        return new GenericEntity<Provider>(obj) {
        };
    }

    @Override
    protected GenericEntity prepareResult(List<Provider> obj) {
        return new GenericEntity<List<Provider>>(obj) {
        };
    }
}
