/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gstrs.webmemo.web.resources;

import com.gstrs.webmemo.model.entities.Conversation;
import com.gstrs.webmemo.model.entities.Message;
import com.gstrs.webmemo.model.entities.Person;
import com.gstrs.webmemo.persistence.IDAO;
import com.gstrs.webmemo.persistence.IWebMemo;
import com.gstrs.webmemo.persistence.validation.ViolationWrapper;
import static com.gstrs.webmemo.web.resources.ResourceUtils.*;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.json.JsonObject;
import javax.json.stream.JsonParsingException;
import javax.ws.rs.Path;
import javax.ws.rs.core.GenericEntity;

/**
 *
 * @author davida
 */
@ApplicationScoped
@Path("/message")
public class MessageResource extends AbstractResource<Message> {

    @EJB
    private IWebMemo webMemo;

    @Override
    protected IDAO<Message, Long> getDao() {
        return webMemo.getMessageRegistry();
    }
    
    @Override
    protected Message parseJson(JsonObject json, Message message) throws JsonParsingException {
        message.setMessage(getJsonString(json, "message"));
        Person person = webMemo.getEmployeeRegistry().findPerson(getJsonNumber(getJsonObject(json, "sender"), "id"));
        message.setSender(person);
        return message;
    }

    @Override
    protected Message newEntity(JsonObject json) {
        return new Message(null, null);
    }

    @Override
    protected Set<ViolationWrapper> addDependenciesOnCreate(JsonObject json, Message message, List<Locale> localeList) {
        Set<ViolationWrapper> violations = new HashSet<>();
        Conversation conversation = webMemo.getConversationRegistry().find(getJsonNumber(json, "conversationId"));
        if(conversation != null){
            conversation.addMessage(message);
            webMemo.getConversationRegistry().setLocaleList(localeList);
            violations = webMemo.getConversationRegistry().update(conversation);
        }else{
            violations.add(new ViolationWrapper("webmemo.resources.message.dependencies.conversationId", localeList, Message.class, false));
        }
        return violations;
    }

    @Override
    protected Set<ViolationWrapper> addDependenciesOnUpdate(JsonObject json, Message message) {
        return new HashSet<>();
    }

    @Override
    protected GenericEntity prepareResult(Message obj) {
        return new GenericEntity<Message>(obj) {
        };
    }

    @Override
    protected GenericEntity prepareResult(List<Message> obj) {
        return new GenericEntity<List<Message>>(obj) {
        };
    }
}
