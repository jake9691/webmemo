/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gstrs.webmemo.web.resources;

import com.gstrs.webmemo.model.entities.Conversation;
import com.gstrs.webmemo.model.entities.Customer;
import com.gstrs.webmemo.model.entities.Memo;
import com.gstrs.webmemo.model.entities.Organization;
import com.gstrs.webmemo.model.entities.Store;
import com.gstrs.webmemo.persistence.IDAO;
import com.gstrs.webmemo.persistence.IWebMemo;
import com.gstrs.webmemo.persistence.validation.ViolationWrapper;
import static com.gstrs.webmemo.web.resources.ResourceUtils.*;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.json.JsonObject;
import javax.json.stream.JsonParsingException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

/**
 *
 * @author davida
 */
@ApplicationScoped
@Path("/customer")
public class CustomerResource extends AbstractResource<Customer> {

    @EJB
    private IWebMemo webMemo;

    @Override
    protected IDAO<Customer, Long> getDao() {
        return webMemo.getCustomerRegistry();
    }

    @Override
    protected Customer parseJson(JsonObject json, Customer customer) throws JsonParsingException {
        customer.setName(getJsonString(json, "name"));
        customer.setLastName(getJsonString(json, "lastName"));
        customer.setMobilePhone(getJsonString(json, "mobilePhone"));

        Store store = webMemo.getStoreRegistry().find(getJsonNumber(getJsonObject(json, "customerCreatedInStore"), "id"));
        customer.setCustomerCreatedInStore(store);

        customer.setAddress(ResourceUtils.parseAddress(getJsonObject(json, "address"), customer.getAddress()));
        customer.setAssociation(getJsonString(json, "association"));
        customer.setEmail(getJsonString(json, "email"));
        customer.setExtraPhoneNumber(getJsonString(json, "extraPhoneNumber"));
        return customer;
    }

    @Override
    protected Customer newEntity(JsonObject json) {
        return new Customer(null, null, null, null);
    }

    @Override
    protected Set<ViolationWrapper> addDependenciesOnCreate(JsonObject json, Customer customer, List<Locale> localeList) {
        Set<ViolationWrapper> violations = new HashSet<>();
        Organization org = webMemo.getOrganizationRegistry().find(getJsonNumber(json, "organizationId"));
        if(org != null){
            org.addCustomer(customer);
            webMemo.getOrganizationRegistry().setLocaleList(localeList);
            violations = webMemo.getOrganizationRegistry().update(org);
        }else{
            violations.add(new ViolationWrapper("webmemo.resources.customer.dependencies.organizationId", localeList, Customer.class, false));
        }
        return violations;
    }

    @Override
    protected Set<ViolationWrapper> addDependenciesOnUpdate(JsonObject json, Customer entity) {
        return new HashSet<>();
    }

    @Override
    protected GenericEntity prepareResult(Customer obj) {
        return new GenericEntity<Customer>(obj) {
        };
    }

    @Override
    protected GenericEntity prepareResult(List<Customer> obj) {
        return new GenericEntity<List<Customer>>(obj) {
        };
    }

    @GET
    @Path("{id}/conversation")
    @Produces({"application/json"})
    public Response findConversationForPerson(@Context HttpHeaders headers, @PathParam("id") long id) {
        List<Conversation> conversations = webMemo.getConversationRegistry().findRangeOfConversationsForPerson(id, 0, 1);
        return Response.ok(new GenericEntity<Conversation>(conversations.get(0)) {
        }).build();
    }
    
    @GET
    @Path("{id}/memos")
    @Produces({"application/json"})
    public Response findRangeOfMemoWithCustomer(@Context HttpHeaders headers, @PathParam("id") long id, @QueryParam("offset") int offset, @QueryParam("count") int count) {
        if(count == 0){
            count = 10;
        }
        List<Memo> memos = webMemo.getMemoRegistry().findRangeOfMemoWithCustomer(id, offset, count);
        return Response.ok(new GenericEntity<List<Memo>>(memos){}).build();
    }

    @GET
    @Path("search")
    @Produces({"application/json"})
    public Response searchRangeOfCustomer(@Context HttpHeaders headers, @QueryParam("words") String words,
        @QueryParam("offset") int offset,           @QueryParam("count") int count) {
        List<Customer> customerList = webMemo.getCustomerRegistry().searchForCustomer(words, null, offset, count);
        if(customerList != null){
            return Response.ok(prepareResult(customerList)).build();
        }else{
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }
}
