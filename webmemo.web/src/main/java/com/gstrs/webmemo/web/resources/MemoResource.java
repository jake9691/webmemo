/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gstrs.webmemo.web.resources;

import com.gstrs.webmemo.model.entities.Customer;
import com.gstrs.webmemo.model.entities.Employee;
import com.gstrs.webmemo.model.entities.Item;
import com.gstrs.webmemo.model.entities.Memo;
import com.gstrs.webmemo.model.entities.Order;
import com.gstrs.webmemo.model.entities.Provider;
import com.gstrs.webmemo.model.entities.Reception;
import com.gstrs.webmemo.model.entities.Store;
import com.gstrs.webmemo.persistence.IDAO;
import com.gstrs.webmemo.persistence.IWebMemo;
import com.gstrs.webmemo.persistence.validation.ViolationWrapper;
import static com.gstrs.webmemo.web.resources.ResourceUtils.*;
import java.text.ParseException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.json.JsonObject;
import javax.json.stream.JsonParsingException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

/**
 *
 * @author davida
 */
@ApplicationScoped
@Path("/memo")
public class MemoResource extends AbstractResource<Memo> {

    @EJB
    private IWebMemo webMemo;

    @Override
    protected IDAO<Memo, Long> getDao() {
        return webMemo.getMemoRegistry();
    }
    
    /**
     * You have to create the customer, order and reception infront if you're creating a new memo!
     * Changed made to the customer that is posted with the memo will not be saved. YOu have to update them serperatly!
     * 
     * An order or a reception will never be added on create!!
     *
     * @param json
     * @param memo
     * @return
     * @throws JsonParsingException
     */
    @Override
    protected Memo parseJson(JsonObject json, Memo memo) throws JsonParsingException {
        Employee creator = webMemo.getEmployeeRegistry().find(getJsonNumber(getJsonObject(json,"creator"),"id"));
        memo.setCreator(creator);
        
        Employee responsible = webMemo.getEmployeeRegistry().find(getJsonNumber(getJsonObject(json,"responsible"), "id"));
        memo.setResponsible(responsible);

        Customer customer = webMemo.getCustomerRegistry().find(getJsonNumber(getJsonObject(json,"customer"), "id"));
        memo.setCustomer(customer);

        memo.setFreeText(getJsonString(json, "freeText"));

        JsonObject jsonItem = getJsonObject(json,"item");
        Item item = memo.getItem();
        item.setAgreedPrice(getJsonString(jsonItem, "agreedPrice"));
        item.setColor(getJsonString(jsonItem, "color"));
        item.setItemNumber(getJsonString(jsonItem, "itemNumber"));
        item.setName(getJsonString(jsonItem, "name"));
        item.setProviderItemNumber(getJsonString(jsonItem, "providerItemNumber"));
        item.setQuantity(getJsonString(jsonItem, "quantity"));
        item.setSize(getJsonString(jsonItem, "size"));
        Provider provider = webMemo.getProviderRegistry().find(getJsonNumber(getJsonObject(jsonItem, "provider"), "id"));
        item.setProvider(provider);
        memo.setItem(item);
        
        JsonObject jsonOrder = getJsonObject(json, "order");
        Order order = webMemo.getOrderRegistry().find(getJsonNumber(jsonOrder, "id"));
        if(order != null){
            Employee orderCreator = webMemo.getEmployeeRegistry().find(getJsonNumber(getJsonObject(jsonOrder, "creator"), "id"));
            order = parseOrder(jsonOrder, order, orderCreator);
        }
        memo.setOrder(order);
        
        JsonObject jsonReception = getJsonObject(json, "reception");
        Reception reception = webMemo.getReceptionRegistry().find(getJsonNumber(jsonReception, "id"));
        if(reception != null){
            Employee receptionCreator = webMemo.getEmployeeRegistry().find(getJsonNumber(getJsonObject(jsonReception, "creator"), "id"));
            reception = parseReception(jsonReception, reception, receptionCreator);
        }
        memo.setReception(reception);

        memo.setReclaim(getJsonBoolean(json,"isReclaim"));
        memo.setReclaimId(getJsonString(json, "reclaimId"));
        try{
            memo.setRemindDate(parseStringToDate(getJsonString(json, "remindDate")));
        }catch(ParseException e){
            e.printStackTrace();
        }
        return memo;
    }

    @Override
    protected Memo newEntity(JsonObject json) {
        return new Memo(null);
    }

    @Override
    protected Set<ViolationWrapper> addDependenciesOnCreate(JsonObject json, Memo memo, List<Locale> localeList) {
        Set<ViolationWrapper> violations = new HashSet<>();
        Store store = webMemo.getStoreRegistry().find(getJsonNumber(json, "storeId"));
        if(store != null){
            store.addMemo(memo);
            webMemo.getStoreRegistry().setLocaleList(localeList);
            violations = webMemo.getStoreRegistry().update(store);
            
        }else{
            violations.add(new ViolationWrapper("webmemo.resources.memo.dependencies.storeId", localeList, Memo.class, false));//TODO! language
        }
         
        
        //add to store!
//        if (memo.getRemindDate() != null) {
//            //TODO Fix language for Reminder
//            Reminder reminder = new Reminder(memo.getResponsible(), "You asked for a reminder for this memo", memo, memo.getRemindDate(), Reminder.Type.EMPLOYEE_CONFIRM);
//            violations.addAll(webMemo.getReminderRegistry().create(reminder));
//        }
//        if (memo.getCreator() != null && !memo.getCreator().equals(memo.getResponsible())) {
//            Reminder reminder2 = new Reminder(memo.getResponsible(), memo.getCreator().getName() + " has added a memo with you as responsible", memo, new Date(), Reminder.Type.EMPLOYEE_CONFIRM);
//            violations.addAll(webMemo.getReminderRegistry().create(reminder2));
//        }
//        violations.addAll(addOrderAndReceptionReminders(memo));
        return violations;
    }

    private Set<ViolationWrapper> addOrderAndReceptionReminders(Memo memo) {
        Set<ViolationWrapper> violations = new HashSet<>();
//        Order order = memo.getOrder();
//        if (order != null && order.getModified() == null && memo.getCreator() != null && !memo.getCreator().equals(order.getCreator())) {
//            Reminder reminder3 = new Reminder(memo.getResponsible(), order.getCreator().getName() + " has ordered one of your memos", memo, new Date(), Reminder.Type.EMPLOYEE_CONFIRM);
//            violations.addAll(webMemo.getReminderRegistry().create(reminder3));
//        }
//        Reception reception = memo.getReception();
//        if (reception != null && reception.getModified() == null && memo.getCreator() != null && !memo.getCreator().equals(reception.getCreator())) {
//            if (reception.isIsCostumerNotified()) {
//                Reminder reminder4 = new Reminder(memo.getResponsible(), reception.getCreator().getName() + " has received one of your memos, but the customer is not notified", memo, new Date(), Reminder.Type.CUSTOMER_NOTIFIED);
//                violations.addAll(webMemo.getReminderRegistry().create(reminder4));
//            } else {
//                Reminder reminder5 = new Reminder(memo.getResponsible(), reception.getCreator().getName() + " has received one of your memos and also notified the customer", memo, new Date(), Reminder.Type.EMPLOYEE_CONFIRM);
//                violations.addAll(webMemo.getReminderRegistry().create(reminder5));
//            }
//        }
        return violations;
    }

    @Override
    protected Set<ViolationWrapper> addDependenciesOnUpdate(JsonObject json, Memo memo) {
        Set<ViolationWrapper> violations = new HashSet<>();
//        violations.addAll(addOrderAndReceptionReminders(memo));
        return violations;
    }

    @Override
    protected GenericEntity prepareResult(Memo obj) {
        return new GenericEntity<Memo>(obj) {
        };
    }

    @Override
    protected GenericEntity prepareResult(List<Memo> obj) {
        return new GenericEntity<List<Memo>>(obj) {
        };
    }
    
    @GET
    @Path("search")
    @Produces({"application/json"})
    public Response searchRangeOfMemos(@Context HttpHeaders headers, @QueryParam("words") String words,
            @QueryParam("from") String from,          @QueryParam("responsibleId") long responsibleId,
            @QueryParam("creatorId") long creatorId,    @QueryParam("providerId") long providerId, 
            @QueryParam("status") String status,        @QueryParam("isReclaim") String isreclaim,
            @QueryParam("offset") int offset,           @QueryParam("count") int count) {
        //memo/search?words=jacob&from=2014-10-13&responsibleId=4&creatorId=5&providerId=2&status=created&isreclaim=true&offset=0&count=10
        if(count == 0){
            count = 10;
        }
        Date since = null;
        Memo.Status statusEnum = null;
        if(status != null)
            try{
                statusEnum = Memo.Status.valueOf(status.toUpperCase());
            }catch(IllegalArgumentException e){
                statusEnum = null;
        }
        Boolean isReclaimBool = null;
        if(isreclaim != null && isreclaim.toLowerCase().equals("true")){
            isReclaimBool = true;
        }else if(isreclaim != null && isreclaim.toLowerCase().equals("false")){
            isReclaimBool = false;
        }
        
        if(from != null){
            switch (from) {
                case "d1":
                    since = Date.from(Instant.now().minus(1, ChronoUnit.DAYS));
                    break;
                case "d7":
                    since = Date.from(Instant.now().minus(7, ChronoUnit.DAYS));
                    break;
                case "d30":
                    since = Date.from(Instant.now().minus(30, ChronoUnit.DAYS));
                    break;
                default:
                    try {
                        since = parseStringToDate(from);
                    } catch (ParseException e) {
                        Set<ViolationWrapper> viol = new HashSet<>();
                        viol.add(new ViolationWrapper("webmemo.resources.memo.search.parseDate", headers.getAcceptableLanguages(), Memo.class, false));
                        return getErrorResponse(viol);
                    }   break;
            }
        }
        
        
        List<Memo> memos = webMemo.getMemoRegistry().searchRangeOfMemos(words, since, creatorId, responsibleId, providerId, statusEnum, isReclaimBool , offset, count);
        if(memos != null){
            return Response.ok(new GenericEntity<List<Memo>>(memos){}).build();
        }else{
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }
}
