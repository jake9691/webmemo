/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gstrs.webmemo.web.config;

import com.gstrs.webmemo.web.auth.AuthFilter;
import com.gstrs.webmemo.web.filters.CORSFilter;
import com.gstrs.webmemo.web.filters.LoggingResponseFilter;
import com.gstrs.webmemo.web.resources.*;
import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 * @author davida
 */
@ApplicationPath("/api")
public class ApplicationConfig extends Application {
    
    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new HashSet<>();
        
        resources.add(CORSFilter.class);
        resources.add(AuthFilter.class);
        resources.add(AuthResource.class);
        resources.add(UserResource.class);
        resources.add(LoggingResponseFilter.class);
        
        resources.add(OrganizationResource.class);
        resources.add(StoreResource.class);
        resources.add(ProviderResource.class);
        resources.add(CustomerResource.class);
        resources.add(EmployeeResource.class);
        resources.add(MemoResource.class);
        resources.add(OrderResource.class);
        resources.add(ReceptionResource.class);
        resources.add(ConversationResource.class);
        resources.add(MessageResource.class);
        return resources;
    }
}
