    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gstrs.webmemo.web.resources;

import com.gstrs.webmemo.model.entities.Conversation;
import com.gstrs.webmemo.model.entities.Employee;
import com.gstrs.webmemo.model.entities.Memo;
import com.gstrs.webmemo.model.entities.Organization;
import com.gstrs.webmemo.model.entities.Reminder;
import com.gstrs.webmemo.model.entities.Store;
import com.gstrs.webmemo.persistence.IDAO;
import com.gstrs.webmemo.persistence.IWebMemo;
import com.gstrs.webmemo.persistence.validation.ViolationWrapper;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.json.JsonObject;
import javax.json.stream.JsonParsingException;
import javax.ws.rs.Path;
import javax.ws.rs.core.GenericEntity;
import static com.gstrs.webmemo.web.resources.ResourceUtils.*;
import java.util.Locale;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

/**
 *
 * @author davida
 */
@ApplicationScoped
@Path("/employee")
public class EmployeeResource extends AbstractResource<Employee> {

    @EJB
    private IWebMemo webMemo;

    @Override
    protected IDAO<Employee, Long> getDao() {
        return webMemo.getEmployeeRegistry();
    }
    
    @Override
    protected Employee parseJson(JsonObject json, Employee employee) throws JsonParsingException {
        employee.setName(getJsonString(json, "name"));
        employee.setLastName(getJsonString(json, "lastName"));
        employee.setAbbreviation(getJsonString(json, "abbreviation"));
        employee.setPinCode(getJsonString(json, "pinCode"));
        
        Store store = webMemo.getStoreRegistry().find(getJsonNumber(getJsonObject(json,"workingInStore"),"id"));
        employee.setWorkingInStore(store);

        employee.setMobilePhone(getJsonString(json, "mobilePhone"));
        employee.setIsResponsible(getJsonBoolean(json,"isResponsible"));
        employee.setEmail(getJsonString(json, "email"));
        employee.setExtraPhoneNumber(getJsonString(json, "extraPhoneNumber"));
        return employee;
    }

    @Override
    protected Employee newEntity(JsonObject json) {
        return new Employee(null, null, null, null, null);
    }
    
    @Override
    protected Set<ViolationWrapper> addDependenciesOnCreate(JsonObject json, Employee employee, List<Locale> localeList) {
        Set<ViolationWrapper> violations = new HashSet<>();
        Organization org = webMemo.getOrganizationRegistry().find(getJsonNumber(json, "organizationId"));
        if(org != null){
            org.addEmployee(employee);
            webMemo.getOrganizationRegistry().setLocaleList(localeList);
            violations = webMemo.getOrganizationRegistry().update(org);
        }else{
            violations.add(new ViolationWrapper("webmemo.resources.employee.dependencies.organizationId", localeList, Employee.class, false));
        }
        return violations;
    }

    @Override
    protected Set<ViolationWrapper> addDependenciesOnUpdate(JsonObject json, Employee entity) {
        return new HashSet<>();
    }

    @Override
    protected GenericEntity prepareResult(Employee obj) {
        return new GenericEntity<Employee>(obj) {
        };
    }

    @Override
    protected GenericEntity prepareResult(List<Employee> obj) {
        return new GenericEntity<List<Employee>>(obj) {};
    }
    
    @GET
    @Path("{id}/conversations")
    @Produces({"application/json"})
    public Response findRangeOfConversationsForPerson(@Context HttpHeaders headers, @PathParam("id") long id, @QueryParam("offset") int offset, @QueryParam("count") int count){
        if(count == 0){
            count = 10;
        }
        List<Conversation> conversations = webMemo.getConversationRegistry().findRangeOfConversationsForPerson(id, offset, count);
        return Response.ok(new GenericEntity<List<Conversation>>(conversations){}).build();
    }
    
    @GET
    @Path("{id}/memos")
    @Produces({"application/json"})
    public Response findRangeOfMemos(@Context HttpHeaders headers, @PathParam("id") long id, @QueryParam("type") String type, @QueryParam("offset") int offset, @QueryParam("count") int count){
        if(count == 0){
            count = 10;
        }
        List<Memo> memos = null;
        if(type == null || type.equals("all")){
            memos = webMemo.getMemoRegistry().findRangeOfMemoWithCreator(id, offset, count);
        }else if(type.equals("creator")){
            memos = webMemo.getMemoRegistry().findRangeOfMemos(id, offset, count);
        }else if(type.equals("responsible")){
            memos = webMemo.getMemoRegistry().findRangeOfMemoWithResponsible(id, offset, count);
        }
        return Response.ok(new GenericEntity<List<Memo>>(memos){}).build();
    }
    
    @GET
    @Path("{id}/reminders")
    @Produces({"application/json"})
    public Response findRangeOfRemindersForEmployee(@Context HttpHeaders headers, @PathParam("id") long id, @QueryParam("offset") int offset, @QueryParam("count") int count){
        if(count == 0){
            count = 10;
        }
        List<Reminder> reminders = webMemo.getReminderRegistry().findRangeOfRemindersForEmployee(id, offset, count);
        return Response.ok(new GenericEntity<List<Reminder>>(reminders){}).build();
    }
    
    @POST
    @Path("/verify")
    @Produces({"application/json"})
    @Consumes({"application/json"})
    public Response findEmployeeWithStoreAndPinCode(@Context HttpHeaders headers, JsonObject json){
        try {
            Employee employee = webMemo.getEmployeeRegistry().findEmployeeWithStoreAndPinCode(getJsonNumber(json, "storeId"), getJsonString(json, "pinCode"));
            if(employee != null){
                return Response.ok(prepareResult(employee)).build();
            }else{
                return Response.status(Response.Status.NOT_FOUND).build();
            }
        } catch (Exception e) {
            // no time!
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }
}
