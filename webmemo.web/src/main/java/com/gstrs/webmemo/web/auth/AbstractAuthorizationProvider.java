/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gstrs.webmemo.web.auth;

import com.gstrs.webmemo.model.entities.Permission;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author davida
 */

public abstract class AbstractAuthorizationProvider implements AuthorizationProvider {
    
    public class Route {
        String name;
        Collection<Permission> permissions;
        
        public Route(String name) {
            this.name = name;
            permissions = new ArrayList<>();
        }
        
        public Route permission(Permission p) {
            permissions.add(p);
            return this;
        }
        
        public Collection<Permission> getPermissions() {
            return permissions;
        }      
    }
    
    public abstract Map<String, Collection<Route>> getPermissionsMap(); 
    
    /**
     * Queries the permission map. Returns true if either: 
     *  1. the specified permission has access
     *  2. the supplied resource name isn't found.
     * otherwise false.
     */
    @Override
    public boolean query(String resourceName, String routeName, Permission perm) {
        // normalize the resource and route names
        resourceName = resourceName.toLowerCase();
        routeName = routeName.toLowerCase(); 
        
        Map<String, Collection<Route>> permissionMap = getPermissionsMap();
        
        if (!permissionMap.containsKey(resourceName)) {
            Logger.getLogger(getClass().toString()).log(Level.WARNING, "No permissions specified for route {0} in resource {1}. Returning true!", new Object[]{routeName, resourceName});
            return true;
        }
        
        Collection<Route> routes = permissionMap.get(resourceName);
        
        // todo: this can be made O(1) with a bit of initialization.
        for(Route r : routes) {
            if (r.name.equals(routeName)) {
                if (r.permissions.contains(perm)) {
                    return true;
                }
            }
        }
        
        return false;
    }
    
}
