/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gstrs.webmemo.web.auth.xml;

import com.gstrs.webmemo.model.entities.Permission;
import com.gstrs.webmemo.web.auth.AbstractAuthorizationProvider;
import com.gstrs.webmemo.web.auth.AuthorizationProvider;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.ApplicationScoped;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author davida
 */
@ApplicationScoped
public class XmlAuthorizationProvider extends AbstractAuthorizationProvider implements AuthorizationProvider {

    private String path;
    
    public XmlAuthorizationProvider() {
        this("authorization.xml");
    }
    
    public XmlAuthorizationProvider(String path) {
        this.path = path;
    }
    
    private Map<String, Collection<Route>> permissionMap;

    @Override
    public Map<String, Collection<Route>> getPermissionsMap() {
        if (permissionMap == null) {
            permissionMap = new HashMap<>();

            try {
                JAXBContext jaxbContext = JAXBContext.newInstance(ObjectFactory.class);

                Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
                JAXBElement<Authorization> authorizationWrapper = 
                        (JAXBElement<Authorization>)unmarshaller.unmarshal(
                                getClass().getClassLoader().getResourceAsStream(path));
                
                Authorization authorization = authorizationWrapper.getValue();
                
                for (Resource resource : authorization.getResource()) {
                    permissionMap.put(resource.getName(), new ArrayList<AbstractAuthorizationProvider.Route>());
                    
                    for (com.gstrs.webmemo.web.auth.xml.Route r : resource.getRoute()) {
                        AbstractAuthorizationProvider.Route route = new AbstractAuthorizationProvider.Route(r.getName());

                        for (String perm : r.getContent().split(",")) {
                            route.permission(Permission.valueOf(perm.trim()));
                        }

                        permissionMap.get(resource.getName()).add(route);
                    }
                }
            } catch (JAXBException ex) {
                Logger.getLogger(XmlAuthorizationProvider.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalArgumentException ex) {
                
                Logger.getLogger(XmlAuthorizationProvider.class.getName()).log(Level.SEVERE, "Unable to parse the permission levels!", ex);
            }     
        }

        return permissionMap;
    }
}
