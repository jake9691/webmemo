/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gstrs.webmemo.web.resources;

import com.gstrs.webmemo.model.entities.Address;
import com.gstrs.webmemo.model.entities.Employee;
import com.gstrs.webmemo.model.entities.Order;
import com.gstrs.webmemo.model.entities.Reception;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.json.JsonObject;

/**
 *
 * @author Jacob
 */
public class ResourceUtils {

    protected static Address parseAddress(JsonObject json, Address address) {
        String addressString = getJsonString(json, "address");
        String postalCode = getJsonString(json, "postalCode");
        String city = getJsonString(json, "city");
        if(address == null && (addressString != null || postalCode != null || city != null)){    
            address = new Address(null);
        }else{
            return null;
        }
        address.setAddress(addressString);
        address.setPostalCode(postalCode);
        address.setCity(city);
        return address;
    }
    protected static Order parseOrder(JsonObject json, Order order, Employee creator) {
        if(order == null){
            return null;
        }
        order.setCreator(creator);
        order.setOrderId(getJsonString(json, "orderId"));
        order.setProviderId(getJsonString(json, "providerId"));
        order.setRemarks(getJsonString(json, "remarks"));
        return order;
    }
    
    protected static Reception parseReception(JsonObject json, Reception reception, Employee creator) {
        if(reception == null){
            return null;
        }
        reception.setCreator(creator);
        reception.setIsCostumerNotified(getJsonBoolean(json, "isCostumerNotified"));
        reception.setStorageLocation(getJsonString(json, "storageLocation"));
        reception.setRemarks(getJsonString(json, "remarks"));
        return reception;
    }
    
    protected static String getJsonString(JsonObject json, String name) {
        if (json != null && json.containsKey(name) && !json.isNull(name)) {
            return json.getString(name);
        } else {
            return null;
        }
    }
    
    protected static Boolean getJsonBoolean(JsonObject json, String name) {
        if (json != null && json.containsKey(name) && !json.isNull(name)) {
            return json.getBoolean(name);
        } else {
            return false;
        }
    }

    protected static JsonObject getJsonObject(JsonObject json, String name) {
        if (json != null && json.containsKey(name) && !json.isNull(name)) {
            return json.getJsonObject(name);
        } else {
            return null;
        }
    }

    protected static Long getJsonNumber(JsonObject json, String name) {
        if (json != null && json.containsKey(name) && !json.isNull(name)) {
            return json.getJsonNumber(name).longValue();
        } else {
            return null;
        }
    }
    protected static Date parseStringToDate(String date) throws ParseException{
        if (date == null) {
            return null;
        } else {
            //"2014-10-20T17:06:48.172+02:00"
            Date realDate = null;
            try{
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
                realDate = formatter.parse(date);
                
            }catch(ParseException e){
                return parseStringToSimpleDate(date);
            }
            return realDate;
        }
    }
    
    private static Date parseStringToSimpleDate(String date) throws ParseException{
        if (date == null) {
            return null;
        } else {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                Date realDate = formatter.parse(date);
                return realDate;
        }
    }
}
