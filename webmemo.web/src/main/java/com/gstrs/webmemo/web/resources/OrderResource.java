/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gstrs.webmemo.web.resources;

import com.gstrs.webmemo.model.entities.Employee;
import com.gstrs.webmemo.model.entities.Order;
import com.gstrs.webmemo.persistence.IDAO;
import com.gstrs.webmemo.persistence.IWebMemo;
import com.gstrs.webmemo.persistence.validation.ViolationWrapper;
import static com.gstrs.webmemo.web.resources.ResourceUtils.*;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.json.JsonObject;
import javax.json.stream.JsonParsingException;
import javax.ws.rs.Path;
import javax.ws.rs.core.GenericEntity;

/**
 *
 * @author davida
 */
@ApplicationScoped
@Path("/order")
public class OrderResource extends AbstractResource<Order> {

    @EJB
    private IWebMemo webMemo;

    @Override
    protected IDAO<Order, Long> getDao() {
        return webMemo.getOrderRegistry();
    }
    
    @Override
    protected Order parseJson(JsonObject json, Order order) throws JsonParsingException {
        Employee orderCreator = webMemo.getEmployeeRegistry().find(getJsonNumber(getJsonObject(json, "creator"), "id"));
        order = parseOrder(json, order, orderCreator);
        return order;
    }

    @Override
    protected Order newEntity(JsonObject json) {
        return new Order(null);
    }

    @Override
    protected Set<ViolationWrapper> addDependenciesOnCreate(JsonObject json, Order order, List<Locale> localeList) {
        return new HashSet<>();
    }

    @Override
    protected Set<ViolationWrapper> addDependenciesOnUpdate(JsonObject json, Order entity) {
        return new HashSet<>();
    }

    @Override
    protected GenericEntity prepareResult(Order obj) {
        return new GenericEntity<Order>(obj) {
        };
    }

    @Override
    protected GenericEntity prepareResult(List<Order> obj) {
        return new GenericEntity<List<Order>>(obj) {
        };
    }
}
