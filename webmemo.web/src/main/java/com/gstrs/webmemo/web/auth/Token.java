/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gstrs.webmemo.web.auth;

import com.gstrs.webmemo.model.entities.User;
import java.io.Serializable;
import java.util.Date;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * A rudimentary implementation of an OAuth 2 token.
 * @author davida
 */
@XmlRootElement
public class Token implements Serializable {
    
    @XmlElement(name = "access_token")
    private String token; 
    
    @XmlElement(name = "token_type")
    private static final String TYPE = "Bearer";
    
    @XmlTransient
    private User user;
    
    @XmlTransient
    private Date expiryDate;
    
    public Token() {
    }

    public Token(String token, User user, Date expiryDate) {
        this.token = token;
        this.user = user;
        this.expiryDate = expiryDate;
    }

    public String getToken() {
        return token;
    }
    
    public User getUser() {
        return user;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }
    
    public void setUser(User user) {
        this.user = user;
    }
    
    @Override
    public String toString() {
        return String.format("%s %s",TYPE, token);
    }
    
    public static String extractTokenString(MultivaluedMap<String, String> headers) {
        String authentication = headers.getFirst(HttpHeaders.WWW_AUTHENTICATE);
        
        if (authentication == null) {
            return null;
        }
        
        String[] split = authentication.split(" ");
        
        if (split.length != 2) {
            return null;
        }
        
        return split[1];
    }
}