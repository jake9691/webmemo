/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gstrs.webmemo.web.resources;

import com.gstrs.webmemo.model.entities.Conversation;
import com.gstrs.webmemo.model.entities.Message;
import com.gstrs.webmemo.model.entities.Person;
import com.gstrs.webmemo.persistence.IDAO;
import com.gstrs.webmemo.persistence.IWebMemo;
import com.gstrs.webmemo.persistence.validation.ViolationWrapper;
import static com.gstrs.webmemo.web.resources.ResourceUtils.*;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.json.JsonObject;
import javax.json.stream.JsonParsingException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

/**
 *
 * @author davida
 */
@ApplicationScoped
@Path("/conversation")
public class ConversationResource extends AbstractResource<Conversation> {

    @EJB
    private IWebMemo webMemo;

    @Override
    protected IDAO<Conversation, Long> getDao() {
        return webMemo.getConversationRegistry();
    }
    
    @Override
    protected Conversation parseJson(JsonObject json, Conversation conversation) throws JsonParsingException {
        Person person1 = webMemo.getEmployeeRegistry().findPerson(getJsonNumber(getJsonObject(json, "dependencies"), "person1Id"));
        Person person2 = webMemo.getEmployeeRegistry().findPerson(getJsonNumber(getJsonObject(json, "dependencies"), "person2Id"));
        conversation.addParticipant(person1);
        conversation.addParticipant(person2);
        return conversation;
    }

    @Override
    protected Conversation newEntity(JsonObject json) {
        return new Conversation(null, null);
    }

    @Override
    protected Set<ViolationWrapper> addDependenciesOnCreate(JsonObject json, Conversation conversation, List<Locale> localeList) {
        return new HashSet<>();
    }

    @Override
    protected Set<ViolationWrapper> addDependenciesOnUpdate(JsonObject json, Conversation conversation) {
        return new HashSet<>();
    }

    @Override
    protected GenericEntity prepareResult(Conversation obj) {
        return new GenericEntity<Conversation>(obj) {
        };
    }

    @Override
    protected GenericEntity prepareResult(List<Conversation> obj) {
        return new GenericEntity<List<Conversation>>(obj) {
        };
    }
    
    @GET
    @Path("{id}/participants")
    @Produces({"application/json"})
    public Response findRangeOfParticipantsForConversation(@Context HttpHeaders headers, @PathParam("id") long id, @QueryParam("offset") int offset, @QueryParam("count") int count){
        if(count == 0){
            count = 10;
        }
        List<Person> persons = webMemo.getConversationRegistry().findRangeOfParticipantsForConversation(id, offset, count);
        return Response.ok(new GenericEntity<List<Person>>(persons){}).build();
    }
    
    @GET
    @Path("{id}/messages")
    @Produces({"application/json"})
    public Response findRangeWithMessagesForConversation(@Context HttpHeaders headers, @PathParam("id") long id, @QueryParam("offset") int offset, @QueryParam("count") int count){
        if(count == 0){
            count = 10;
        }
        List<Message> messages = webMemo.getMessageRegistry().findRangeWithMessagesForConversation(id, offset, count);
        return Response.ok(new GenericEntity<List<Message>>(messages){}).build();
    }
    
    
}
