/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gstrs.webmemo.web.resources;

import com.gstrs.webmemo.model.entities.Employee;
import com.gstrs.webmemo.model.entities.Reception;
import com.gstrs.webmemo.persistence.IDAO;
import com.gstrs.webmemo.persistence.IWebMemo;
import com.gstrs.webmemo.persistence.validation.ViolationWrapper;
import static com.gstrs.webmemo.web.resources.ResourceUtils.*;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.json.JsonObject;
import javax.json.stream.JsonParsingException;
import javax.ws.rs.Path;
import javax.ws.rs.core.GenericEntity;

/**
 *
 * @author davida
 */
@ApplicationScoped
@Path("/reception")
public class ReceptionResource extends AbstractResource<Reception> {

    @EJB
    private IWebMemo webMemo;

    @Override
    protected IDAO<Reception, Long> getDao() {
        return webMemo.getReceptionRegistry();
    }

    @Override
    protected Reception parseJson(JsonObject json, Reception reception) throws JsonParsingException {
        Employee receptionCreator = webMemo.getEmployeeRegistry().find(getJsonNumber(getJsonObject(json, "creator"), "id"));
        reception = parseReception(json, reception, receptionCreator);
        return reception;
    }

    @Override
    protected Reception newEntity(JsonObject json) {
        return new Reception(null);
    }

    @Override
    protected Set<ViolationWrapper> addDependenciesOnCreate(JsonObject json, Reception reception, List<Locale> localeList) {
        return new HashSet<>();
    }

    @Override
    protected Set<ViolationWrapper> addDependenciesOnUpdate(JsonObject json, Reception entity) {
        return new HashSet<>();
    }

    @Override
    protected GenericEntity prepareResult(Reception obj) {
        return new GenericEntity<Reception>(obj) {
        };
    }

    @Override
    protected GenericEntity prepareResult(List<Reception> obj) {
        return new GenericEntity<List<Reception>>(obj) {
        };
    }
}
