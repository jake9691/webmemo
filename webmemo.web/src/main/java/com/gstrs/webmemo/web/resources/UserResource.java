/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gstrs.webmemo.web.resources;

import com.gstrs.webmemo.model.entities.Permission;
import com.gstrs.webmemo.model.entities.User;
import com.gstrs.webmemo.persistence.IDAO;
import com.gstrs.webmemo.persistence.IWebMemo;
import com.gstrs.webmemo.persistence.validation.ViolationWrapper;
import com.gstrs.webmemo.web.auth.AuthManager;
import com.gstrs.webmemo.web.auth.Token;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.json.JsonObject;
import javax.json.stream.JsonParsingException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.HttpHeaders;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import javax.ws.rs.core.Response;
import static com.gstrs.webmemo.web.resources.ResourceUtils.*;
import java.util.Locale;

/**
 *
 * @author davida
 */
@ApplicationScoped
@Path("/user")
public class UserResource extends AbstractResource<User> {
    
    @Inject
    private AuthManager manager;
    
    @EJB
    private IWebMemo webMemo;
    
    @Override
    protected IDAO<User,Long> getDao() {
        return webMemo.getUserRegistry();
    }
    
    @Override
    protected User newEntity(JsonObject json){
        return new User(null,null);
    }

    @Override
    protected User parseJson(JsonObject json, User user) throws JsonParsingException {
        user.setUsername(getJsonString(json, "username"));
        user.setPermission(Permission.valueOf(getJsonString(json, "permission")));
        String pass = getJsonString(json,"password");
        if(pass != null){
            user.setPassword(pass);
        }
        return user;
    }
    
    @Override
    protected Set<ViolationWrapper> addDependenciesOnCreate(JsonObject json, User object, List<Locale> localeList) {
        return new HashSet<>();
    }
    
    @Override
    protected Set<ViolationWrapper> addDependenciesOnUpdate(JsonObject json, User entity) {
        return new HashSet<>();
    }

    @Override
    protected GenericEntity prepareResult(User obj) {
        return new GenericEntity<User>(obj) {};
    }

    @Override
    protected GenericEntity prepareResult(List<User> obj) {
        return new GenericEntity<List<User>>(obj) {};
    }
    
    @GET
    @Path("current")
    @Produces({APPLICATION_JSON})
    public Response getCurrentUser(@Context HttpHeaders headers) {
        String token = Token.extractTokenString(headers.getRequestHeaders());
        
        Token t = manager.getToken(token);
        
        if (t != null) {
            User u = t.getUser();
            
            GenericEntity entity = new GenericEntity<User>(u) {};
            return Response.ok(entity).build();
        }
        
        return Response.status(401).build();
    }
}
