/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gstrs.webmemo.web.resources;

import com.gstrs.webmemo.model.entities.User;
import com.gstrs.webmemo.persistence.IWebMemo;
import com.gstrs.webmemo.persistence.validation.ViolationWrapper;
import com.gstrs.webmemo.web.auth.AuthManager;
import com.gstrs.webmemo.web.auth.Token;
import java.util.HashSet;
import java.util.Set;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.json.JsonObject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;


/**
 *
 * @author davida
 */
@ApplicationScoped
@Path("/auth")
public class AuthResource {
    
    @Inject 
    private AuthManager manager;
    
    @EJB 
    private IWebMemo webMemo;
    
    @POST 
    @Produces("application/json")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response login(JsonObject obj) {
        Set<ViolationWrapper> errors = new HashSet<>();
        
        if (obj == null) {
            return Response.status(400).build();
        }
        
        String username = obj.getString("username", null);
        
        if (username == null || username.isEmpty()) {            
            errors.add(violation("username", "A username needs to be specified!"));
            return buildErrorResponse(400, errors);
        }
        
        User u = webMemo.getUserRegistry().getByUsername(username);
        
        if (u == null) {
            errors.add(violation("username", "A user with that name was not found."));
            return buildErrorResponse(401, errors);
        } 
        
        Token token = manager.login(u, obj.getString("password"));
        
        if (token == null) {
            errors.add(violation("username", "Login failed."));
            return buildErrorResponse(401, errors);
        }
             
        GenericEntity<Token> p = new GenericEntity<Token>(token) {};
        
        Response r = Response.ok(token).build();
        
        return r; 
    }
    
    private Response buildErrorResponse(int status, Set<ViolationWrapper> violations) {
        GenericEntity<Set<ViolationWrapper>> entity = new GenericEntity<Set<ViolationWrapper>>(violations) {};
        
        return Response.status(status).entity(entity).build();
    }
    
    @POST
    @Produces("text/html")
    @Path("logout")
    public Response invalidate(@Context HttpHeaders headers) {
        String token = Token.extractTokenString(headers.getRequestHeaders());
        
        manager.invalidate(token);
        
        return Response.ok().build();
    }
    
    @POST
    @Produces(APPLICATION_JSON)
    @Path("check")
    public Response checkToken(@Context HttpHeaders headers) {
        String token = Token.extractTokenString(headers.getRequestHeaders());
        
        if (token != null && !token.isEmpty()){
            Token t = manager.getToken(token);
            
            if (t != null) {
                return Response.ok(t).build();
            }
        }
        return Response.status(Status.FORBIDDEN).build();
    }
    
    private static ViolationWrapper violation(String key, String message) {
        ViolationWrapper wrapper = new ViolationWrapper();
        wrapper.setKey(key);
        wrapper.setMessage(message);
        wrapper.setIsField(true);
        return wrapper;
    }
}
