
# Web Memo Client

This is the client side part of the Web Memo application. It uses AngularJS to bring order to the chaos that can be frontend programming, LESS for CSS preprocessing, and bootstrap as a basis for CSS. 

We use a build system called [Gulp](https://github.com/gulpjs/gulp), which is written in Javascript and runs on the [Node.js](https://www.nodejs.org) platform. As such, Node.js is required to build the client application. 

NPM is used for the node package management, and bower is used for the client side library package management. 

### Setting up Dev Environment

First, you need to download and install Node.js for your platform, and make sure that the node executable is in your Path environment variable. When that is done, you need to set up [Bower](https://github.com/bower/bower). You do that by opening up a Terminal/Command Prompt, typing `npm install -g bower` and pressing enter. NPM should kick in and install bower globally for your system, which means that you can type `bower` in your Terminal to use it. You also need to install Gulp globally, like bower: `npm install -g gulp` .

When Bower and Gulp have been installed you can install all the dependencies for the client side application. You do that by `cd`-ing into the directory which contains the client application code, and typing the following into your Terminal : 

1. `npm install`
2. `bower install`

These two commands install dependencies needed for the build (see `gulpFile.js`), and then dependencies needed for the client side application, respectively. 

### Building 

Once you have set up your development environment and are inside the client application directory, you can test it out by typing `gulp` into your Terminal. This should execute the default build task for gulp, which compiles all the javascript into a single file containing all the client side code, compiles LESS into CSS, and copies any assets, such as fonts, into the `webmemo.web/src/main/webapp` directory. 


### Working on this 

The main folder is the `src/` folder. It contains the entry point to the angular application, `app.js`, as well as the `templates/` folder, which contains all the bundled HTML templates the Angular app uses, and lastly the LESS style files in the `styles/` folder.

We structure the app into logical modules, with the root angular app/module being called `memoapp`. For instance all the logic pertaining to the Memo object (CRUD, listing, etc) goes into a seperate module, which will be called `memo.app.memo`. The directory tree should look something like the following: 

```

- src/
  - app.js          (memoapp)
  - styles/
  - templates/      (memo.app.templates)
  - memos/          (memo.app.memos)
    - index.js      Creates the memo.app.memos module and references all the submodules
    - directives/   (memo.app.memos.directives)
      - index.js    Contains all the references to the directives in the module
        . 
        . 
        .
    - controllers/  (memo.app.memos.controllers)
      - index.js    Contains all the references to the controllers in the module
        .
        . 
        .
    - services/     (memo.app.memos.services)
      - index.js    Contains all the references to the services in the module
        .
        .
        .
    - util/         (memo.app.memos.util)
      - index.js    Contains all the references to filters or any other support code belonging to this module.
        .
        .
        .
  
```


