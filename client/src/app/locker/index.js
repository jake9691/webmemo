'use strict';
var angular = require('angular');

require('angular-cookies');

var locker = angular.module('memo.app.locker', ['ngCookies']);

// the events for the locker stuff.
locker.constant('LOCKER_EVENTS', {
	locked: 'locker-locked',
	unlocked: 'locker-unlocked'
});

locker.factory('LockerService', require('./services/LockerService'));

locker.directive('lockerStatus', require('./directives/lockerStatus'));

locker.controller('LockerCtrl', require('./controllers/LockerCtrl'));

locker.config(require('./services/LockerConfig'));

locker.run(require('./services/LockerRunner'));