'use strict';

// displays the status of the locker 
module.exports = [
  'LockerService',
  '$rootScope',
  'LOCKER_EVENTS',
  'AUTH_EVENTS',
  function (LockerService, $rootScope, LOCKER_EVENTS, AUTH_EVENTS) {
    return {
      restrict: 'EA',
      templateUrl: 'locker/locker-status.html',
      controller: 'LockerCtrl',
      controllerAs: 'ctrl',
      link: function (scope, element, attrs) {
        
        $rootScope.$on(LOCKER_EVENTS.unlocked, function (evt, e) {
          scope.employee = e;
        });

        $rootScope.$on(LOCKER_EVENTS.locked, function () {
          scope.employee = null;
        });

      }
    };
  }
];