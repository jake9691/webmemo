'use strict';

// supplies methods that interact with the locker service.
module.exports = [
  '$scope',
  'LockerService',
  'LOCKER_EVENTS',
  '$location',
  '$routeParams',
  function ($scope, LockerService, LOCKER_EVENTS, $location, $routeParams) {
    var me = this;
    
    me.submit = function (pinCode) {
      $scope.currentUser().then(function(u) {
        LockerService.loginEmployee(pinCode, u.store.id).success(function () {        
          var old = $routeParams.ret;
          if (/locked\?ret/gi.test(old)) {
            old = '/';
          }
          
          $location.url(old ? old : '/');
          $scope.invalid = false;
        }).error(function() {
          $scope.invalid = true;
        }); 
      });
    };

    me.lock = function () {
      LockerService.logoutEmployee();
    };
  }
];