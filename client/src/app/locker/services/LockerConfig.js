'use strict';


// supplies a path for the locker
module.exports = [
  '$routeProvider',
  function ($routeProvider) {
    $routeProvider.when('/locked', {
      templateUrl : 'locker/locker.html',
      data: {
        authorizedRoles: ['ORGANIZATIONOWNER', 'EMPLOYEE']
      }
    }); 
  }
];