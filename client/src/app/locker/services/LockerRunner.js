'use strict';


// a runner which provides locker event handling and bootstrapping the locker.
module.exports = [
  '$rootScope',
  'LOCKER_EVENTS',
  'AUTH_EVENTS',
  '$location',
  'LockerService',
  'AuthService',
  function($rootScope, LOCKER_EVENTS, AUTH_EVENTS, $location, LockerService, AuthService) { 
    var isLocked;
    $rootScope.$on(LOCKER_EVENTS.unlocked, function (employee) {
      isLocked = false;
      //$location.url('/');
    });

    $rootScope.$on(LOCKER_EVENTS.locked, function () {
      isLocked = true;

      if (AuthService.isAuthenticated()) {
        $location.url('/locked?ret=' + $location.url());  
      }
    });

    

    $rootScope.$on(AUTH_EVENTS.logoutSuccess, function () {
      $rootScope.$broadcast(LOCKER_EVENTS.locked);
    });

    $rootScope.$on(AUTH_EVENTS.loginSuccess, function (u) {
      LockerService.initializeFromCookie();
    });

    $rootScope.$on('$routeChangeStart', function (event, next) {
      if (isLocked && AuthService.isAuthenticated() && !/locked/gi.test($location.url())) {
        event.preventDefault();
        $location.url('/locked?ret=' + $location.url());  
      }
    });
  } 
];
