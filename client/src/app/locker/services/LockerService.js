'use strict';

var config = require('config');

// supplies methods for logging and employee in and out of the app side, either unlocking or 
// locking the site in the process.
module.exports = [
  '$rootScope',
  'LOCKER_EVENTS',
  '$http',
  '$cookies',
  '$q',
  function ($rootScope, LOCKER_EVENTS, $http, $cookies, $q) {
    var employeePromise = $q.defer();
    var currentEmployee = null; 

    function getEmployee () {
        if (currentEmployee === null) {
            return employeePromise.promise;
        }   
        return $q.when(currentEmployee);
    }

    function loginEmployee (pinCode, store) {  

      store = parseInt(store, 10);
      var request = {
        pinCode: pinCode,
        storeId: store
      };
    
      /* this is bad, but the time pressure was hiiiiigh */     
      $cookies.pinCode = pinCode;
      $cookies.store = store;

      return $http.post(config.apiRoot + '/employee/verify', request).success(function (e) {
        currentEmployee = e;
        employeePromise.resolve(currentEmployee);
        $rootScope.$broadcast(LOCKER_EVENTS.unlocked, currentEmployee);
      }).error(logoutEmployee);
    }

    function initializeFromCookie() {   
      return loginEmployee($cookies.pinCode, $cookies.store);
    }

    function logoutEmployee() {
      currentEmployee = null;
      
      delete $cookies.pinCode;
      delete $cookies.store; 

      $rootScope.$broadcast(LOCKER_EVENTS.locked);
    }

    return {
      getEmployee : getEmployee,
      loginEmployee : loginEmployee,
      initializeFromCookie: initializeFromCookie,
      logoutEmployee: logoutEmployee
    };
  }
];