/*
 * Parameters in searchForMemos are optional and not required.
 */

'use strict';

var config = require('config');

module.exports = [
  '$http', 
  function ($http) {

    function get(id) {
      return $http.get(config.apiRoot + '/customer/' +id);
    }
    
    function searchForMemos(searchWords, from, creatorId, responsibleId, providerId, status, isReclaim, offset, count){
      return $http.get(config.apiRoot + '/memo/search', {
          params: {
              words:searchWords,
              from:from,
              creatorId:creatorId,
              responsibleId:responsibleId,
              providerId:providerId,
              status:status,
              isReclaim:isReclaim,
              offset:offset,
              count:count
          }
      });
    }

    return {
      get: get,
      searchForMemos:searchForMemos
    };
  }
];