'use strict';

var config = require('config');

module.exports = [
	'$http',
	function ($http) {

		function create(reception) {
			return $http.post(config.apiRoot + '/reception', reception);
		}

		return {
			create: create
		};
	}
];
