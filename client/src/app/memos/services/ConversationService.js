'use strict';

var config = require('config');

module.exports = [
	'$http',
	function ($http) {

		function create(conversation) {
			return $http.post(config.apiRoot + '/conversation', conversation);
		}

		function update(conversation) {
			return $http.put(config.apiRoot + '/conversation/' + conversation.id, conversation);
		}

		function getList() {
			return $http.get(config.apiRoot + '/conversation');
		}

		function get(id) {
			return $http.get(config.apiRoot + '/conversation/'+ id);
		}
                
                function getConversationForCustomer(customerId) {
			return $http.get(config.apiRoot + '/customer/'+ customerId + "/conversation");
		}

		return {
			getList: getList,
			create: create,
			get: get,
                        getConversationForCustomer: getConversationForCustomer,
			update: update
		};
	}
];
