'use strict';

var config = require('config');

module.exports = [
	'$http',
	function ($http) {

		function create(order) {
			return $http.post(config.apiRoot + '/order', order);
		}

		return {
			create: create
		};
	}
];
