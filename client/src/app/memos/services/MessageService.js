'use strict';

var config = require('config');

module.exports = [
  '$http', 
  function ($http) {

    function getMessagesForConversation (id) {
      return $http.get(config.apiRoot + '/conversation/' + id + '/messages/');
    }
    
    function create(message) {
      return $http.post(config.apiRoot + '/message', message);
    }

    return {
      getMessagesForConversation: getMessagesForConversation,
      create: create
    };
  }
];