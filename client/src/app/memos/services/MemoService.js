'use strict';

var config = require('config');

module.exports = [
	'$http',
	function ($http) {

		function create(memo) {
			return $http.post(config.apiRoot + '/memo', memo);
		}

		function update(memo) {
			return $http.put(config.apiRoot + '/memo/' + memo.id, memo);
		}

		function getList() {
			return $http.get(config.apiRoot + '/memo');
		}

		function get(id) {
			return $http.get(config.apiRoot + '/memo/'+ id);
		}

		function remove(id) {
			return $http.delete(config.apiRoot + '/memo/' + id);
		}

		return {
			getList: getList,
			create: create,
			get: get,
			update: update,
			remove:remove
		};
	}
];
