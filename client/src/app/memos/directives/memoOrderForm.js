'use strict';

module.exports = function () {
	return {
		restrict: 'EA',
		templateUrl: 'memos/memo-order-form.html',
		scope: false,
		link: function (scope, element, attrs) {

			scope.$watch('memo', function (m) {
				if (!m) return;
				
				if (!scope.memo.order) {
					scope.memo.order = {};
				}

			});
		}
	};
};