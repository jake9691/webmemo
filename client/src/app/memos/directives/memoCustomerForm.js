/*
* The customer part of the src/templates/memos/memo.html
*/

'use strict';

var config = require('config');

module.exports = [
    '$http',
    function($http) {
    	return {
    		restrict: 'EA',
    		templateUrl: 'memos/memo-customer-form.html',
    		scope: true,
    		link: function (scope, element, attrs) {

                scope.search = function(val) {
                    return $http.get(config.apiRoot + '/customer/search', {
                        params: {
                            words: val,
                            count: 30
                        }
                    }).then(function(response) {
                        return response.data.map(function (c) {
                            c.searchString = c.name + ' ' + c.lastName;

                            if (c.mobilePhone && c.mobilePhone.length) {
                                c.searchString += ' (' + c.mobilePhone + ')';
                            }
                            
                            return c;
                        });
                    });
                };
            
                scope.selectCustomer = function(item, model, label) {
                    scope.memo.customer = item;
                };

                function reinitCustomer (c) {
                    if (c) {
                        c.customerCreatedInStore = scope.user.store;
                        c.dependencies = {
                            organizationId : scope.user.store.organization.id
                        };
                    }
                }
                scope.$watch('memo.customer', reinitCustomer);

                scope.$watch('customerSearch', function (e) {
                    if (!scope.memo) return;

                	if (!e) {
                        scope.memo.customer = {};
                        reinitCustomer(scope.memo.customer);
                	}
                });
    		}
    	};
    }   
];
