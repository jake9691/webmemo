
'use strict'; 


module.exports = function () {
	return {
		restrict: 'EA',
		templateUrl: 'memos/memo-details-form.html',
		scope: false
	};
};