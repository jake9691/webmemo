'use strict';

module.exports =  [
	'LockerService',
	function (LockerService) {
		return {
			restrict: 'EA',
			require: 'ngModel',
			templateUrl: 'memos/creator-selector.html',
			scope: true,
			link: function (scope, element, attrs, ngModelCtrl) {
				scope.$watch('employees', function (e) {
					if (!e) return;

					scope.value = ngModelCtrl.$viewValue;
				});	

				scope.$watch('value', function (v) {
					ngModelCtrl.$setViewValue(v);
				});

				scope.currentUser().then(function(u) {
					scope.readOnly = u.permission !== "ORGANIZATIONOWNER";
					LockerService.getEmployee().then(function (e) {
						scope.value = e.id;
					});
				});
			}
		};
	}
];