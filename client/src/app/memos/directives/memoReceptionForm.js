'use strict';

module.exports = function () {
	return {
		templateUrl: 'memos/memo-received-form.html',
		scope: false,
		restrict: 'EA'
	};
};