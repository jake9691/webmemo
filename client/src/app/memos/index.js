'use strict';

var angular = require('angular');

var controllers = angular.module('memo.app.memos.controllers', []);
controllers.controller('SearchCtrl', require('./controllers/SearchCtrl'));
controllers.controller('MemoCtrl', require('./controllers/MemoCtrl'));
controllers.controller('MessageCtrl', require('./controllers/MessageCtrl'));

var dirs = angular.module('memo.app.memos.directives', []);
dirs.directive('memoDetails', require('./directives/memoDetailsForm'));
dirs.directive('memoItem', require('./directives/memoItemForm'));
dirs.directive('memoOrder', require('./directives/memoOrderForm'));
dirs.directive('memoCustomer', require('./directives/memoCustomerForm'));
dirs.directive('memoReception', require('./directives/memoReceptionForm'));
dirs.directive('creatorSelector', require('./directives/creatorSelector'));
dirs.directive('createdDate', require('./directives/createdDate'));

var services = angular.module('memo.app.memos.services', []);
services.factory('MemoService', require('./services/MemoService'));
services.factory('SearchService', require('./services/SearchService'));
services.factory('MessageService', require('./services/MessageService'));
services.factory('ConversationService', require('./services/ConversationService'));
services.factory('OrderService', require('./services/OrderService'));
services.factory('ReceptionService', require('./services/ReceptionService'));

angular.module('memo.app.memos', [
  'memo.app.memos.controllers',
  'memo.app.memos.directives',
  'memo.app.memos.services'
]);