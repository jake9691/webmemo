//For the filters used in the search in the /app side of the application

module.exports = {};

module.exports.timePeriods = {
    d1: 'Today',
    d7: 'Last Week',
    d30: 'Last Month',
    else:'Other Date'
};
module.exports.isReclaim = {false: "No",true: "Yes"};
module.exports.statuses = {
    CREATED:"Created", 
    ORDERED:"Ordered",
    RECEIVED:"Received"};
