/*
 * Controller for the src/templates/memos/memo.html (Memo Creation Page)
 * 
 */

(function (){    
    'use strict'; 

    module.exports = [
        '$scope', 
        '$http',
        'MemoService',
        'EmployeeService',
        'ProviderService',
        '$location',
        '$routeParams',
        '$q',
        'CustomerService',
        function ($scope, $http, MemoService, EmployeeService, ProviderService, $location, $routeParams, $q, CustomerService) {
            var me = this;            


            // a memo can be updated with a new customer whenever. We need
            // to be able to handle that.
            function addCustomerToMemo(memo) {
                var deferred = $q.defer();

                if (memo.customer && !angular.equals(memo.customer, {}) && !memo.customer.id) {
                    CustomerService.create(memo.customer).success(function (c) {
                        memo.customer = c;
                        deferred.resolve(memo);
                    }).error(function (e) {
                        me.errors = e;
                        deferred.reject();
                    });
                } else {
                    deferred.resolve(memo);
                }

                return deferred.promise;
            }

            
            this.create = function (memo) {
                if((!memo.item || (memo.item  && !memo.item.provider)) && !confirm("Are you sure you don¨t want to set a provider?")) {
                     return;
                }
                addCustomerToMemo(memo).then(function(m) {
                    return MemoService.create(m).success(function(obj) {
                        $location.url('/memo/' + obj.id);
                    }).error(function (e) {
                        me.errors = e;
                    });
                });
            };  

            this.submit = function (memo) {
                return memo.id ? this.update(memo) : this.create(memo);
            };

            this.update = function (memo) {
                return addCustomerToMemo(memo).then(function (m) {
                    return MemoService.update(m).success(function(obj) {
                        $location.url('/memo/' + memo.id);
                    }).error(function (e) {
                        me.errors = e;
                    });
                });
            };


            // this monstrosity is needed for cold initialization.
            var shouldInit = true;
            $scope.currentUser().then(function (cu) {
                if (!cu) return;
                if (cu && shouldInit) {
                    $scope.cu = cu;
                    EmployeeService.getForOrganization(cu.store.organization.id).success(function(es) {
                        $scope.employees = es;
                    });

                    ProviderService.getProvidersInOrganization(cu.store.organization.id).success(function(ps) {
                        $scope.providers = ps;
                    });

                    shouldInit = false;

                    $scope.memo = {
                        status: '',
                        dependencies: {
                            storeId: cu.store.id,
                            organizationId: cu.store.organization.id
                        }
                    };   

                    if ($routeParams.id && $routeParams.id.length) {
                        MemoService.get($routeParams.id).success(function (m) {
                            $scope.memo = angular.extend($scope.memo, m);
                            $scope.memo.remindDate = new Date($scope.memo.remindDate);
                        });
                    }
                }
            });
           

          

            $scope.statuses = [
                {
                    label: 'Created',
                    value: 'CREATED'
                },
                {
                    label: 'Ordered',
                    value: 'ORDERED'
                },
                {
                    label: 'Received',
                    value: 'RECEIVED'
                }
            ];
        }
    ];
})();