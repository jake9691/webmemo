module.exports = ['$scope', '$modalInstance', '$filter', 'datePicked', 
    function ($scope, $modalInstance, $filter, datePicked) {
        
    $scope.datePicked = datePicked;

    // The last possible date to select, earlier is greyed out
    $scope.minDate = '2014-01-01';

    $scope.today = function() {
        $scope.dt = new Date();
    };
    $scope.today();

    $scope.clear = function () {
        $scope.dt = null;
    };

    // Disable weekend selection
    $scope.disabled = function(date, mode) {
        return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
    };

    $scope.toggleMax = function() {
        $scope.maxDate = $scope.maxDate ? null : new Date();
    };
    $scope.toggleMax();

    $scope.open = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy-MM-dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[1];

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    $scope.ok = function () {
        $scope.newdt = $filter('date')($scope.dt, $scope.format);
        $modalInstance.close($scope.newdt);
    };

}];