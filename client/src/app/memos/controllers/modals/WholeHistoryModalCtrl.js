module.exports = ['$scope', '$modalInstance', 'messages',
        function ($scope, $modalInstance, messages) {
        $scope.messages = messages;
        
         $scope.cancel = function () {
                $modalInstance.dismiss();
            };
    }];
