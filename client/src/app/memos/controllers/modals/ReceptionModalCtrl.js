module.exports = ['$scope', '$modalInstance', 'selected', 'EmployeeService', 'LockerService','ReceptionService','MemoService','currentUser', '$q',
    function ($scope, $modalInstance, selected, EmployeeService, LockerService, ReceptionService, MemoService, currentUser, $q) {
        EmployeeService.getForStore(currentUser.store.id).success(function (employeeList) {
            $scope.employees = employeeList;
        });
        
        EmployeeService.getForStore(currentUser.store.id).success(function (employeeList) {
            $scope.employees = employeeList;
        });
        $scope.reception = {};
        
        LockerService.getEmployee().then(function(employee){
            $scope.reception.creator = employee;
        });

        $scope.ok = function () {
            var deferreds = [];
            for (var i = 0; i < selected.length; i++){
                
                (function (selectedMemo, r, idx) {
                    deferreds[idx] = $q.defer();
                    
                    ReceptionService.create(r).success(function(reception){
                        selectedMemo.reception = reception;
                        MemoService.update(selectedMemo).success(function(memo){
                            deferreds[idx].resolve(memo);
                        });
                    });
                })(selected[i], $scope.reception, i);
            }
            
            $modalInstance.close($q.all(deferreds.map(function(d) {return d.promise; })));
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }];