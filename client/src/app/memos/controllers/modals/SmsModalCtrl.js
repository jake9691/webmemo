module.exports = ['$scope', '$modalInstance', 'selected', 
    function ($scope, $modalInstance, selected) {
        
    $scope.selected = selected;
    $scope.name = selected.map(function(p) { return p.name; });
    //debugger;
    $scope.selected.item = selected[0];

    $scope.ok = function () {
        $modalInstance.close($scope.selected.item);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}];
