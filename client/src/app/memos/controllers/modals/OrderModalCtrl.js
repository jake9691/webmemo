module.exports = ['$scope', '$modalInstance','selected', 'EmployeeService', 'LockerService','OrderService','MemoService','currentUser', '$q',
    function ($scope, $modalInstance, selected, EmployeeService, LockerService, OrderService, MemoService, currentUser, $q) {

    EmployeeService.getForStore(currentUser.store.id).success(function (employeeList) {
            $scope.employees = employeeList;
    });
        $scope.order = {};
        
        LockerService.getEmployee().then(function(employee){
            $scope.order.creator = employee;
        });

        $scope.ok = function () {
            var deferreds = [];
            for (var i = 0; i < selected.length; i++){
                (function (selectedMemo, o, idx) {
                    deferreds[idx] = $q.defer();
                    
                    OrderService.create(o).success(function(order){
                        selectedMemo.order = order;
                        MemoService.update(selectedMemo).success(function(memo){
                            deferreds[idx].resolve(memo);
                        });
                    });
                })(selected[i], $scope.order, i);
            }
            
            $modalInstance.close($q.all(deferreds.map(function(d) {return d.promise; })));
        };


        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }];

