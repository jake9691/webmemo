/*
 *  Controller for src/templates/memos/messageHistory.html
 */

(function () {
    'use strict';
    
    var WholeHistoryModalCtrl = require('./modals/WholeHistoryModalCtrl');

    module.exports = [
        '$scope', '$routeParams', '$modal', 'MessageService','ConversationService','LockerService',
        function ($scope, $routeParams, $modal, MessageService, ConversationService, LockerService)
        {
            
            LockerService.getEmployee().then(function(e){
                $scope.employee1 = e;
            })
            
            $scope.send = function () {
                MessageService.create({
                    message: $scope.inputMessage,
                    sender: {
                        id: $scope.employee1.id
                    },
                    dependencies: {
                        conversationId: parseInt($routeParams.id,10)
                    }
                }).success(function(message){
                    $scope.messages.push(message);
                    $scope.inputMessage = "";
                });

            };

            this.fetchMessages = function() {
                return MessageService.getMessagesForConversation($routeParams.id);
            };
            
            this.fetchMessages().success(function(s) {
                $scope.messages = s; 
            });
            
            $scope.model = {
                customer: $routeParams.id
            };
            
            $scope.showWholeHistory = function () {

                var modalInstance = $modal.open({
                    templateUrl: 'memos/wholeMessageHistory.html',
                    controller: WholeHistoryModalCtrl,
                    size: 'lg',
                    resolve: {
                        messages: function () {
                            return $scope.messages;
                        }
                    }
                });

                // If we pass information on to the other controller, then
                // use this function
                modalInstance.result.then();
            };
        }
    ];
})();