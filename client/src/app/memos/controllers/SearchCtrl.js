/*
 * Controller for src/templates/memos/search.html
 * Sets initial states for filters, filter functionality and fetches 
 * appropriate memos while searching.
 */

(function () {
    'use strict';
    
    // Load the filters used in the search
    var Filters = require('./Filters');
    
    var SmsModalCtrl = require('./modals/SmsModalCtrl');
    var DatePickerModalCtrl = require('./modals/DatePickerModalCtrl');
    var OrderModalCtrl = require('./modals/OrderModalCtrl');
    var ReceptionModalCtrl = require('./modals/ReceptionModalCtrl');

    // The Search controller itself
    module.exports = ['$scope', '$modal', '$routeParams', 'SearchService','EmployeeService','ProviderService','ConversationService','$location', 'MemoService',
        function ($scope, $modal, $routeParams, SearchService, EmployeeService, ProviderService, ConversationService, $location, MemoService) {
        
        $scope.printDiv = function(divName) {
            var printContents = document.getElementById('memo-detail-' + divName).innerHTML;
            
            var newWindow=window.open();
            newWindow.document.body.innerHTML = printContents;
            newWindow.document.close();
            newWindow.focus();
            newWindow.print();
            newWindow.close();

       };


        // No memos loaded in the beginning
        $scope.memos = [];
        
        $scope.search_text = ""
        
        // Has a search taken place?
        $scope.showResults = false;
        
        // Collapses the "visa filterar" view
        $scope.isFilterCollapsed = true;
        
        // Are more than two items in the table selected?
        // If not, the buttons in the bottom of the table are disabled.
        $scope.isLessThanOneMemoSelected = true;
        
        // #############################################################   
        // -------------------------------------------------------------
        // Here follows a bunch of crap related to the filters.
        // -------------------------------------------------------------
        // #############################################################
        // 
        //Initialize filters
        $scope.timeFilter = "";
        $scope.responsibleFilter = "";
        $scope.creatorFilter = "";
        $scope.providerFilter = "";
        $scope.statusFilter = "";
        $scope.reclaimFilter = "";
        
        //Are the filters visible or not? No filters selected at initialization time..
        $scope.isTimeFilterVisible = false;
        $scope.isResponsibleFilterVisible = false;
        $scope.isCreatorFilterVisible = false;
        $scope.isProviderFilterVisible = false;
        $scope.isStatusFilterVisible = false;
        $scope.isReclaimFilterVisible = false;
        $scope.isThereAFilter = false;
        
        // Check whether to remove the "remove all filters" box and do it if applicable
        $scope.maybeRemoveAllFilters = function() {
            if ($scope.isTimeFilterVisible === false &&
                $scope.isResponsibleFilterVisible === false &&
                $scope.isCreatorFilterVisible === false && 
                $scope.isProviderFilterVisible === false &&
                $scope.isStatusFilterVisible === false && 
                $scope.isReclaimFilterVisible === false)
                {
                    $scope.isThereAFilter = false;
                }
        };
        
        // Add and remove the time filter
        $scope.addTimeFilter = function(timePeriod, stringValue) {
            $scope.isTimeFilterVisible = true;
            $scope.isThereAFilter = true;
            if(timePeriod === "else"){
                var modalInstance = $modal.open({
                templateUrl: 'memos/datePicker.html',
                controller: DatePickerModalCtrl,
                size: '',
                resolve: {
                    datePicked: function () {
                        return $scope.datePicked;
                        },
                    }
                });
                modalInstance.result.then(function (dateChosen) {
                    $scope.datePicked = dateChosen;
                }, function () {
                });
            }
            $scope.timeFilter = [timePeriod, stringValue];
            
            
            //Temporary, just for testing the mock data
            $scope.timeFrom = timePeriod;
        };
        $scope.removeTimeFilter = function() {
            $scope.isTimeFilterVisible = false;
            $scope.timeFilter = "";
            $scope.maybeRemoveAllFilters();
            
            //Temporary. Remove this after connected to the backend!
            $scope.getMemos();
        };
        
        // Add and remove the responsible filter
        $scope.addResponsibleFilter = function(responsible) {
            $scope.isResponsibleFilterVisible = true;
            $scope.isThereAFilter = true;
            $scope.responsibleFilter = responsible;
        };
        $scope.removeResponsibleFilter = function() {
            $scope.isResponsibleFilterVisible = false;
            $scope.responsibleFilter = "";
            $scope.maybeRemoveAllFilters();
        };      
        
        // Add and remove the creator filter
        $scope.addCreatorFilter = function(creator) {
            $scope.isCreatorFilterVisible = true;
            $scope.isThereAFilter = true;
            $scope.creatorFilter = creator;
        };
        
        $scope.removeCreatorFilter = function() {
            $scope.isCreatorFilterVisible = false;
            $scope.CreatorFilter = "";
            $scope.maybeRemoveAllFilters();
        };
        
        // Add and remove the provider filter
        $scope.addProviderFilter = function(provider) {
            $scope.isProviderFilterVisible = true;
            $scope.isThereAFilter = true;
            $scope.providerFilter = provider;
        };
        
        $scope.removeProviderFilter = function() {
            $scope.isProviderFilterVisible = false;
            $scope.providerFilter = "";
            $scope.maybeRemoveAllFilters();
        };     
        
        // Add and remove the status filter
        $scope.addStatusFilter = function(status,stringValue) {
            $scope.isStatusFilterVisible = true;
            $scope.isThereAFilter = true;
            $scope.statusFilter = [status,stringValue];
        };
        
        $scope.removeStatusFilter = function() {
            $scope.isStatusFilterVisible = false;
            $scope.statusFilter = "";
            $scope.maybeRemoveAllFilters();
        };
        
        // Add and remove the reclaim filter
        $scope.addReclaimFilter = function(reclaim, stringValue) {
            $scope.isReclaimFilterVisible = true;
            $scope.isThereAFilter = true;
            $scope.reclaimFilter = [reclaim, stringValue];
        };
        
        $scope.removeReclaimFilter = function() {
            $scope.isReclaimFilterVisible = false;
            $scope.reclaimFilter = "";
            $scope.maybeRemoveAllFilters();
        };
        
        // Remove all filters
        $scope.removeAllFilters = function() {
            $scope.reclaimFilter = "";
            $scope.timeFilter = "";
            $scope.responsibleFilter = "";
            $scope.creatorFilter = "";
            $scope.providerFilter = "";
            $scope.statusFilter = "";
            
            $scope.isTimeFilterVisible = false;
            $scope.isResponsibleFilterVisible = false;
            $scope.isCreatorFilterVisible = false;
            $scope.isProviderFilterVisible = false;
            $scope.isStatusFilterVisible = false;
            $scope.isReclaimFilterVisible = false;
            $scope.isThereAFilter = false;
            
        };
        // #############################################################
        // -------------------------------------------------------------
        // End of the filter stuff!
        // -------------------------------------------------------------    
        // 
        // #############################################################
        // 
        // -------------------------------------------------------------
        // Stuff to do with memo deletion and collapsing them:
        // -------------------------------------------------------------
        // #############################################################
        
        $scope.deleteConfirmation = function (memo) {
            var r = confirm("Are you sure you want do delete this memo?");
            if (r === true) {
                memo.expanded = false;
                
                $scope.removeRow(memo.id);

                // Now send information about memo deletion to the server!
                MemoService.remove(memo.id).success(function () {
                    
                });
            }
        };
        
        $scope.removeRow = function(id) {				
		var index = -1;		
		var memoArr = $scope.memos;
		for (var i = 0; i < memoArr.length; i++) {
                    if (memoArr[i].id === id) {
                        index = i;
                        break;
                    }
		}
		if (index === -1) 
                {
                    alert( "Something gone wrong" );
		}
		$scope.memos.splice(index, 1);		
	};
        
        $scope.collapseMemos = function(memo) {
            for (var i = 0; i < $scope.memos.length; i++) { 
                if ($scope.memos[i] !== memo)
                {
                    $scope.memos[i].expanded = false;
                }
            }
        };
        // -------------------------------------------------------------
        // End of memo deletion stuff
        // -------------------------------------------------------------
        
        // Function for getting the memos, i.e. after the search button is pressed
        $scope.getMemos = function () {
            
            //Collapse all previously open memos
            $scope.collapseMemos();
            
            //Get the mock memos 
            SearchService.searchForMemos($scope.search_text, $scope.timeFilter[0] === "else" ? $scope.datePicked : $scope.timeFilter[0],$scope.creatorFilter.id, $scope.responsibleFilter.id,
            $scope.providerFilter.id, $scope.statusFilter[0], $scope.reclaimFilter[0], 0,100000).success(function(memos) {
  		        $scope.memos = memos;
                $scope.memoCount = memos.length;
                $scope.showResults = memos.length;
                $scope.maxSize = 10; 
            });
//        searchWords, from, creatorId, responsibleId, providerId, status, isReclaim, offset, count
            
            
            //Get the data for the filters
            $scope.timePeriods = Filters.timePeriods;
            $scope.currentUser().then(function(u) {
                EmployeeService.getForStore(u.store.id).success(function(employeeList){
                    $scope.responsibles = employeeList;
                    $scope.creators = employeeList;
                });
            
                ProviderService.getForOrganization(u.store.organization.id).success(function(providerList){
                    $scope.providers = providerList;
                });
                
                
            });
            
            
            $scope.statuses = Filters.statuses;
            
            $scope.isReclaim = Filters.isReclaim;
            
            // The "visa filterar" box is now expanded
            $scope.isFilterCollapsed = false;
        };

        // -------------------------------------------------------------
        // Functions related to opening various modals are here:
        // -------------------------------------------------------------
        // 
        // Opens the send sms modal
        $scope.showSmsPopup = function () {
            var modalInstance = $modal.open({
                templateUrl: 'memos/writeSms.html',
                controller: SmsModalCtrl,
                size: '',
                resolve: {
                    selected: function () {
                        return $scope.selected;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
            });
        };
        
        $scope.showSmsPopup = function () {
            var modalInstance = $modal.open({
                templateUrl: 'memos/writeSms.html',
                controller: SmsModalCtrl,
                size: '',
                resolve: {
                    selected: function () {
                        return $scope.selected;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
            });
        };
        
        // Opens the date picker popup for selecting a time period
        $scope.openConversation = function (memo) {
            ConversationService.getConversationForCustomer(memo.customer.id).success(function(conversation) {
  		$location.url('/messageHistory/' + conversation.id);
            }).error(function(){
                ConversationService.create({
                    dependencies:
                            {person1Id:memo.customer.id}
                }).success(function(conversation){
                    $location.url('/messageHistory/' + conversation.id);
                });
            });
            
        };
        
        // Opens the date picker popup for selecting a time period
        $scope.showOrderPopup = function (memos) {

            var modalInstance = $modal.open({
                templateUrl: 'memos/orderItem.html',
                controller: OrderModalCtrl,
                size: '',
                resolve: {
                    selected: function () {
                        return memos;
                    },
                    currentUser: function () {
                        return $scope.user;
                    }
                }
            });

            modalInstance.result.then(function (resultMemos) {
                for (var i = 0; i < $scope.memos.length; ++i) {
                    for (var j = 0; j < resultMemos.length; ++j) {
                        if ($scope.memos[i].id === resultMemos[j].id) {
                            $scope.memos[i] = resultMemos[j];
                        }
                    }
                }
            });
        };
        
        
        // Opens the date picker popup for selecting a time period
        $scope.showReceptionPopup = function (memos) {

            var modalInstance = $modal.open({
                templateUrl: 'memos/receiveItem.html',
                controller: ReceptionModalCtrl,
                size: '',
                resolve: {
                    selected: function () {
                        return memos;
                    },
                    currentUser: function () {
                        return $scope.user;
                    }
                }
            });

            modalInstance.result.then(function (resultMemos) {
                for (var i = 0; i < $scope.memos.length; ++i) {
                    for (var j = 0; j < resultMemos.length; ++j) {
                        if ($scope.memos[i].id === resultMemos[j].id) {
                            $scope.memos[i] = resultMemos[j];
                        }
                    }
                }
            });
        };
        
        // -------------------------------------------------------------
        // End of functions related to opening modals.
        // -------------------------------------------------------------
        
        // -------------------------------------------------------------
        // Functions related to selecting many memos at the same time
        // -------------------------------------------------------------
        $scope.selected = [];

        var updateSelected = function (action, memo) {
            if (action === 'add' & $scope.selected.indexOf(memo) === -1)
            {
                $scope.selected.push(memo);
                $scope.isLessThanOneMemoSelected = false;
            }
            if (action === 'remove' && $scope.selected.indexOf(memo) !== -1)
                $scope.selected.splice($scope.selected.indexOf(memo), 1);
                if ($scope.selected.length === 0)
                {
                    $scope.isLessThanOneMemoSelected = true;
                }

        };

        $scope.updateSelection = function ($event, memo) {
            var checkbox = $event.target;
            var action = (checkbox.checked ? 'add' : 'remove');
            updateSelected(action, memo);
        };

        $scope.selectAll = function ($event) {
            var checkbox = $event.target;
            var action = (checkbox.checked ? 'add' : 'remove');
            for (var i = 0; i < $scope.memos.length; i++) {
                var entity = $scope.memos[i];
                updateSelected(action, entity);
            }
        };

        $scope.getSelectedClass = function (entity) {
            return $scope.isSelected(entity.id) ? 'selected' : '';
        };

        $scope.isSelected = function (id) {
            return $scope.selected.indexOf(id) >= 0;
        };

        $scope.isSelectedAll = function () {
            return $scope.selected.length === $scope.memos.length;
        };
        
        // -------------------------------------------------------------
        // Here follows code related to the pagination:
        // -------------------------------------------------------------
        
        $scope.maxSize = 6;
        $scope.bigCurrentPage = 1;
        
        // When the total of items on a page is changed
        $scope.changePaginationTotal = function()
        {
            // Calculate the number of pages in the table
            $scope.bigTotalItems = Math.ceil($scope.memoCount / $scope.maxSize)*10;
            // Go back to page 1 in the table
            $scope.bigCurrentPage = 1;
        };
        
        // -------------------------------------------------------------
        // End of pagination code
        // -------------------------------------------------------------

    }];
})();


