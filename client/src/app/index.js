'use strict';

var angular = require('angular');

require('angular-route');
require('angular-bootstrap');
require('memo-templates');
require('../shared');
require('./memos');
require('./locker');

var authRouteRegister = require('../shared/auth/routeRegister');

var app = angular.module('memo.app', ['ngRoute', 'ui.bootstrap', 'memo.app.memos','memo.templates','memo.shared', 'memo.app.locker']);

app.config(['$routeProvider', function($routeProvider) {
	$routeProvider.when('/', {
      templateUrl: 'memos/search.html',
      data: {
        authorizedRoles: ['EMPLOYEE', 'EMPLOYEEEXTENDED', 'ORGANIZATIONOWNER']
      }
    }).
    when('/registerCustomer', {
      templateUrl: 'customers/registerCustomer.html',
      data: {
        authorizedRoles: ['*']
      }
    }).
    when('/search', {
        templateUrl: 'memos/search.html',
        data: {
            authorizedRoles : ['EMPLOYEE','EMPLOYEEEXTENDED', 'ORGANIZATIONOWNER']
        }
    }).
    when('/messageHistory/:id', {
        templateUrl: 'memos/messageHistory.html',
        data: {
            authorizedRoles : ['EMPLOYEE','EMPLOYEEEXTENDED', 'ORGANIZATIONOWNER']
        }
    }).
    when('/memo/:id?', {
      templateUrl: 'memos/memo.html',
      data: {
        authorizedRoles: ['EMPLOYEE', 'EMPLOYEEEXTENDED', 'ORGANIZATIONOWNER']
      }
    }).
    when('/login', {
      templateUrl: 'auth/login-app.html',
      data: {
        authorizedRoles: ['*']
      }
    }).otherwise({
      templateUrl: '404.html',
      data: {
        authorizedRoles: ['*']
      }
    });


  //authRouteRegister($routeProvider);
}]);