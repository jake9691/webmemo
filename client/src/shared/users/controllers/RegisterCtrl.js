/*
 * Creates an employee by using the UserService
 */

'use strict';


module.exports = [
  '$scope',
  '$location',
  'UserService', 
  function ($scope, $location, UserService) {
    var that = this;
    this.register = function (c, destination) {
      return UserService.create(c).then(function(r) {
         $location.url(destination);
      },
      function(d) {
        that.errors = d.data;
      });
    };
  }
];