/*
 *  CRU calls to the backend. Also provides calls to the backend to get the
 *  current logged in user.
 */

'use strict';


var config = require('config');

module.exports = [
  '$http', 
  function ($http) {

    function create(u) {
      return $http.post(config.apiRoot + '/user', u);
    }

    function update (u) {
      return $http.put(config.apiRoot + '/user/' + u.id, u);
    }

    function get(id) {
      return $http.get(config.apiRoot + '/user/' +id);
    }

    function getCurrentUser() {
      return $http.get(config.apiRoot + '/user/current'); 
    }

    return {
      create : create,
      update: update,
      get: get,
      getCurrentUser: getCurrentUser
    };
  }
];