/*
 *  Dependencies for src/shared/users
 */

'use strict'; 


var angular = require('angular');



var services = angular.module('memo.shared.users.services', []);
services.factory('UserService', require('./services/UserService'));

var ctrls = angular.module('memo.shared.users.controllers', []);
ctrls.controller('RegisterCtrl', require('./controllers/RegisterCtrl'));

var directives = angular.module('memo.shared.users.directives', []);
directives.directive('registerForm', require('./directives/registerForm'));
directives.directive('currentUser', require('./directives/currentUser'));
directives.directive('registerUserForm', require('./directives/registerUserForm'));

var users = angular.module('memo.shared.users', [
  'memo.shared.users.services',
  'memo.shared.users.controllers',
  'memo.shared.users.directives'
]);

