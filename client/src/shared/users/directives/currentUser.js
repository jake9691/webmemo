/*
 * Provides the data needed to display the current logged in user.
 */

'use strict';

module.exports = [
  'UserService', 
  'AUTH_EVENTS',
  'AuthService',
  function (UserService, AUTH_EVENTS, AuthService) {
    return {
      restrict: 'EA',
      templateUrl: 'users/current-user.html',
      link: function(scope, element, attrs) {

        function getUser() {
          scope.currentUser().then(function (c) {
            scope.user = c;
          });
        }

        scope.$on(AUTH_EVENTS.loginSuccess, getUser);
      }
    };
  }
];