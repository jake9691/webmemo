/*
 *  Points to the template users/register-form which is used in
 *  src/templates/registerUser.html and sets scope.credentials.permission
 */

'use strict';

module.exports = function () {
  return {
    restrict: 'EA',
    templateUrl: 'users/register-form.html',
    scope: {
      role: '@',
      destination: '@'
    },
    link: function(scope, element, attrs) {
      scope.credentials = {
        permission: scope.role
      };
    }
  };
};