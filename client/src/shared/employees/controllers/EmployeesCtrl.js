/*
*  Controller for registering Employees. Provides the correct employee while
*  using the edit form in src/templates/employees/edit.html. Also makes sure that
*  the correct organization dependency is set in the creation of an Employee
*/
'use strict';

module.exports = [
	'$scope',
	'EmployeeService',
	'$routeParams',
	'$location',
	'OrganizationsService',
	'StoreService',
	function ($scope, EmployeeService, $routeParams, $location, OrganizationsService, StoreService) {
		$scope.organizationId = $routeParams.id;

		if ($routeParams.employee_id && $routeParams.employee_id.length) {
			EmployeeService.get($routeParams.employee_id).success(function(e) {
				$scope.employee = e;
			});
		}


		this.getForOrganization = function (organization_id) {
			return EmployeeService.getForOrganization(organization_id);
		};

		var me = this;

		this.create = function (e, destination) {
			e.dependencies = {
				organizationId: parseInt($scope.organizationId, 10)
			};
			
			return EmployeeService.create(e).success(function (e) {
				$location.url(destination);
			}).error(function(d) {
				me.errors = d;
			});
		};

		this.update = function (e, destination) {
			return EmployeeService.update(e).success(function (e) {
				$location.url(destination);
			}).error(function(e) {
				me.errors = e;
			});
		};

		this.deleteEmployee = function (e) {
			return EmployeeService.remove(e.id);
		};
	}
];