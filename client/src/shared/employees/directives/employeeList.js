/*
*  Points to the templateURL which displays a list of customers. Provides 
*  correct data for the table used in the .html page with fetchEmployees. Handles
*  the deletion of a an employee with the deleteEmployee function when the
*  delete button in the employee table is pressed
*/
'use strict';

module.exports = [
'EmployeeService',
function (EmployeeService) {
	return {
		restrict: 'EA',
		templateUrl: 'employees/employee-list.html',
		scope: {
			organization: '='
		},
		link: function (scope, element, attrs) {
			
			function fetchEmployees(os) {
				if (!os) return;
				scope.fetching = true;
				EmployeeService.getForOrganization(os.id).success(function (e) {
					scope.list = e;
					scope.fetching = false;
				});
			}

			scope.$watch('organization', fetchEmployees);
			
			scope.deleteEmployee = function (c) {
				EmployeeService.remove(c.id).success(function () {
					fetchEmployees(scope.organization);
				});
			};
		}
	};
}
];