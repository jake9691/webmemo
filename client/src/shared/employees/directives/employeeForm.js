/* 
*  Points to templateURL for ../templates/employees/edit
*  and ../templates/employees/new
*/ 
'use strict';

module.exports = [	
	function () {
		return {
			restrict: 'EA',
			templateUrl: 'employees/employee-form.html',
			scope: {
				action: '&', 
				errors: '=',
				employee: '=',
				organization: '='
			}
		};
	}
];