/*
*  Dependencies for src/shared/employees
*/
'use strict';

var angular = require('angular');

var employees = angular.module('memo.shared.employees', []);

employees.factory('EmployeeService', require('./services/EmployeeService'));
employees.controller('EmployeesCtrl', require('./controllers/EmployeesCtrl'));
employees.directive('employeeList', require('./directives/employeeList'));
employees.directive('employeeForm', require('./directives/employeeForm'));