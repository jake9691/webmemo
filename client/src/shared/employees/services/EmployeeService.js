/*
*  CRUD calls to the backend along with backend calls to fetch the correct 
*  employees for an organization.
*/
var config = require('config');

module.exports = [
	'$http', 
	function ($http) {

		function get (id) {
			return $http.get(config.apiRoot + '/employee/' + id);
		}

		function getForOrganization(id) {
			return $http.get(config.apiRoot + '/organization/' + id + '/employees');
		}
                
                function getForStore(id) {
			return $http.get(config.apiRoot + '/store/' + id + '/employees');
		}

		function create(e) {
			return $http.post(config.apiRoot + '/employee', e);
		}

		function update(e) {
			return $http.put(config.apiRoot + '/employee/' + e.id, e);
		}

		function remove(id) {
			return $http.delete(config.apiRoot + '/employee/' + id);
		}

		return {
			get: get,
			getForOrganization: getForOrganization,
                        getForStore: getForStore,
			create: create,
			update: update,
			remove: remove
		};
	}
];