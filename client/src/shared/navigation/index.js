/*
*  Navigation Controls
*/
'use strict';

var angular = require('angular');

require('angular-bootstrap');

var nav = angular.module('memo.shared.navigation', ['ui.bootstrap']);

nav.controller('NavigationCtrl', [
	'$scope',
	'$location',
	function ($scope, $location) {
		var active = null;
		var pages = {};
		this.pageClicked = function (page) {
			if (active && active.$id !== page.$id) {
				active.isActive = false;
			}

			page.isActive = true;
			active = page;
		};

		this.digest = function () {
			var page = pages[$location.url()];
			if (page) {
				this.pageClicked(page);
			}
		};

		this.register = function (page, href) {
			pages[href] = page;
		};

	}
]);

nav.directive('navigation', function () {
	return {
		restrict: 'AE',
		scope: {
			horizontal: '@'
		},
		replace:true,
		transclude: true,
		templateUrl: 'navigation/navigation.html',
		controller: 'NavigationCtrl'
	};
});

nav.directive('submenu', function () {
	return {
		restrict: 'AE',
		transclude: true, 
		replace: true,
		scope: {
			caption: '@'
		},
		templateUrl: 'navigation/dropdown-menu.html',
		require: '^navigation',
		controller: 'NavigationCtrl'
	};
});

nav.directive('navItem', function () {
	return {
		restrict: 'AE',
		transclude: true,
		replace: true,
		template: '<li ng-transclude></li>',
		require: '^navigation'
	};
});

nav.directive('navPage', function () {
	return {
		restrict: 'AE',
		templateUrl: 'navigation/page.html',
		replace: true,
		scope: {
			href: '@',
			caption: '@',
			isDefault: '@'
		},
		require: '^navigation',
		controller: 'NavigationCtrl',
		link: function (scope, element, attrs, navCtrl) {
			navCtrl.register(scope, scope.href);
			element.on('click', function () {
				navCtrl.pageClicked(scope);
			});

			if (scope.isDefault === "true") {
				element.triggerHandler('click');
			}
		}
	};
});