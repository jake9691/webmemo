/*
 *  Provides dependencies for src/shared
 */


'use strict';

var angular = require('angular');

require('memo-templates');

require('./auth');
require('./utilities');
require('./customers');
require('./employees');
require('./providers');
require('./navigation');
require('./users');
require('./stores');

var shared = angular.module('memo.shared', [
	'memo.shared.auth', 
	'memo.shared.navigation', 
	'memo.templates', 
	'memo.shared.utilities',
	'memo.shared.customers',
	'memo.shared.employees',
  'memo.shared.stores',
	'memo.shared.providers',
	'memo.shared.users']);