/*
* This directive is reused in multiple forms. Since multiple forms
* require a "store selector" this points to the temlateURL. Provides the correct
* stores to be used in the store selector with 
* StoreService.getStoresinOrganization function.
*/
'use strict'; 

module.exports = [
	'StoreService',
	function(StoreService) {
		return {
			restrict: 'EA',
			templateUrl: 'stores/store-selector.html',
			require: '^ngModel', 
			scope: {
				organization: '=',
				data: '=',
				name: '@'
			}, 
			link: function (scope, element, attrs, ngModelCtrl) {

				scope.$watch('organization', function (o) {
					if (!o) return;
					StoreService.getStoresInOrganization(o).success(function (ss) {
						scope.stores = ss;
						scope.val = ngModelCtrl.$viewValue;
					});
				});

				scope.$watch('val', ngModelCtrl.$setViewValue);
			}
		};
	}
];