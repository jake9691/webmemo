'use strict';

var angular = require('angular');

var dirs = angular.module('memo.shared.stores.directives', []);
dirs.directive('storeSelector', require('./directives/storeSelector'));

var services = angular.module('memo.shared.stores.services', []);
services.factory('StoreService', require('./services/StoreService'));


angular.module('memo.shared.stores', [
  'memo.shared.stores.directives',
  'memo.shared.stores.services'
]);