/*
 *  Provides CRUD callsa to the backend. getStoresInOrganization fetches all the
 *  stores created in the appropriate organization.
 */

'use strict';

var config = require('config');

module.exports = [
  '$http', 
  '$q',
  function ($http, $q) {

    function create(u) {
      return $http.post(config.apiRoot + '/store', u);
    }

    function update (u) {
      return $http.put(config.apiRoot + '/store/' + u.id, u);
    }

    function get(id) {
      return $http.get(config.apiRoot + '/store/' +id);
    }

    function getStoresInOrganization(orgId) {
      return $http.get(config.apiRoot + '/organization/' + orgId + '/stores');  
    }

    function remove (id) {
      return $http.delete(config.apiRoot + '/store/' + id);
    }

    return {
      create : create,
      update: update,
      get: get,
      getStoresInOrganization: getStoresInOrganization,
      remove: remove
    };
  }
];