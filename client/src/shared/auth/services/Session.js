'use strict';

module.exports = [
  '$cookies',
  function ($cookies) {

    this.checkAndCreateFromCookies = function () {
      var token = $cookies.sessionId;

      if (token && token.length) {
        var userId = $cookies.userId;
        var userRole = $cookies.userRole;

        this.populate(token, userId, userRole);
        return true;
      }
      return false;
    };

    this.populate = function(sessionId, userId, userRole) {
      this.id = sessionId;
      this.userId = userId;
      this.userRole = userRole;
    };

    this.create = function (sessionId, userId, userRole) {
      this.populate(sessionId, userId, userRole);
      
      $cookies.sessionId = this.id;
      $cookies.userId = this.userId;
      $cookies.userRole = this.userRole;
    };

    this.destroy = function () {
      this.populate(null,null,null);
      
      delete $cookies.sessionId;
      delete $cookies.userId;
      delete $cookies.userRole;
    };

    return this;
  }
];