'use strict';

module.exports = [
    '$rootScope', 
    'AUTH_EVENTS', 
    'AuthService',
    'Session',
    '$location',
    'UserService',
function($rootScope, AUTH_EVENTS, AuthService, Session, $location, UserService) {
  $rootScope.$on('$routeChangeStart', function (event, next) {
    
    if(!next) {
      return;
    }

    var authorizedRoles = (next ? (next.data ? next.data.authorizedRoles : []) :[]);
    if (!AuthService.isAuthorized(authorizedRoles)) {
      event.preventDefault();
      $rootScope.$broadcast(AUTH_EVENTS.notAuthenticated);
     }
   });


  $rootScope.$on(AUTH_EVENTS.logoutSuccess, function () {
    Session.destroy();
    $location.url('/login');
  });

  $rootScope.$on(AUTH_EVENTS.notAuthenticated, function () {
    Session.destroy();
    $location.url('/login?ret=' + $location.url());
  });

  // initial login 
  if (Session.checkAndCreateFromCookies()) {
    UserService.getCurrentUser().success(function(u) {
      $rootScope.$broadcast(AUTH_EVENTS.loginSuccess, u);
    });
  }
}];