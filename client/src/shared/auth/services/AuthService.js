'use strict';

var config = require('config');
var angular = require('angular');
module.exports = [
    '$http',
    'Session',
    'AUTH_EVENTS',
    '$rootScope',
    function ($http, Session, AUTH_EVENTS, $rootScope) {
        var authService = {};

        authService.login = function (credentials) {
            return $http
                    .post(config.apiRoot + '/auth' , credentials)
                    .success(function (res) {
                        Session.create(res.access_token, res.user.id,
                                res.user.permission);

                        $rootScope.$broadcast(AUTH_EVENTS.loginSuccess, res.user);
                        
                        return res.user;
                    }).error(function (e) {
                        $rootScope.$broadcast(AUTH_EVENTS.loginFailed, e);
                    });
        };

        authService.isAuthenticated = function () {
            return !!Session.userId;
        };

        authService.logout = function() {
            return $http.post(config.apiRoot + '/auth/logout', {}).then(function() {
                $rootScope.$broadcast(AUTH_EVENTS.logoutSuccess);
            });
        };

        authService.isAuthorized = function (authorizedRoles) {
            if (!angular.isArray(authorizedRoles)) {
                authorizedRoles = [authorizedRoles];
            }
            
            if (authorizedRoles.indexOf('*') !== -1) {
                return true;
            }

            return (authService.isAuthenticated() &&
                    authorizedRoles.indexOf(Session.userRole) !== -1);
        };

        return authService;
    }];