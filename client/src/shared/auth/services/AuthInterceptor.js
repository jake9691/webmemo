'use strict';

module.exports = [
    '$rootScope',
    '$q',
    'AUTH_EVENTS',
    'Session',
    '$location',
    function ($rootScope, $q, AUTH_EVENTS, Session, $location) {
      var currentRequests = [];
        return {
          responseError: function (response) { 
            if (response.status > 400 && response.status < 404 && !/login/gi.test($location.url())) {
              $rootScope.$broadcast(AUTH_EVENTS.notAuthenticated);
            }
            
            return $q.reject(response);
          },
          request: function (req) {
            var token = Session.id;

            if (token && token.length) {
              req.headers['WWW-Authenticate'] = 'Bearer ' + token;  
            }

            return req;
          }
        };
    }
];