'use strict';


var angular = require('angular');

require('angular-cookies');

/* auth is mostly borrowed from https://medium.com/opinionated-angularjs/techniques-for-authentication-in-angularjs-applications-7bbf0346acec */ 
var authServices = angular.module('memo.shared.auth.services', ['ngCookies']);
authServices.factory('AuthService', require('./services/AuthService'));
authServices.service('Session', require('./services/Session'));
authServices.factory('AuthInterceptor', require('./services/AuthInterceptor'));

var authCtrls = angular.module('memo.shared.auth.controllers', []);
authCtrls.controller('ApplicationCtrl', require('./controllers/ApplicationCtrl'));
authCtrls.controller('LoginCtrl', require('./controllers/LoginCtrl'));

var authDirectives = angular.module('memo.shared.auth.directives', []);
 
authDirectives.directive('loginForm', function () {
  return {
    restrict: 'EA',
    templateUrl: 'auth/login-form.html', 
    scope: {
      enableRegistration: '='
    }
  };
});


var auth = angular.module('memo.shared.auth', [
    'memo.shared.auth.services', 
    'memo.shared.auth.controllers',
    'memo.shared.auth.directives']);
 
auth.run(require('./services/AuthRunner'))
.constant('AUTH_EVENTS', {
  authenticated: 'auth-authenticated',
  loginSuccess: 'auth-login-success',
  logoutSuccess : 'auth-logout-success',
  loginFailed: 'auth-login-failed',
  notAuthenticated: 'auth-not-authenticated'
})
.constant('USER_ROLES', {
    ANONYMOUS: 'ANONYMOUS',
    ADMINISTRATOR: 'ADMINISTRATOR',
    ORGANIZATIONOWNER: 'ORGANIZATIONOWNER',
    EMPLOYEE_EXTENDED: 'EMPLOYEE_EXTENDED',
    EMPLOYEE: 'EMPLOYEE'
})
.config([
  '$httpProvider',
  function ($httpProvider) {
  $httpProvider.interceptors.push([
    '$injector',
    function ($injector) {
      return $injector.get('AuthInterceptor');
    }
  ]);
}]);
