'use strict';

module.exports = [
    '$scope',
    'AUTH_EVENTS',
    '$location',
    'AuthService',
    '$q',
    function ($scope, AUTH_EVENTS, $location, AuthService, $q) {       
        var userDefer = $q.defer();

        $scope.logout = function () {
            $scope.user = null;
            $scope.userRole = null;

            AuthService.logout();
        };



        $scope.currentUser = function () {
            return userDefer.promise;
        };

        function clearUser() {
            $scope.user = null;
            userDefer = $q.defer();
        }

        $scope.$on(AUTH_EVENTS.loginSuccess, function (e, u) {
            $scope.user = u;

            userDefer.resolve(u);
        });

        $scope.$on(AUTH_EVENTS.notAuthenticated, clearUser);
        $scope.$on(AUTH_EVENTS.logoutSuccess, clearUser);
    }
];