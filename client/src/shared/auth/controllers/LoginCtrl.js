'use strict';

module.exports = [
    '$scope',
    'AUTH_EVENTS',
    'AuthService',
    '$routeParams',
    '$location',
    function ($scope, AUTH_EVENTS, AuthService, $routeParams, $location) {
        var that = this;
        $scope.login = function (c) {
            AuthService.login(c).then(
            function() {
                $location.url($routeParams.ret ? $routeParams.ret : '/');
            },
            function(r) {
                that.errors = r.data;
            });
        };
    }
];