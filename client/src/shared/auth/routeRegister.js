'use strict';

module.exports = function ($routeProvider) {
	$routeProvider
	.when('/register', {
      templateUrl: 'auth/register.html',
      data: {
        authorizedRoles: ['*']
      }
    })
    .when('/login', {
      templateUrl: 'auth/login.html',
      data: {
        authorizedRoles: ['*']
      }
    });
};