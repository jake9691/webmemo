/*
 *  Provides dependencies for /src/shared/utilities
 */


'use strict';

var angular = require('angular');


var utils = angular.module('memo.shared.utilities', []);
utils.directive('formHelper', require('./directives/formHelper'));
utils.directive('errorMessages', require('./directives/errorMessages'));
utils.directive('addressFields', require('./directives/addressFields'));
