/*
*  Points to the templateURL which contains error msgs.
*/

'use strict'; 

module.exports = function () {
	return {
		restrict: 'EA',
		require: '^formHelper',
		scope: {
			key: '@'
		},
		templateUrl: 'utilities/error-messages.html',
		link: function (scope, element, attrs, formHelperCtrl) {
			function setMessages(messages) {
				scope.messages = messages;
			}

			if (attrs.key) {
				formHelperCtrl.register(attrs.key, setMessages);	
			}
		}
	};
};