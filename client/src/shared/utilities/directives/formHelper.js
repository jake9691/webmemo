/*
 *  Provides the correct msgs code displayed from the server when a
 *  user doesn't enter a valid input.
 */

'use strict';

var angular = require('angular');

module.exports = [
	function () {
		return {
			restrict: 'A',
			require: '^form',
			scope: {
				formHelper: '='
			},
			controller: function ($scope, $element, $attrs) {
				this.register = function (key, fn) {
					$scope.errorMessageRenderers[key] = fn;
				};
			},
			link: function(scope, element, attrs, formCtrl) {
				scope.errorMessages = {};
				scope.errorMessageRenderers = {};

				scope.$watch('formHelper', function(errors) {
					if(!errors) return; 

					var errorsByKey = {};

					// reset validity
					for (var key in formCtrl) {
						// hxor times!
						if (key[0] !== '$') {
							formCtrl[key].$setValidity('server', true);
							(scope.errorMessageRenderers[key] || angular.noop)([]);
						}
					}

					angular.forEach(errors, function (error) {
						if (formCtrl[error.key]) {
							formCtrl[error.key].$setValidity('server', false);
						}

						(errorsByKey[error.key] = (errorsByKey[error.key] || [])).push(error);
					});

					for(var k in errorsByKey) {
						(scope.errorMessageRenderers[k] || angular.noop)(errorsByKey[k]);
					}
				});

				
			}
		};
	}
];