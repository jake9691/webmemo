/*
 *  There are multiple forms which require an address object to be sent.
 *  This directive points to the templateURL used in those forms, see
 *  src/templates/utilities/address-fields.html
 */


'use strict'; 

module.exports = function () {
	return {
		restrict: 'EA',
		require: '^form',
		templateUrl: 'utilities/address-fields.html',
		scope: {
			data: '='
		},
		link: function (scope, element, attrs, formCtrl) {
			scope.parentForm = formCtrl;
		}
	};
};