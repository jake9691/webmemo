/*
 * provides dependencies for src/shared/providers
 */


'use strict'; 

var angular = require('angular');

var services = angular.module('memo.shared.providers.services', []);
services.factory('ProviderService', require('./services/ProviderService'));

var ctrls = angular.module('memo.shared.providers.controllers', []);
ctrls.controller('RegisterProviderCtrl', require('./controllers/RegisterProviderCtrl'));

var directives = angular.module('memo.shared.providers.directives', []);
directives.directive('providerForm', require('./directives/registerProviderForm'));
directives.directive('providerList', require('./directives/providerList'));

var users = angular.module('memo.shared.providers', [
  'memo.shared.providers.services',
  'memo.shared.providers.controllers',
  'memo.shared.providers.directives'
]);