/*
 *  Controller for creating and editing Providers in 
 *  src/templates/providers/new.html and src/templates/providers/edit.html. Since 
 *  The EDIT and CREATE functionality both use the same template this controlller
 *  provides the correct data to the respective forms. The correct provider is 
 *  set by $routeParams. Provides the correct organization ID while creating
 *  a new provider.
 */


 'use strict';

 module.exports = [
 '$scope',
 '$routeParams',
 '$location',
 'OrganizationsService',
 'ProviderService',
 function ($scope, $routeParams, $location, OrganizationsService, ProviderService) {
 	$scope.organizationId = $routeParams.id;
 	
 	if ($routeParams.provider_id && $routeParams.provider_id.length) {
 		ProviderService.get($routeParams.provider_id).success(function(p) {
 			$scope.provider = p;
 		});
 	}

 	var me = this;

 	this.register = function (p, destination) {
 		p.organization = {
 			id: parseInt($scope.organizationId, 10)
 		};
 		
 		return ProviderService.create(p).success(function (p) {
 			$location.url(destination);
 		}).error(function(d) {
 			me.errors = d;
 		});
 	};
 	
 	this.update = function (p, destination) {
 		return ProviderService.update(p).success(function (p) {
 			$location.url(destination);
 		}).error(function(p) {
 			me.errors = p;
 		});
 	};

 	this.delete = function (p) {
 		return ProviderService.remove(p);
 	};
 }
 ];