/*
*  CRUD for providers. Also provides calls to the backend to
*  get all Providers for an organization with getProvidersInOrganization(orgId).
*/

'use strict';

var config = require('config');

module.exports = [
  '$http', 
  function ($http) {

    function create(p) {
      return $http.post(config.apiRoot + '/provider', p);
    }

    function update (p) {
      return $http.put(config.apiRoot + '/provider/' + p.id, p);
    }

    function get(id) {
      return $http.get(config.apiRoot + '/provider/' +id);
    }
    
    function getProvidersInOrganization(orgId) {
      return $http.get(config.apiRoot + '/organization/' + orgId + '/providers');  

    }
    
    function remove(id) {
      return $http.delete(config.apiRoot + '/provider/' + id);
    }
    
    function getForOrganization(id){
        return $http.get(config.apiRoot + '/organization/' + id + '/providers');
    }

    return {
      create : create,
      update: update,
      get: get,
      remove: remove,
      getForOrganization: getForOrganization,
      getProvidersInOrganization: getProvidersInOrganization
    };
  }
];