/*
 *  Points to the templateURL for src/templates/edit and
 *  src/templates/new
 */

'use strict';

module.exports = function () {
  return {
    restrict: 'EA',
    templateUrl: 'providers/provider-form.html',
    scope: {
      destination: '@',
      provider:'=',
      organization:'=',
      action:'&',
      errors: '='
    },
    link: function(scope, element, attrs) {
      
    }
  };
};