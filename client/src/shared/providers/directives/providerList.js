/*
*  Points to the templateURL used in src/templates/providers/provider-list.html
*  Gets the providers from the current organization being viewed with the function
*  fetchProviders, which is needed to populate the table in provider-list.html.
*  Deletes a provider when the delete button in the table is pressed
*/
'use strict';

module.exports = [
'ProviderService',
function (ProviderService) {
	return {
		restrict: 'EA',
		templateUrl: 'providers/provider-list.html',
		scope: {
			organization: '='
		},
		link: function (scope, element, attrs) {
			
			function fetchProviders(os) {
				if (!os) return;
				scope.fetching = true;
				ProviderService.getProvidersInOrganization(os.id).success(function (p) {
					scope.list = p;
					scope.fetching = false;
				});
			}

			scope.$watch('organization', fetchProviders);
			
			scope.deleteProvider = function (p) {
				ProviderService.remove(p.id).success(function () {
					fetchProviders(scope.organization);
				});
			};
		}
	};
}
];