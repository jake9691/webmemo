/* 
*  Points to the template used for editing and creating customers in
*  src/templates/customers/edit.html and 
*  src/templates/customers/registerCustomer.html
*/ 
'use strict';

module.exports = function () {
  return {
    restrict: 'EA',
    templateUrl: 'customers/register-customer-form.html',
    scope: {
      customer: '=',
      errors: '=',
      action: '&',
      organization: '='
    },
    link: function(scope, element, attrs) {
      
    }
  }; 
};