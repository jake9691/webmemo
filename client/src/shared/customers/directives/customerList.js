/*
*  Points to the template which displays a list of customers.
*  Provides the number of customers created in an organization with
*  fetchCustomerCount which is needed for pagination within customerstable. 
*  The function fetchCustomers provides the correct amount of customers(with the
*   parameters offset and pageSize) to be displayed in the customers table.
*   Also handles the deletion of a customer when delete icon in the table
*   is clicked.
*/

'use strict';

module.exports = [
'CustomerService',
function (CustomerService) {
	return {
		restrict: 'EA',
		templateUrl: 'customers/customer-list.html',
		scope: {
			organization: '='
		},
		link: function (scope, element, attrs) {
            var init = true; 
            
            scope.currentPage = 1;
            scope.pageSize = 5;
            scope.offset = (scope.currentPage -1) * scope.pageSize;
            
            
            scope.$watch('organization', function(o) {
               if (!o) return; 
               if (init) {
                   fetchCustomerCount(o).then(function() {
                       fetchCustomers(o);
                   });
                   init = false;
               }
           });
            
            function fetchCustomerCount(os){
                return CustomerService.getNumberOfCustomersForOrganization(os.id).success(function (c) {
                    scope.totalItems = c;
                    return os;
                });
            }
            
            scope.$watch('currentPage', function (cp) {
                scope.offset = (cp-1) * scope.pageSize;
                
                fetchCustomers(scope.organization);
            });
            
            function fetchCustomers(os) {
                if (!os) return;
                
                CustomerService.getRangeOfCustomerForOrganization(os.id, scope.offset, scope.pageSize).success(function (e) {
                    scope.list = e; 
                });
            }
            

            scope.deleteCustomer = function (c) {
                CustomerService.remove(c.id).success(function () {
                    fetchCustomers(scope.organization);
                });
            };

        }
    };
}
];