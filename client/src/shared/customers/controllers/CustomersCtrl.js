'use strict'; 

module.exports = [
  '$scope',
  'CustomerService', 
  'OrganizationsService',
   '$routeParams',
  function ($scope, CustomerService, OrganizationsService,$routeParams) {
    
    OrganizationsService.find($routeParams.id).success(function(os) {
        
        $scope.organization = os;
        
    }).then(function(os) {
        CustomerService.getRangeOfCustomerForOrganization($scope.organization.id).success(function(c) {
  		    $scope.customers = c;
  	    });
    });
    
  }
];