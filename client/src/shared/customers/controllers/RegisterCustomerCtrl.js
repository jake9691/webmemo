/*
* Controller for creating and editing customers in 
* src/templates/customers/registerCustomer.html.  Since the EDIT and CREATE
* functionality both use the same template this controller makes sure to pass on
* the correct customer to the EDIT form in src/templates/customers/edit.html
* and to provide the correct organization from the route params when creating a user.
*/
'use strict';

module.exports = [
  '$scope',
  '$location',
  'CustomerService',
  '$routeParams',
  function ($scope, $location, CustomerService, $routeParams) { 
    // populate customer if applicable 
    if ($routeParams.customer_id && $routeParams.customer_id.length) {
        CustomerService.get($routeParams.customer_id).success(function(c) {
            $scope.customer = c;
        });
    }
    // set scope organization id in route params 
    $scope.organizationId = $routeParams.id;
    
    var me = this;
    this.register = function (c, destination) {
      c.dependencies = {
          organizationId: parseInt($routeParams.id, 10)
      };
      
      return CustomerService.create(c).then(function(r) {
          
         $location.url(destination);
      },
      function(d) {
          
          me.errors = d.data;
      });
    };

    this.update = function (c, destination) {
      c.dependencies = {
          organizationId: parseInt($routeParams.id, 10)
      };
      
      return CustomerService.update(c).then(function(r) {
         $location.url(destination);
      },
      function(d) {
          me.errors = d.data;
      });
    };
  }
];