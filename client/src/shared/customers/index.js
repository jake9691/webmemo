/*
 * Sets dependencies for src/shared/customers/
 */

'use strict'; 

var angular = require('angular');

var services = angular.module('memo.shared.customers.services', []);
services.factory('CustomerService', require('./services/CustomerService'));

var ctrls = angular.module('memo.shared.customers.controllers', []);
ctrls.controller('RegisterCustomerCtrl', require('./controllers/RegisterCustomerCtrl'));
ctrls.controller('CustomersCtrl', require('./controllers/CustomersCtrl'));

var directives = angular.module('memo.shared.customers.directives', []);
directives.directive('registerCustomerForm', require('./directives/registerCustomerForm'));
directives.directive('customerList', require('./directives/customerList'));

var users = angular.module('memo.shared.customers', [
  'memo.shared.customers.services',
  'memo.shared.customers.controllers',
  'memo.shared.customers.directives'
]);
