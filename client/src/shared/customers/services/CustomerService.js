/* 
*  Provided calls to the backend to CRUD customers.
*  See doucmentation for customers/customerList.js for  
*  clarification of getNumberOfCustomersForOrganization
*  and getRangeOfCustomerForOrganization
*/ 
'use strict';

var config = require('config');

module.exports = [
  '$http', 
  function ($http) {

    function create(u) {
      return $http.post(config.apiRoot + '/customer', u);
    }

    function update (u) {
      return $http.put(config.apiRoot + '/customer/' + u.id, u);
    }

    function get(id) {
      return $http.get(config.apiRoot + '/customer/' +id);
    }
    
    function getNumberOfCustomersForOrganization(orgId){
      return $http.get(config.apiRoot + '/organization/' + orgId + '/customers/count');
    }
    
    function getRangeOfCustomerForOrganization(orgId, offset, count) {
      return $http.get(config.apiRoot + '/organization/' + orgId + '/customers', {
          params: {
              offset: offset,
              count: count
          }
      });  
    }
    
    function remove(id) {
      return $http.delete(config.apiRoot + '/customer/' + id);
    }

    return {
      create : create,
      update: update,
      get: get,
      remove: remove,
      getNumberOfCustomersForOrganization: getNumberOfCustomersForOrganization,
      getRangeOfCustomerForOrganization: getRangeOfCustomerForOrganization
      
    };
  }
];