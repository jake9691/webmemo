/*
*  Controller for src/templates/organizations/organizations.html
*  Gets the data needed to populate a list of orgranizations with the
*  Organization Service.
*/

'use strict'; 

module.exports = [
  '$scope',
  'OrganizationsService', 
  function ($scope, OrganizationsService) {
  	OrganizationsService.getList().success(function(o) {
  		$scope.organizations = o;
  	});
    
  }
];