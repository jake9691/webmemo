/*
 *  Controller for organization Creation in src/templates/organizations/new.html. 
 *  Sets the appropriate user dependencies needed in order to create a store.
 */

'use strict'; 

module.exports = [
  '$scope',
  'OrganizationsService', 
  'Session',
  '$location',
  function ($scope, OrganizationsService, Session, $location) {
    var me = this;
    this.create = function(o) {
      o.dependencies = {
        userId: parseInt(Session.userId, 10)
      };

      return OrganizationsService.create(o).then(function (o,b) {
        $location.url('/organizations/' + o.data.id);
      }, function(e) {
        me.errors = e.data;
      });
    };

    this.update = function(o) {
      return OrganizationsService.update(o);
    };
  }
];