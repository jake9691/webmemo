/*
 * Controls for src/templates/organizations/organization.html. Provides the 
 * appropriate organization with $routeParams.
 */

'use strict';

module.exports = [
	'$scope',
	'$routeParams',
	'OrganizationsService',
	function ($scope, $routeParams, OrganizationsService) {
		
		OrganizationsService.find($routeParams.id).success(function(d) {
			$scope.organization = d;
		});
	}
];