/*
 *  Points the templateURL to the template used to populate a list in the
 *  OrganizationDashboard  in src/templates/organization-menu.html 
 *  and the data needed to populate that list. 
 */


'use strict'; 

module.exports = [
	'OrganizationsService',
	'ORGANIZATION_EVENTS',
	'Session',
	'AUTH_EVENTS',
	function (OrganizationsService, ORGANIZATION_EVENTS, Session, AUTH_EVENTS) {
		return {
			restrict: 'EA',
			templateUrl: 'organizations/organization-menu.html',
			replace: true,
			require: '^navigation',
			link: function (scope, element, attrs, navCtrl) {
				scope.$on(ORGANIZATION_EVENTS.newOrganization, function (evt, o) {
					scope.organizations.push(o);
					scope.$evalAsync(function () {
						navCtrl.digest();
					});
				});

				function fetchOrganizations() {
					OrganizationsService.getList().success(function(os) {
						scope.organizations = os;
					});	
				}

				if (Session.id && Session.id.length) {
					fetchOrganizations();
				}

				scope.$on(AUTH_EVENTS.loginSuccess, fetchOrganizations);

				scope.$on(AUTH_EVENTS.logoutSuccess, function () {
					scope.organizations = [];
				});
			}
		};
	}
];