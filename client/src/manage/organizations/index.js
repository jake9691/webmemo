/*
 *  Provides dependencies for src/manage/organizations/
 */


'use strict';

var angular = require('angular');

var services = angular.module('memo.manage.organizations.services', []);
services.factory('OrganizationsService', require('./services/OrganizationsService'));

var directives = angular.module('memo.manage.organizations.directives', []);
directives.directive('organizationMenu', require('./directives/organizationMenu'));

var controllers = angular.module('memo.manage.organizations.controllers', []);
controllers.controller('OrganizationsCtrl', require('./controllers/OrganizationsCtrl'));
controllers.controller('OrganizationCtrl', require('./controllers/OrganizationCtrl'));
controllers.controller('OrganizationDetailCtrl', require('./controllers/OrganizationDetailCtrl'));

angular.module('memo.manage.organizations', [
  'memo.manage.organizations.services',
  'memo.manage.organizations.directives',
  'memo.manage.organizations.controllers'
]).constant('ORGANIZATION_EVENTS', {
	newOrganization : 'organization-new-organization'
});