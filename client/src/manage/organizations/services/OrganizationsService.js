/*
 *  Provides Create&Read for Organizations. getList provides all organizations
 *  created by a certain user.
 */


'use strict'; 

var config = require('config');

module.exports = [
  '$http',
  '$q',
  'ORGANIZATION_EVENTS',
  '$rootScope',
  function ($http, $q, ORGANIZATION_EVENTS, $rootScope) {
    function create(o) {
      return $http.post(config.apiRoot + '/organization', o).success(function(or) {
        $rootScope.$broadcast(ORGANIZATION_EVENTS.newOrganization, or);
      });
    }

    function getList() {
      return $http.get(config.apiRoot + '/organization/user');
    }

    function find(id) {
      return $http.get(config.apiRoot + '/organization/' + id); 
    }

    return {
      create: create,
      getList: getList,
      find: find
    };
  }
];