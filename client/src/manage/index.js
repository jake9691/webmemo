/*
 *  Sets dependencies for src/manage/stores and provides part of sitemap
 */

'use strict';

var angular = require('angular');

require('angular-route');
require('angular-bootstrap');
require('../shared');
require('./organizations');
require('./stores');


require('memo-templates');
var routeRegister = require('../shared/auth/routeRegister');

var manage = angular.module('memo.manage', [
    'ngRoute', 
    'ui.bootstrap', 
    'memo.templates', 
    'memo.shared', 
    'memo.manage.organizations', 
    'memo.manage.stores',
    'memo.shared.stores']);

manage.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/', {
      templateUrl: 'owner/owner-dashboard.html',
      data: {
        authorizedRoles: ['ORGANIZATIONOWNER']
      }
    }).
    when('/registerUser', {
      templateUrl: 'users/registerUser.html',
      data: {
        authorizedRoles: ['ORGANIZATIONOWNER']
      }  
    }).
    when('/account', {
      templateUrl: 'account.html'
    }).
    when('/organizations/:id/customers/new', {
       templateUrl: 'customers/registerCustomer.html',
       data: {
           authorizedRoles: ['ORGANIZATIONOWNER']
       } 
    }).
    when('/organizations/:id/customers', {
      templateUrl: 'customers/customers.html',
      data: {
        authorizedRoles: ['ORGANIZATIONOWNER']
      }  
    }).
    when('/organizations/new', {
      templateUrl: 'organizations/new.html',
      data: {
        authorizedRoles: ['ORGANIZATIONOWNER']
      }
    }).
    when('/organizations', {
      templateUrl: 'organizations/organizations.html',
      data: {
        authorizedRoles: ['ORGANIZATIONOWNER']
      }
    }).
    when('/organizations/:id', {
      templateUrl: 'organizations/organization.html',
      data: {
        authorizedRoles: ['ORGANIZATIONOWNER']
      }
    }).
    when('/organizations/:id/employees/new', {
      templateUrl: 'employees/new.html',
      data: {
        authorizedRoles: ['ORGANIZATIONOWNER']
      }
    }).
    when('/organizations/:id/employees/:employee_id', {
      templateUrl: 'employees/edit.html',
      data: {
        authorizedRoles: ['ORGANIZATIONOWNER']
      }
    }).
    when('/organizations/:id/stores/new', {
      templateUrl: 'stores/new.html',
      data: {
        authorizedRoles: ['ORGANIZATIONOWNER']  
      }
    }).
    when('/organizations/:id/stores/:store_id', {
      templateUrl: 'stores/edit.html',
      data: {
        authorizedRoles: ['ORGANIZATIONOWNER']  
      }
    }).          
    when('/organizations/:id/providers/new', {
      templateUrl: 'providers/new.html',
      data: {
        authorizedRoles: ['ORGANIZATIONOWNER']
      }  
    }).
    when('/organizations/:id/providers/:provider_id', {
      templateUrl: 'providers/edit.html',
      data: {
        authorizedRoles: ['ORGANIZATIONOWNER']  
      }
    }).
    when('/organizations/:id/customers/:customer_id', {
      templateUrl: 'customers/edit.html',
      data: {
        authorizedRoles: ['ORGANIZATIONOWNER']  
      }
    }).
    otherwise({
      templateUrl: '404.html',
      data: {
        authorizedRoles: ['*'] 
      }
    });

    routeRegister($routeProvider);
}]);