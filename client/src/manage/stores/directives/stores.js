/*
 * Points to the templateURL which list stores in a table. fetchStores(o) provides
 * the correct data to input in to the table. deleteStore function deletes the appropriate
 * store when the delete button is cliked in the table
 */

'use strict';

module.exports = [
	'StoreService',
	function (StoreService) {
		return {
			restrict: 'EA',
			templateUrl: 'stores/stores.html',
			scope: {
				organization: '='
			},
			link: function(scope, element, attrs) {
				scope.$watch('organization', fetchStores);

				function fetchStores(o) {
					if (!o) return;
					scope.fetching = true;
					StoreService.getStoresInOrganization(o.id).success(function (ss) {
						scope.fetching = false;
						scope.stores = ss;
					});
				}				

				scope.deleteStore = function (s) {
					StoreService.remove(s.id).success(function () {
						fetchStores(scope.organization);
					}).error(function (e) {
						var error;
						if (e && e.length > 0) {
							error = e[0];
						}
						scope.deleteError = error.message;
					});
				};

				scope.closeDeleteError = function () {
					scope.deleteError = null;
				};
			}
		};
	}
];