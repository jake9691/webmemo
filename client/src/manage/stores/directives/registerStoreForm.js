/*
*  Points to the templateURL used in src/templates/stores/new
*  and src/template/stores/edit
*/


'use strict';

module.exports = function () {
  return {
    restrict: 'EA',
    templateUrl: 'stores/register-store-form.html',
    scope: {
      action: '&',
      credentials: '=',
      errors: '=',
      editing: '@'
    },
    link: function(scope, element, attrs) {
      scope.editing = attrs.editing === "true";
    }
  };
};