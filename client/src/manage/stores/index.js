/*
 *  Sets dependencies for src/manage/stores/
 */

'use strict'; 

var angular = require('angular');


var ctrls = angular.module('memo.manage.stores.controllers', []);
ctrls.controller('StoreCtrl', require('./controllers/StoreCtrl'));

var directives = angular.module('memo.manage.stores.directives', []);
directives.directive('registerStoreForm', require('./directives/registerStoreForm'));
directives.directive('stores', require('./directives/stores'));

var users = angular.module('memo.manage.stores', [
  'memo.manage.stores.controllers',
  'memo.manage.stores.directives'
]);

