/*
*  Controller for creating a store. Sets the correct organizationId with
*  $routeParams needed for store creation in src/templates/stores/new. Since the
*  same form is used to EDIT and CREATE a store this controller sets the correct 
*  credentials for a store to populate the edit form found in 
*  /src/templates/stores/edit.html
*/
'use strict';

module.exports = [
  '$scope',
  '$location',
  'StoreService', 
  '$routeParams',
  function ($scope, $location, StoreService, $routeParams) {
    $scope.organizationId = parseInt($routeParams.id, 10);

    if ($routeParams.store_id && $routeParams.store_id.length) {
      StoreService.get($routeParams.store_id).success(function (s) {
        $scope.credentials = s;
      });
    }

    var me = this;

    this.update = serverCall("update");
    this.register = serverCall("create");
     
    function serverCall(name) {
      return function (c, destination) {
        c.organization = {
          id: $scope.organizationId
        };
        return StoreService[name](c).then(function(r) {
          $location.url(destination);
        },
        function(d) {
          me.errors = d.data;
        });
      };
    }
  }

];