(function () {
  'use strict';


  var angular = require('angular');

  require('angular-route');
  require('memo-templates');
  require('./shared');
  require('./app/index');
  require('./manage');
  
  var memoapp = angular.module('memo', ['ngRoute', 'memo.app', 'memo.manage', 'memo.shared', 'memo.templates']);

})();